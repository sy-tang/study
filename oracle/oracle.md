在docker中安装oracle。

```shell
docker pull wnameless/oracle-xe-11g
docker run -d -p 1521:1521 --name myoracle oracle
docker exec -it myoracle /bin/bash   #进入oracle容器

#在容器内部安装sudo，改变字符集
apt-get update

apt-get install sudo

sudo apt-get -y install language-pack-zh-hans  #安装支持中文的字符集

export LANG=zh_CN.UTF-8  #使用支持中文的字符集

locale  #查看使用的字符集

sqlplus system/oracle    #在docker中使用sqlplus命令，进入oracle数据库。system 是账号，oracle是密码

c  #修改上一条sql语句中的某个单词
ed  #修改上一条sql语句

#在oracle中字符、日期需要单引号引起来。
#在oracle中where后的条件是从右往左执行的
#任何字段和null做计算，结果都为null。
```

**任何字段和null做计算，结果都为null。**

nvl：相当于if 。例：  nvl(sal,0) 如果sal字段为null，则用0替换null。

nvl2：相当于if。。。else。 例：nvl(sal,sal,0)；如果sal不为空，则用自己的值，如果为null，则用0替换null。防止计算结果为null。



escape ‘\’   在oracle中，什么字符都可以当转义字符，所以当需要用转义时，应该使用escape来指定哪个字符是表示转义。

not in（....）:not in中不能有null，否则结果为空。



**在utf-8的编码下，一个汉字或者符号占3个字节**

**在gbk的编码下，一个汉字或者字符占2个字节**

## **单行函数、多行函数**

### 单行函数：每次只操作一条记录

- **字符函数：**
  
  - lower():转小写
  - upper():转大写
  - initcap()：首字符大写
  - substr(str,begin,len):字符串截取。从begin开始截取，截取len个字符。
  - length()：字符数
  - lengthb()：字节数
  - insrt(str,"a"):在str中找a这个字符串出现的位置
  - lpad(str,len,*)：左填充：str一共要有10位，如果不够10位就用※来左填充
  - rpad(str,len,*)：右填充
  - trim（”x" from "xxxhelloxxx"）：去点任意字符,这里是去掉x这个字符。默认是去掉左右两边的空格
- replace('hello','l','-'):字符替换，将l替换成-。
  
- **数值函数**
  
  - round(67.182，2):保留几位小数，并进行四舍五入：保留67.182的2位小数，舍去2。
  - trunc(67.185,2):保留几位小数，不进行四舍五入。
- mod(123,3):求余。123%3
  
- **日期函数**
  
  - sysdate ：表示当前时间
    - 格式化时间：将日期转成字符串：to_char(sysdate,'yyyy-mm-dd')
  - 日期可以加减数字，默认加的是天。但日期和日期之间只能减
  - months_between(d1,d2):日期相减，d1-d2。(str,len,*)
  - add_months(date,3)：给date加上3个月
  - last_day(sysdate) :显示当前时间月份的最后一天。
- next_day(sysdate,'星期五'):显示下个星期五是什么时候
  
- **转换函数**

  - 显示转换

  <img src="D:\TyporaDir\oracle\oracle.assets\1592882055260.png" alt="1592882055260" style="zoom:80%;" />

  - 隐式转换
    - 字符可以转成数字或者时间类型
    - 时间、数字类型可以自动转成字符。

- **通用函数**

  - nvl  nvl2
  - nullif（a,b）：如果a=b则返回null，如果不相等，则返回a。
  - coalesce：从左往右找第一个不为空的。
  - decode(字段，条件1，返回值1，条件2，返回值2，....，最后的返回值)：如果前面条件都不符合，则使用最后的返回值。有符合的则使用符合的返回值。
  - case表达式：case  字段  when  条件1  then  返回值1 when   .. then ..   .....   else 最后的返回值  end  from   emp ；

## 多行函数：每次操作多条记录

count(*):统计

max

min

avg

sum ：求和





```sql
creage table mytab 
as
select * from emp;  /*创建一个表，将emp中的数据和表结构复制到新的表中*/

insert into mytab (字段1，字段2,...) select 字段1，字段2,... from emp;  /*往已存在的表中插入数据*/
```



## delete和truncate

对于小表，delete删除速度更快，因为他是一条一条删除

对于大表，truncate删除速度更快，他是直接丢弃表，然后创建一张新的表。

- delete能回滚，truncate不回滚。

- delete支持闪回（数据恢复的方式），truncate不支持

- delete删除数据不会释放空间（没有真正删除，相当于放回收站一样将数据保存在了undo空间），truncate释放空间（真正删除，相当于清空回收站了）

- delete会产生碎片，truncate不会
  - 如果碎片过多，如何解决：
    - alter table  表名 move    ：整理碎片空间，合成一个大的可用空间。
    - 数据导出导入。将内存碎片太多的表的数据导出到一个新的表中，再将新表中的数据导入到原来的表中。

purge 清空所有delete的表，数据等。







伪列:不存在任何表中，但可以被所有表共享。

如：rownum（逻辑序列）、rowid（物理序列，物理真实存放位置，18位）

在同一条sql中rownum是不会改变的。







orcale中约束：

- 检查约束 check
  - 可以限制字段长度，数据类型等
- 唯一约束 unique
- 外键约束 primary key
- 非空约束 foreign key
- 默认约束 default



列级约束：约束只作用于一个列。一个列可以有多个约束，以空格分隔，最多可以有以上6个约束。

注意，如果有多个约束，default必须放在第一位

![1592897423514](D:\TyporaDir\oracle\oracle.assets\1592897423514.png)

表记约束：作用于多个列。以，分隔，最多只能有4个约束（主键、外键、唯一、检查）

![1592898421265](D:\TyporaDir\oracle\oracle.assets\1592898421265.png)







## 三大范式

1NF：确保每列的原子性（每列不可再分）。

2NF:每张表只描述一件事（宏观）。定义：处理主键以外的其他字段，都依赖于主键。不依赖于主键的字段，不应该存在当前表中，应该将其他字段拆到另一张表中。

3NF：定义，除了主键以外的其他字段，都不传递依赖于主键。x->y->z;z传递依赖于x。 





视图：属于数据库对象之一。视图是一个虚表，不存在的表。

虚表可以建立在一张多种多张表上，可以将多张表中的某些字段放到虚表中，组成一张虚表。

虚表的好处：

- 简化查询

  - 创建视图

  - ```sql
    create  view  视图名  as  select ....;  /*将select查出来的数据当成一张虚表，以后查一些数据时直接查这个虚表就行了，不再使用原来的select语句，这就用视图封装了查询语句*/
    
    
    create  view myview  
    as
    select name,age where age =20 with check option;
    /*加上with check option的目的是防止修改视图时逃逸出where age=20 这个条件。也就是说，不能修改视图中age，否则报错。*/
    
    create  view myview  
    as
    select name,age where age =20 with read only;
    /*with read only:创建只读视图，不允许DML操作*/
    ```

- 增加安全性：只将需要的字段放到虚表中，防止别人访问到不需要的权限。

创建视图需要授权：

- 授权：

```sql
conn scott/tiger  /*切换用户*/
conn  / as sysdba /*切换到管理员账户*/
grant xxx(权限) to scott(用户名) /*将权限授权给scott用户*/
revoke xx(权限) from scott /*撤销用户的权限*/
```

修改视图的数据会改变原表中的数据，所以对于视图，建议只做查询操作，不做其他操作。





## oracle事务：

开启：第一条DML语句开始就自动开启了事务

提交：（手动提交）

- 显示提交 commit
- 隐式提交：正常退出exit、DCl、DDL

回滚：

- 显示回滚：rollback
- 隐式回滚：异常退出时（宕机等）

保存点：savepoint

```sql
delete from student where id =10 ;
delete from student where id =11 ;
savepoint mypoint;  /*设置保存点*/
delete from student where id =12 ;
rollback to savepoint mypoint; /*回滚到保存点*/
```



### 事务的隔离级别：

没有隔离产生的并发问题：

脏读：数据在另一个**未提交的事务中被修改了**，导致某个事务的该数据是**已过时的数据**。

不可重复读：在一个事务中多次读取同一条记录结果不一致。读取到了其他事务提交的修改的数据。

幻读：在一个事务中多次读取一批数据，数据结果不一致。读取到了其他事务提交的新插入的数据。



隔离级别：oracle只支持两种隔离级别（加粗的）

读未提交：

**读已提交：**默认的，有可能发生不可重读读、幻读

可重复读：

**串行化：**三种读都不会发生。

切换隔离级别：

```sql
set transaction isolation level Serializable /*操作事务之前，设置事务的隔离级别位串行化*/
```





## **oracle中没有自增id，可以使用序列来模拟自增。**

```sql
create sequence myseq（序列名）; /*创建一个序列，默认从1开始自增*/

insert into student(myseq.nextval,'zs');/*插入数据时让通过序列让主键自增*/

/*创建一个序列，设置步长，初始自增位置*/
create sequence myseq1
increment by 2         /*步长为2*/
start with 1000;     /*从1000开始自增*/
```



使用序列有可能出现裂缝问题，当断电，异常或者回滚等情况时，会出现1，2，3，6，7，8。中间的4，5缺失了，就是裂缝问题。



```sql
alter sequence  myseq1  /*修改序列，原来使用的序列号不会变，从修改开始，后面使用此序列时就会用改变后的*/
increment by 1
start with  1000;

drop sequence 序列名;  /*删除序列*/
```





### 索引

创建索引

```sql
create index ind_name(索引名) on student (字段名name);
drop index ind_name;  /*删除索引*/

```





## PL/Sql

```sql
/*PL/Sql的语法模板*/
declare

 		定义变量、常量、光标、例外

begin

		....代码

end;

```



```sql
set serveroutput on ;   //开启打印
declare 
  --定义变量、常量等
  psex varchar2(20) :='男';
  pname varchar2(10) :='zs';
begin
--输出自定义的变量
  dbms_output.put_line('性别： '|| psex || ' 姓名： ' || pname);
end;
```



引用型变量：

​		pname    emp.ename%type ;  自定义变量pname的类型就是emp中ename字段的类型。如果将类型写死的话，下次改变ename字段的类型，那么就会出现错误。

记录型变量：

 		emp_info   emp%rowtype;   将emp表中一整行的数据都保存在emp_info这个自定义的变量中。使用emp_info.ename来调用某个字段。

