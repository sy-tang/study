

```bash
 docker run -d -p 6379:6379 --name myredis docker.io/redis
```



默认有16个数据库

databases 16       

 默认是使用第0个数据库

使用`select`切换，linux中使用select num切换



`keys *`查看当前库所有的key





`DBSIZE`    查看当前这个数据库的大小



`flushdb`   清除当前这个库中所有的值



`FLUSHALL` 清空所有库的值



redis 是单线程。基于内存操作，速度很快，使用c语言编写的





单线程为何还这么块？？

首先要明白：高性能的服务器不一定是多线程的；多线程不一定比单线程效率高。(多线程会有cpu上下文切换)

核心：redis所有的数据都放在内存中，所以使用单线程去操作效率就是最高的（多线程的上下文切换比较耗时）。对于内存系统来说，如果没有上下文切换，效率就是最高的！多次读写都在同一个cpu。





常用命令：

`get`

`set`

`exists `   key  该key是否存在

`expire`   key   10  设置一个key过期的时间，10秒

`ttl`     key  查看该key还有多久过期

`move`   key 1  移出这个key ，后带着一个1

`type`   key   查看key的类型

# String

`append`  key value  追加,如果当前key不存在，则新建这个key（相当于set key value）

`strlen`  key   获取key的长度

`incr`  key   给当前这个key的值+1

`decr`  key   减一

`incrby` key  step   给当前key的值加step

`decrby ` key step   减去step

**字符串截取**

`getrange`  key  start end  :字符串截取      end为-1时表示截取到字符串最末尾  左闭右闭

 **字符串替换**

`setrange` key offset  value   :offset表示偏移量（从那个位置开始替换）

```bash
#setex   :(set with expire)设置过期时间
例子：
setex key 30 xiaoming   #设置这个key30秒后过期
```

```bash
#setnx  :(set  if  not exist)不存在在设置
#例子  当这个mykey不存在时，就可以set成功，若mykey之前已经存在，则无法set成功，返回值为0
setnx mykey hello
```

**批量获取和设置值**

`mset` key value [key value...]  :同时批量设置

`mget` key  [key...]  ：同时批量获取

`msetnx`  :结合setnx ：批量设置，若其中某个key原先存在，则该命令所有的key都无法设置成功，返回0。该命令是原子性操作，要么都成功，有一个失败，就全失败

**设置对象**

方一：`set user : 1{name:zhangsan,age : 4} `  设置一个user:1对象，值为json字符

方二：`set user:{id}:{filed}`

​	![1586766876727](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586766876727.png)

`getset`   :先get再set：若不存在，则设置这个新的值，若存在，先获取，再给这个key设置一个新的值。

`cas`:比较再交换

**总结：String使用场景  计数器，统计多单位的数量，对象缓存存储**

# list

基本的数据类型，列表

list中所有的命令都是以l开头的

`lpush`:设置值：插入到列表的头部（从左边插入），`rpush`（从右边插入），插入到列表的尾部

`lrange` key start stop:获取值：

`lpop`:移出值：从头部移出第一个值（从左边移出）

`rpop`:从尾部移出最后一个值(从右边移出一个值)

![1586768208652](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586768208652.png)

`lindex`  key  index  :通过下标获取列表中的元素

`llen`:查看列表的长度

`lrem`   key  count  value :移除指定的值(从当前列表中移出count个指定的value，精确匹配)

`ltrim` key start stop: 截取一个列表  都是左闭又闭。该list只剩下被截取的值了。

`rpoplpush`   : 将一个列表中最后一个元素移出，并将该元素插入到另一个列表的头部

![1586769704141](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586769704141.png)

`exists`  list:查看list是否存在

`lset`  list index value: 修改list列表中下标为index的元素的值，（该list必须存在，该下标必须存在）

插入值

`linsert` key before|ahter pivot value:在某个值(privot)前或者后面插入一个值

![1586770544995](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586770544995.png)

总结：实际上是一个链表。在头或者尾部插入值效率比中间高。消息队列

# Set

set中值不能重复。  member

set中命令都是以s开头的。

`sadd` key value[valuse...]: 设置值

`smembers` key: 获取set中所有的元素

`sismember` key hello :判断key这个set中是否包含hello这个元素。存在则返回1

`scard` key  ：获取set集合中元素个数

`srem` key member :从set集合中移出一个元素（member）

`srandmember` key count: 从set集合中随机获取count个元素，不带count时表示随意获取一个

`spop`  key count:从set集合中随机移出count个元素，不带count时表示随意移出一个

`smove`  :将一个set集合中指定的元素移到另一个set集合中

**数字集合类**

**差集**

`sdiff`  set1 set2:求set1与set2的差集

![1586778252520](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586778252520.png)

**交集**(共同好友)

`sinter` set1 set2：求set1与set2的交集

![1586778315980](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586778315980.png)

**并集**

`sunion` set1 set2：求set1与set2之间的并集

![1586778407222](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586778407222.png)

# Hash

map集合  key-map。本质和string类似，map就是key-value的形式

Hash中命令是以h开头的

`hset`  key  field value:设置值

`hget` key field :获取值

`hmset`、`hmget`

`hgetall` key:获取key这个hash中所有的值，以key-value的形式

`hdel` key field:删除key这个hash中的field字段，field所对应的值也删掉了

`hlen` key :查看hash的长度。

`hexists`key field:查看指定的field字段在这个hash中是否存在

`hkeys` key:查看所有的field字段

**自增**

`hincrby`  myhash age 1  :age+1

`hincrby` myhash   age -1: age-1



`hsetnx`:如果不存在则可以设置，否则不能设置

**总结：可以存一些变更数据，user中的name，age等经常变动的信息的保存。适合对象的存储，string适合字符串存储。**

![1586780380884](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586780380884.png)

# Zset

有序集合，可排序，在set的基础上，增加了一个值。set中是k1 v1[v...]   zset中是k1   score  v1[score v ...]

Zset中命令以开头

`zadd`:设置值（可以同时添加多个值）

`zrange`：截取值

​	![1586780890016](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586780890016.png)

`zrangebyscore`   salary   min  max   :按照salary从小到大排序  -inf负无穷   +inf正无穷（可以根据范围来排序，这里是升序排序）

![1586781180842](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586781180842.png)

`zrevrange` :salary start stop:按照salary从大到小进行排序

![1586781954389](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586781954389.png)

`zrem`  :移出有序集合中的指定元素

`zcard`:获取有序集合中的个数

`zcount`  salary  min  max  ：获取指定区间的元素个数

总结：zset可以排序，存储班级成绩，工资表排序等。普通消息，重要消息，让消息带权重。排行榜应用实现。

# geospatial地理位置

![1586828616592](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586828616592.png)

官方文档：https://www.redis.net.cn/order/3685.html

`geoadd`  添加地理位置  （两极无法添加）

![1586828907639](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586828907639.png)

`geopos`:获取指定位置的经度纬度

![1586829499743](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586829499743.png)

`geodist`：返回给定的两个位置之间的绝对距离（可以指定距离单位，m、 km、mi、ft）

![1586829631765](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586829631765.png)



附近的人，通过半径来查询。（以自己位置为中心，搜索附近的其他人的位置）

`georadius`  china：city    110  30   1000 km:以自己当前位置（110，30）为中心，半径为1000km，在集合中搜索附近的城市。可以指定查询多少个。





城市之间的定位（以指定元素为中心，搜索范围内的其他元素）

`georadiusbymember`  以城市为中心，搜索指定范围内的城市

```bash
127.0.0.1:6379> GEORADIUSBYMEMBER china:city beijing 1000 km
1) "beijing"
2) "xian"
```

`geohash`   将二维的经纬度转换为一维的字符串

```bash
127.0.0.1:6379> geohash china:city beijing chongqing
1) "wx4fbxxfke0"
2) "wm5xzrybty0"
```

geo底层是zset实现，所以可以使用zset中的命令来移出我们设置的地理位置，查看我们设置的地理位置。可以使用zset来操作geo。zrange  zrem等



# hyperloglog基数统计

基数？不重复的元素

统计网页的访问量，（一个人多次访问也只算访问一次）

具有0.81%的错误率。如果不允许错误。则使用set集合进行基数统计

```bash
##基数统计
127.0.0.1:6379> pfadd myset a b c d e f g h i j   #添加元素
(integer) 1
127.0.0.1:6379> pfcount myset       #统计一共有多少个不同的元素
(integer) 10
127.0.0.1:6379> pfadd myset2 i j k m l s v n
(integer) 1
127.0.0.1:6379> pfcount myset2
(integer) 8
127.0.0.1:6379> pfmerge myset3 myset myset2   # 将后面两个合并为myset3
OK
127.0.0.1:6379> pfcount myset3     #统计合并之后一共有多少个不同的元素
(integer) 16
127.0.0.1:6379> 
```





# BitMaps

> 位存储

统计用户信息，活跃不活跃，登录，未登录（0，1，0，1，0），只有两个状态的就可以使用bitmaps

`setbit`

`getbit`

`bitcount`





# 事务

mysql  ACID    

事务本质：一组命令的集合，执行这个集合中的所有命令。一个事务中的所有命令都会被序列化，在事务执行过程中，会按照顺序执行。（一次性，顺序性，排他性）。一次性执行完所有命令，按顺序执行，不允许被打断。

原子性：同时成功，或同时失败。

**redis单条命令保证原子性，但是事务不保证原子性，事务没有隔离级别的概念，所有的命令在事务中，并没有直接执行，只有发起执行命令的时候才会执行，exec。**

reids的事务：

​		1、开启事务（`multi`）

​		2、命令入队（.....写入命令）

​		3、执行事务（`exec`）

​		`discard`  取消事务。

```bash
127.0.0.1:6379> MULTI     #开启事务
OK 
127.0.0.1:6379> set k1 v1   #命令入队
QUEUED      #入队成功
127.0.0.1:6379> set k2 v2   #命令入队
QUEUED 
127.0.0.1:6379> get k2       #命令入队
QUEUED
127.0.0.1:6379> set k3 v3   #命令入队...
QUEUED
127.0.0.1:6379> exec      #执行事务
1) OK                     #依次执行队列中所有的命令，执行的结果
2) OK
3) "v2"
4) OK                   #执行完成后，这个事务就没有了，下次要使用事务要开启事务
```

**放弃事务**

```bash
127.0.0.1:6379> multi    #开启事务
OK
127.0.0.1:6379> set k1 v1    #命令入队
QUEUED
127.0.0.1:6379> set k2 v2   #命令入队
QUEUED
127.0.0.1:6379> set k4 v4   #命令入队
QUEUED
127.0.0.1:6379> discard     #放弃（取消）事务，事务队列中的命令都不被执行了
OK
127.0.0.1:6379> get k4   #set k4没有执行，也就获取不到
(nil)
```

编译型异常：（命令格式错误时）事务中所有的命令都不会被执行

```bash
127.0.0.1:6379> MULTI   #开启事务
OK
127.0.0.1:6379> set k1 v1   #命令入队
QUEUED
127.0.0.1:6379> set k2 v2    #命令入队
QUEUED
127.0.0.1:6379> set k3        #命令格式错误
(error) ERR wrong number of arguments for 'set' command
127.0.0.1:6379> ket k4 v4    #命令格式错误
(error) ERR unknown command `ket`, with args beginning with: `k4`, `v4`, 
127.0.0.1:6379> set k5 v5   #命令入队
QUEUED
127.0.0.1:6379> set k6 v6    #命令入队
QUEUED
127.0.0.1:6379> exec       #执行事务，执行失败，队列中所有命令都不执行
(error) EXECABORT Transaction discarded because of previous errors.
```

运行时异常：事务中，一个命令运行时发生错误不影响其他命令的执行，该错误命令报错，其他命令正常被执行。（不保证原子性）。

```bash
127.0.0.1:6379> multi  #开启事务
OK
127.0.0.1:6379> set k1 hello    
QUEUED
127.0.0.1:6379> incr k1     #字符串是无法自增的
QUEUED
127.0.0.1:6379> set k2 v2
QUEUED
127.0.0.1:6379> set k3 v3
QUEUED
127.0.0.1:6379> set k4 v4
QUEUED
127.0.0.1:6379> exec   #执行事务
1) OK    #执行成功
2) (error) ERR value is not an integer or out of range  #自增执行失败
3) OK     #不影响其他命令被执行
4) OK
5) OK
```

**监控（watch）** 

悲观锁

- 很悲观，认为什么时候都会出现问题，无论做什么都加锁（效率低）

乐观锁

- 很乐观，认为什么时候都不会出现问题，所以不会上锁。更新数据时判断一下，在此期间是否有人修改过这个数据。
- 获取version
- 更新时候比较version

redis监视测试  (watch)。

正常结果

```bash
127.0.0.1:6379> set money 100   
OK
127.0.0.1:6379> set out 0       
OK
127.0.0.1:6379> watch money   #监视money对象
OK
127.0.0.1:6379> MULTI
OK
127.0.0.1:6379> decrby money 20   #减去20
QUEUED
127.0.0.1:6379> incrby out 20   ##增加20
QUEUED
127.0.0.1:6379> exec   #执行   事务正结果，
1) (integer) 80
2) (integer) 20
```

测试多线程修改值，使用watch可以当作redis 的乐观锁操作。**被watch监视的对象在进行操作之前，会判断这个值在监控期间是否被修改过**，被修改过则操作失败

```bash
127.0.0.1:6379> watch money   #监控money
OK
127.0.0.1:6379> multi       #开启事务
OK
127.0.0.1:6379> decrby money 100   #命令入队
QUEUED
127.0.0.1:6379> incrby out 100
QUEUED
127.0.0.1:6379> exec    #执行事务之前，已经在另一个窗口修改了值，所以当前事务执行失败
(nil)

################另一个窗口##############################
127.0.0.1:6379> set money 500   #在事务执行之前，先修改了值
OK
```

**如果事务执行失败（如上），则unwatch解锁，再watch监视最新的值，执行对应的事务。若执行之前又被修改，则重复此步骤。	**

**reids中使用watch监视来实现乐观锁**





# Jedis

1. 导入依赖

   ```xml
    <!-- https://mvnrepository.com/artifact/redis.clients/jedis -->
           <dependency>
               <groupId>redis.clients</groupId>
               <artifactId>jedis</artifactId>
               <version>3.2.0</version>
           </dependency>
   <!--        使用fastjson-->
           <!-- https://mvnrepository.com/artifact/com.alibaba/fastjson -->
           <dependency>
               <groupId>com.alibaba</groupId>
               <artifactId>fastjson</artifactId>
               <version>1.2.68</version>
           </dependency>
   ```

   

2、连接reids

![1586847910020](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1586847910020.png)

**3、常用API**





# Springboot中使用redis

springboot2.x后底层的jedis被替换成lettuce，

- jedis：采用的直连，多个线程操作的话是不安全的，如果想要避免不安全，需要使用jedis pool连接池。

- lettuce:底层使用netty，实例可以在多个线程中共享，不存在线程不安全的情况，可以减少线程数量，类似NIO模式。











