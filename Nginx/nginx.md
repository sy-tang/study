# 一、基本概念

1. **是什么、能做什么！**

高性能的http和反向代理web服务器。

2. **反向代理**

（1）正向代理？

​		 正向代理:是一个位于客户端和原始服务器(origin server)之间的服务器，为了从原始服务器取得内容，客户端向代理服务器发送一个请求并指定目标(原始服务器)，然后通过代理服务器向原始服务器转交请求并将获得的内容返回给客户端。客户端才能使用正向代理。 

浏览器需要配置代理服务器。

**（2）、反向代理？**

​		反向代理，客户端对代理时无感知的，因为客户端不需要任何配置就可以访问，我们只需要将请求发送到反向代理服务器，由反向代理服务器去选择目标服务器获取数据，再返回给客户端，此时反向代理服务器和目标服务器对外就是一个服务器。暴露的是代理服务器的地址，隐藏了真实服务器的IP地址。

3. **负载均衡**

增加服务器数量，将请求分发给各个服务器，原先单个服务器处理所有请求，现在由各个服务器共同分担这些请求，减少单个服务器的负载压力。

4. **动静分离**

为了加快网站解析速度。把动态页面和静态页面交给不同的服务器解析。加快解析速度，降低原来单个服务器的压力。

# 二、安装

## 1、linux中安装nginx

（1）先安装依赖。pcre  。在/usr/src下解压，进入解压后的文件，运行（./configure）, 

编译并安装（make && make install）。

​	pcre-config --version       //查看版本号

（2）安装zlib、openssl。

yum -y install make zlib-devel gcc-c++ libtoll openssl openssl-devel

（3）安装nginx

​	在/usr/src下解压nginx压缩包，进入解压后的文件，运行（./configure）, 

编译并安装（make && make install）。

​	**安装成功后在  /usr/local/下有nginx文件夹**

**nginx文件夹下的sbin中有启动文件**

![1587299075357](D:\TyporaDir\Nginx\Untitled.assets\1587299075357.png)

运行nginx。ps -ef | grep nginx查看进程。

![1587299223395](D:\TyporaDir\Nginx\Untitled.assets\1587299223395.png)

nginx下有conf文件夹，进入。打开nginx.conf文件，可以看到默认的端口80

![1587299459422](D:\TyporaDir\Nginx\Untitled.assets\1587299459422.png)

使用命令打开80端口的访问权限，在win中的浏览器中测试是否能够访问到nginx

firewall-cmd --zone=public --add-port=80/tcp --permanent

systemctl restart firewalld.service

开放80端口后可以访问成功

![1587300152744](D:\TyporaDir\Nginx\Untitled.assets\1587300152744.png)

## 2、nginx常用命令

>使用nginx命令前提是要在nginx文件下的sbin中
>
>常用命令
>
>查看版本号：./nginx -v
>
>启动nginx：./nginx
>
>关闭nginx：./nginx -s stop
>
>重新加载nginx：./nginx -s reload

## 3、nginx配置文件

>配置文件：  /usr/local/nginx/conf/nginx.conf
>
>

# 三、nginx配置实例

## 1、实例一、反向代理

​	**(1)安装tomcat**

​			在/usr/src下解压压缩文件，进入文件，进入bin文件夹，`./start.up`启动tomcat。

​			在tomcat文件夹下的logs文件中执行`tail -f catalina.out `查看日志文件。

​	开启端口

​		`firewall-cmd --zone=public --add-port=8080/tcp --permanent`

​		`systemctl restart firewalld.service`

​	查看防火墙开启的端口

​	`firewall-cmd  --list-all`

​	**(2)在win下配置host文件,配置域名映射的ip地址**

>C:\Windows\System32\drivers\etc\hosts
>
>hosts文件中最后加上  192.168.126.6       www.123.com
>
>www.123.com  映射到  192.168.126.6这个IP地址
>
>配置了后，通过http://www.123.com:8080/也可以访问到linux中的tomcat了

​	**(3)在nginx中配置代理**

> （1）server_name localhosts 修改为  server_name  192.168.126.6;
>
> （2）location / {
>             root   html;
>             proxy_pass  http://127.0.0.1:8080;   //加上，这样访问192.168.126.6:80就会访问这个8080端口
>             index  index.html index.htm;
>         }

（4）测试

> 回到sbin下  使用./nginx -s reload  重加载nginx
>
> 在浏览器端只要输入www.123.com就能访问到Linux中的tomcat了

实例二：

装备两个tomcat服务器，修改他们的端口号，一个为8080，一个为8081，分别在两个服务区的webapps下面创建edu和vod文件夹，里面放上a.html。启动两个服务器

修改nginx的配置文件，加上下面图片中的配置。重启nginx。

在主机的浏览器端执行如下操作

通过192.168.126.6：9001/edu/a.html  跳转到虚拟机的127.0.0.1：8080,访问a.html页面

通过192.168.126.6：9001/vod/a.html   跳转到虚拟机的127.0.0.1：8081，访问a.html页面

![1587348191302](D:\TyporaDir\Nginx\nginx.assets\1587348191302.png)



## 2、实例二、负载均衡

​	在两个服务器中添加相同的文件夹bbb和a.html

nginx中进行负载均衡配置

 1. 在http块中添加如下配置（添加tomcat访问的IP和端口）

    ![1587350882931](D:\TyporaDir\Nginx\nginx.assets\1587350882931.png)

	2. server中 server_name   192.168.126.6；
 	3. location中 ：proxy_pass   http://myserver;

通过不断地发送192.168.126.6:/bbb/a.html请求 ，将请求平均在8080端口和8081端口，达到负载均衡的效果。

**常见的分配策略**

1. 轮询（默认使用）

   每个请求按时间顺序逐一分配到不同的服务器。如果服务器宕机了，会自动剔除该服务器。

2. weight（权重）

   权重越高，被分配的客户端就越多。

3. ip_hash(直接加在upstream中就可以使用)

   每个请求按访问ip的hash结果分配，这样每个访客固定访问一个后端服务器，可以解决session问题。

   同一个session访问的服务器是同一个。（一个用户一开始访问的是8081，只要这个session没有变，那么访问的服务器就永远是8081）

4. fair（第三方，直接加在upstream中就可以使用）

   按后端服务器的响应时间来分配请求，响应时间短的优先分配请求。



## 3、实例三、动静分离

 准备工作：

​	在根目录下创建data文件夹，data文件夹中创建www和images文件夹，分别在里面放入aaa.html和a.jpg静态文件。

​	在nginx配置文件中进行配置。

​	![1587353585499](D:\TyporaDir\Nginx\nginx.assets\1587353585499.png)



测试：

浏览器中输入一个地址：192.168.126.6/images/(因为配置了autoindex on 所以会把静态资源列出来)

![1587353755231](D:\TyporaDir\Nginx\nginx.assets\1587353755231.png)

浏览器中输入一个地址：192.168.126.6/www/aaa.html

## 4、实例四、nginx配置高可用集群

2台nginx，keeplived   虚拟ip



# 四、Nginx执行原理