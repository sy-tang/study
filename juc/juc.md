> 进程，线程：eedd

​	进程是一个应用程序，线程是程序中的任务。

​	线程是进程的最小单位，一个进程有一个或多个线程。

> java默认两个线程（main，GC）

**java不能真正的开启线程，底层是用过调用native方法（本地方法）来执行start（）。**

java不能直接操作硬件，因为java是运行在虚拟机上的。

> 并发，并行

并发：多个线程操作同一个资源（cpu是单核时，快速交替模拟出来的多个线程）

并行：cpu多核，多个线程可以同时进行 （可以使用线程池提高效率）

并发编程的本质：充分利用cpu的资源。

> 线程状态  6个状态

```java
public enum State {
       //新生
        NEW,
    
        //运行
    
        RUNNABLE,
    
		//阻塞
        BLOCKED,

       //等待
        WAITING,

       //超时等待，过期不候
        TIMED_WAITING,

        //结束，终止
        TERMINATED;
    }
```

**wait/sleep的区别**

1. wait属于object，sleep属于Thread

​		常用TimeUnit（属于concurrent包下），不用sleep和wait。

```java
   //睡一天
    TimeUnit.DAYS.sleep(1);
```

2. wait会释放锁，sleep不释放锁（抱着锁睡着了）。

3. 范围不一样，wait只能在同步代码块中使用，sleep可以在任何地方使用。

# lock锁（重点）

​	synchronize和lock

lock的简单使用

```java
package com.project;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SaleTicket01 {

    public static void main(String[] args) {
        Ticket2 ticket = new Ticket2();
        new Thread(() -> {for (int i = 1; i <= 30; i++) ticket.sale();}, "a").start();
        new Thread(() -> {for (int i = 1; i <= 30; i++) ticket.sale();}, "b").start();
        new Thread(() -> {for (int i = 1; i <= 30; i++) ticket.sale();}, "c").start();

    }

}
class Ticket2{
    private int num=30;
    //创建锁
    private Lock lock=new ReentrantLock();

    public void sale(){
        lock.lock();//上锁
        try {
            //执行业务
            if (num>0){
                System.out.println(Thread.currentThread().getName()+"第"+num+"张");
                num--;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            lock.unlock();//解锁
        }
    }
}
```

## synchronize和lock的区别

1. synchronize是java关键字，lock属于java类
2. synchronize是可重入，非公平，不可中断的，lock是可重入，非公平，可以判断锁
3. synchronize不可判断获取锁的状态，lock可以判断是否获取到锁
4. synchronize适合锁少量的代码同步问题，lock适合锁大量的同步代码（更灵活）
5. synchronize会自动释放锁，lock不会自动释放锁，需要手动释放，如果不释放锁，会造成死锁
6. synchronize中当一个线程阻塞后，后面的线程会一直等待它，lock中，当一个线程阻塞后，后面的线程不一定会一直等待

# 集合类不安全

在多线程的情况下，使用list、set、map是不安全的。会有ConcurrentModificationException并发修改异常。

1. **list不安全**

   不安全原因： 并发争抢修改导致， 一个人正在写，另一个同学过来抢夺，导致数据不一致异常。并发修改异常 。

```java
package com.conllectionUnsafe;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/*
集合类不安全
测试list
 */
public class testList {

    public static void main(String[] args) {
//        List<String> list=new ArrayList<>();
//        List<String> list= Collections.synchronizedList(new ArrayList<>());
        List<String> list=new CopyOnWriteArrayList<>();
        /*
        ConcurrentModificationException并发修改异常
        解决：1.使用vector
              2.使用Conllections中的synchronizedList（）
              3.使用concurrent下的CopyOnWriteArrayList（写入时复制）
         */
        for (int i=1;i<=100;i++){
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(list);
            }).start();
        }
    }

}


//CopyOnWriteArrayList底层，使用lock。以add为例
 public boolean add(E e) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            Object[] elements = getArray();
            int len = elements.length;
            Object[] newElements = Arrays.copyOf(elements, len + 1);
            newElements[len] = e;
            setArray(newElements);
            return true;
        } finally {
            lock.unlock();
        }
    }

//解析：获取原来的数组，获取它的长度，然后复制一份长度加1的新数组，在新数组最后添加值，用这个新数组覆盖原来的数组。完成add操作。

//注意getArray和setArray
    final Object[] getArray() {
        return array;
    }

    /**
     * Sets the array.
     */
    final void setArray(Object[] a) {
        array = a;
    }


//array的的类型是Object，但是别volatile关键字标注了
/** The array, accessed only via getArray/setArray. */
    private transient volatile Object[] array;

//volatile是什么？
volatile是java虚拟机提供的一种轻量级同步机制。
    特点：
    保证可见性
    不保证原子性
    禁止指令重排

```

2.**set不安全**

```java
package com.conllectionUnsafe;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;

/*
测试set不安全
 */
public class TestSet {
    public static void main(String[] args) {
//        .ConcurrentModificationException并发修改异常
//        Set<String> hashSet = new HashSet<>();
//        Set<String> hashSet = Collections.synchronizedSet(new HashSet<>());
        CopyOnWriteArraySet arraySet = new CopyOnWriteArraySet<>();
        /*
        解决：
            1.Collections.synchroizedSet
            2.使用concurrent中的CopyOnWriteArraySet
         */
        for (int i=1;i<=100;i++){
            new Thread(()->{
                arraySet.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(arraySet);
            }).start();
        }

    }
}

//底层
  public boolean add(E e) {
        return al.addIfAbsent(e);
    }

//进入addIfAbsent(不存在时添加)
 public boolean addIfAbsent(E e) {
     //和list一样，getArray都是volatile维护的Object数组
        Object[] snapshot = getArray();
     	//判断是否存在
        return indexOf(e, snapshot, 0, snapshot.length) >= 0 ? false :
     		//不存在则执行添加
            addIfAbsent(e, snapshot);
    }

    /**
     * A version of addIfAbsent using the strong hint that given
     * recent snapshot does not contain e.
     */
    private boolean addIfAbsent(E e, Object[] snapshot) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            Object[] current = getArray();
            int len = current.length;
            if (snapshot != current) {
                // Optimize for lost race to another addXXX operation
                int common = Math.min(snapshot.length, len);
                for (int i = 0; i < common; i++)
                    if (current[i] != snapshot[i] && eq(e, current[i]))
                        return false;
                if (indexOf(e, current, common, len) >= 0)
                        return false;
            }
            Object[] newElements = Arrays.copyOf(current, len + 1);
            newElements[len] = e;
            setArray(newElements);
            return true;
        } finally {
            lock.unlock();
        }
    }

```

3.map不安全

```java
package com.conllectionUnsafe;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class TestMap {
    public static void main(String[] args) {
        //使用ConcurrentHashMap
        ConcurrentHashMap<String, Object> map = new ConcurrentHashMap<>();
        for (int i=1;i<=100;i++){
            new Thread(()->{
                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0,5));
                System.out.println(map);
            },String.valueOf(i)).start();
        }

    }
}
```





# **三个辅助类**

## **countdownlatch**

```java
package com.three;

import java.util.concurrent.CountDownLatch;

//递减，等待计数器归零，再向下执行
public class Count {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(6);
        for (int i=1;i<=6;i++){
            new Thread(()->{
                //递减。减一
                countDownLatch.countDown();
            }).start();
        }
        countDownLatch.await();//等待计数器归零，归零了就向下执行。
        System.out.println("执行完毕");
    }
}
```





## **cyclicbarrier**

```java
package com.three;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

//等待所有的线程到达同一个点，就执行
public class cyc {
    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7,()->{
            System.out.println("success");
        });
        for (int i=1;i<=7;i++){
            new Thread(()->{
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}

```





## **semaphore**

```java
package com.three;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Semaph {

    //每次只有3个线程允许
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);
        for (int i=1;i<=6;i++){
            new Thread(()->{
                try {
                    //获取到可以允许
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName()+":抢到车位");
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println(Thread.currentThread().getName()+":离开车位");
                } catch (InterruptedException e) {

                    e.printStackTrace();
                }finally {
                    //释放
                    semaphore.release();

                }
            }).start();
        }
    }
}

```





# **读写锁**

**ReentrantReadWriterLock**

**.Writer   .Read**

**写锁：（独占锁）：一个线程写的时候不允许别的线程去写，也不允许去读。**

**读锁：（共享锁）：读的时候不允许写，可以允许多个线程去读。**

**读写互斥。**

```java
package com.readwrite;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
/**
 * 独占锁（写锁） 一次只能被一个线程占有
 * 共享锁（读锁） 多个线程可以同时占有
 * ReadWriteLock
 * 读-读 可以共存！
 * 读-写 不能共存！
 * 写-写 不能共存！
 */
public class ReadWriteLockDemo {
    public static void main(String[] args) {
        MyCache myCache = new MyCache();
// 写入
        for (int i = 1; i <= 5 ; i++) {
            final int temp = i;
            new Thread(()->{
                myCache.put(temp+"",temp+"");
            },String.valueOf(i)).start();
        }
// 读取
        for (int i = 1; i <= 5 ; i++) {
            final int temp = i;
            new Thread(()->{
                myCache.get(temp+"");
            },String.valueOf(i)).start();
        }
    }
}
// 加锁的
class MyCacheLock{
    private volatile Map<String,Object> map = new HashMap<>();
    // 读写锁： 更加细粒度的控制
    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private Lock lock = new ReentrantLock();
    // 存，写入的时候，只希望同时只有一个线程写
    public void put(String key,Object value){
        readWriteLock.writeLock().lock();
        try {
            System.out.println(Thread.currentThread().getName()+"写入"+key);
            map.put(key,value);
            System.out.println(Thread.currentThread().getName()+"写入OK");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }
    // 取，读，所有人都可以读！
    public void get(String key){
        readWriteLock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName()+"读取"+key);
            Object o = map.get(key);
            System.out.println(Thread.currentThread().getName()+"读取OK");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            readWriteLock.readLock().unlock();
        }
    }
}
/**
 * 自定义缓存
 */
class MyCache{
    //单独使用volatile是不能保证读写互斥的，还是要使用读写锁才行。
//    volatile不保证原子性
    private volatile Map<String,Object> map = new HashMap<>();
    // 存，写
    public void put(String key,Object value){
        System.out.println(Thread.currentThread().getName()+"写入"+key);
        map.put(key,value);
        System.out.println(Thread.currentThread().getName()+"写入OK");
    }
    // 取，读
    public void get(String key){
        System.out.println(Thread.currentThread().getName()+"读取"+key);
        Object o = map.get(key);
        System.out.println(Thread.currentThread().getName()+"读取OK");
    }
}

```



# **阻塞队列（BlockingQueue）**

**blockingQueue**  

**使用场景： 多线程并发处理  ，线程池。（当获取不到所需要的资源时，线程等待，进入阻塞状态，当能获取资源时，唤醒阻塞的线程。类似生产者和消费者）**

**Deque：双端队列**

**abstractQueue ：非阻塞队列   blockingQueue：阻塞队列**

ArrayBlockingQueue

**四组API**

​	**1、抛出异常  ` add`（添加元素）、` remove`（移出元素）、`element`（查看队首元素）。添加超过队列长度时报错，队列为空时使用remove报错**

​	**2、不抛出异常，有返回值。`offer`（添加元素）、`poll`（弹出元素）`peek`（查看队首元素）、。添加超过队列长度时不报错，返回false。队列为空时使用poll返回null**

​	**3、阻塞等待**

​				**队列满时，如果再往队列中添加值就等待，会一直阻塞，直到有空位。**

​				**`put`（添加值），`take`（弹出元素）**

​				**队列空时，使用take会一直阻塞，因为没有值给他弹出。**

​	**4、超时等待**

​					**`offer（"d",2,TimeUnit.seconds）`,假如队列满了，d加不进去时，只等待2秒，等待超过2秒还没有位置，则退出**

​					**`poll（2，TimeUnit.seconds）`弹出时，等待超过2秒不能弹出值，就退出。**

```java
package com.blocking;

import org.junit.Test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Blcoking01 {


    //1、抛出异常  add,remove element
    @Test
    public void test01() {
        BlockingQueue bl = new ArrayBlockingQueue(3);

        System.out.println("bl.add(\"a\") = " + bl.add("a"));
        System.out.println("bl.add(\"a\") = " + bl.add("b"));
        System.out.println("bl.add(\"a\") = " + bl.add("c"));
//        System.out.println("bl.add(\"a\") = " + bl.add("d"));
        System.out.println("bl.element() = " + bl.element());
        System.out.println("========================");
        System.out.println("bl.remove() = " + bl.remove());
        System.out.println("bl.remove() = " + bl.remove());
        System.out.println("bl.remove() = " + bl.remove());
//        System.out.println("bl.remove() = " + bl.remove());
    }


    //2、不抛异常。offert（添加）  poll（弹出）   peek（返回栈首元素）
    //满了再添加返回false
    //空了再弹出返回null
    @Test
    public void test02() {
        BlockingQueue bl = new ArrayBlockingQueue(3);

        System.out.println("bl.add(\"a\") = " + bl.offer("a"));
        System.out.println("bl.add(\"a\") = " + bl.offer("b"));
        System.out.println("bl.add(\"a\") = " + bl.offer("c"));
//        System.out.println("bl.add(\"a\") = " + bl.offer("d"));
        System.out.println("bl.element() = " + bl.peek());
        System.out.println("========================");
        System.out.println("bl.remove() = " + bl.poll());
        System.out.println("bl.remove() = " + bl.poll());
        System.out.println("bl.remove() = " + bl.poll());
        System.out.println("bl.remove() = " + bl.poll());
    }


    //3、不抛异常,一直等待空位去添加。 put（添加）   take（弹出）
    @Test
    public void test03() throws InterruptedException {
        BlockingQueue bl = new ArrayBlockingQueue(3);

        bl.put("a");
        bl.put("b");
        bl.put("c");
        bl.put("d");

        System.out.println("========================");
        System.out.println("bl.remove() = " + bl.take());
        System.out.println("bl.remove() = " + bl.take());
        System.out.println("bl.remove() = " + bl.take());
//        System.out.println("bl.remove() = " + bl.take());
    }

    //4、不抛异常，超时等待，过期不候，带参数的offer  poll
    //等带了一定时间还没有空位，则返回false，添加失败
    @Test
    public void test04() throws InterruptedException {
        BlockingQueue bl = new ArrayBlockingQueue(3);

        System.out.println(bl.offer("a", 2, TimeUnit.SECONDS));
        System.out.println(bl.offer("b", 2, TimeUnit.SECONDS));
        System.out.println(bl.offer("c", 2, TimeUnit.SECONDS));
//        System.out.println(bl.offer("d", 2, TimeUnit.SECONDS));

        System.out.println("========================");
        System.out.println("bl.remove() = " + bl.poll(2,TimeUnit.SECONDS));
        System.out.println("bl.remove() = " + bl.poll(2,TimeUnit.SECONDS));
        System.out.println("bl.remove() = " + bl.poll(2,TimeUnit.SECONDS));
        System.out.println("bl.remove() = " + bl.poll(2,TimeUnit.SECONDS));

    }
}

```





## **同步队列**

**synchronousQueue（blockeingQueue的实现类）**

​	**只能存放一个元素。放入一个元素时，必须将它弹出后才能放其他的元素。**

​	**put()  ：添加值         take()：弹出值**

```java
//synchronousQueue 同步队列，只能放一个值，必须等待它被取出来能放其他的值。

    public static void main(String[] args) {
        SynchronousQueue<Integer> queue = new SynchronousQueue<>();
        new Thread(()->{
            try {
                System.out.println(Thread.currentThread().getName()+":put 1");
                queue.put(1);
                System.out.println(Thread.currentThread().getName()+":put 2");
                queue.put(2);
                System.out.println(Thread.currentThread().getName()+":put 3");
                queue.put(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t1").start();
        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName()+":take 1");
                queue.take();
                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName()+":take 2");
                queue.take();
                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName()+":take 3");
                queue.take();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t2").start();
    }

```





# **线程池**



**三大方法，7大参数、4种拒绝策略**

> **池化技术:**
>
> **事先准备好一些资源，需要用的时候直接拿取，用完后再还回来。**

**线程池、连接池、对象池、内存池...**

**线程池的优点：**

1. **降低资源的消耗。**
2. **提高响应的速度（不用不断的创建和销毁）。**
3. **方便管理。**

**线程可以复用，可以控制最大并发数，方便管理线程**





## **线程池的三大方法**

**使用ThreadPoolExcutors创建线程，而不使用Excutors创建**

**线程池的最大容量为队列大小+最大线程数量**

​	**Exectuors.newSingleThreadExecutor()   //单个线程**

​	**Executors.newFixedThreadPool(5)  //创建一个固定的线程池大小**

​	**Executors.newCachedThreadPool()   //可伸缩大小**

```java
package com.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreeMothed {
    public static void main(String[] args) {
        //pool-1-thread-1 ok：创建单个线程的线程池，只有这个线程去不断的执行任务
//        ExecutorService threaPool = Executors.newSingleThreadExecutor();
        //pool-1-thread-5 ok :最多就只有5个线程来执行任务
//        ExecutorService threaPool = Executors.newFixedThreadPool(5);
        //遇强则强，则以伸缩大小（可以随着任务的数量不断增加）  pool-1-thread-10 ok：可能会出现OOM问题。
        ExecutorService threadPool = Executors.newCachedThreadPool();
        try {
            for (int i = 0; i < 10; i++) {
                //使用线程池创建线程
                threadPool.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "ok");
                });
            }
        } finally {
            threadPool.shutdown();  //关闭线程池
        }


    }
}

```





## **7大参数**

**核心线程数量**

**最大线程数量**

**超时等待**

**时间单位**

**阻塞队列**

**线程工厂**

**拒绝策略**



## **四大拒绝策略**

**AbortPolicy  :  满了，不处理，抛出异常**

**CallerRunPolicy  ：满了，从哪个线程来的就回哪个线程执行**

**DiscardPolicy  ：满了，不处理，丢掉任务，不抛出异常**

**DiscardOldestPolicy  ：满了，尝试和最早获取的去竞争，不会抛出异常，如果没获取到线程则还是会丢掉任务。**

**总结：**

**最大线程数量如何定义（线程池的最大大小如何去设置）**

​	**1.cpu密集型。根据cpu核数决定，几核的cpu就定义为几。可以保证cpu效率最高。**

​	**2.io密集型。判断程序中十分耗IO资源的线程数量。大于这个数量即可（一般是这个数量的2倍）**

```java
package com.pool;

import java.util.concurrent.*;

public class SevenFields {
    public static void main(String[] args) {
        //使用ThreadPoolExecutor创建线程池
        //使用完线程池后一定要关闭

        //AbortPolicy()  不处理，抛异常
        //CallerRunsPolicy()  :哪个线程来的就会哪个线程去执行
        //DiscardPolicy() :队列满了，不抛异常，直接丢掉任务
        //                                                          ():不抛异常，尝试和最早的去竞争，若竞争不到则还是会丢掉任务
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
                2,
                5,
                3,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.DiscardOldestPolicy());

        try {
            for (int i=1;i<=9;i++){
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+":OK");
                });
            }
        } finally {
            threadPool.shutdown();
        }
    }


}

```







# **四大函数式接口**

**lambda表达式、链式编程、函数式接口、Stream流式计算**

> **函数式接口：只用一个方法的接口 如Runnable，Callable等    函数式接口主要用于简化编程模型**
>
> **只要是函数式接口，就可以使用lambda表示**

1. **function  函数型接口    泛型为T，T当作方法参数的类型，方法返回值类型为R（类似x-> y）**

2. **predicate  断定型接口    泛型为T，T当作方法参数的类型，方法返回值为Boolean值**

3. **consumer  消费型接口   泛型为T，T当作方法参数的类型，void方法，无返回值。消费了。（只有参数 输入，没有返回值）**
4. **supplier    供给型接口   泛型为T   方法返回值类型为T   （没有参数，只有返回值）**





# **Stream流式计算**

>**Stream是什么？    （并行流）**

​	**存储+`计算`**

**集合用于存储，计算交给流**

****



# **ForkJoin（分支合并）**

>**ForkJoin：并行执行任务，提高效率，大数据量非常明显。任务拆分（类似分治法）为小任务，多个不同的线程并行去处理，最后合并结果。**

**ForkJoin特点：工作窃取。一个线程执行完后，会去窃取其他还未完成任务的线程的任务来执行（偷别人的工作）。（双端队列）**

```java
package com.Stream;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.LongStream;

public class Fork extends RecursiveTask<Long> {


    private Long start;
    private Long end;
    private Long temp=10000L;

    public Fork(Long start, Long end) {
        this.start = start;
        this.end = end;
    }

    @Override
    protected Long compute() {
        if ((end - start)<temp){
            Long sum=0L;
            for (long i=start;i<=end;i++){
                sum+=i;
            }
            return sum;
        }else {
            long middle=(start+end)/2;
            Fork task1 = new Fork(start,middle);
            task1.fork();//拆分任务，将任务压入栈
            Fork task2 = new Fork(middle + 1, end);
            task2.fork();//拆分任务，将任务压入栈
            return task1.join()+task2.join();
        }
    }
}

class Test{
    // 普通程序员
    public static void test1(){
        Long sum = 0L;
        long start = System.currentTimeMillis();
        for (Long i = 1L; i <= 10_0000_0000; i++) {
            sum += i;
        }
        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+" 时间："+(end-start));
    }

    // 会使用ForkJoin
    /*
        1、创建ForkJoinPool
        2、创建任务 new Fork(0L, 10_0000_0000L)
        3、提交任务 submit
        4、获取返回结果  get

        方法中
        1、先递归
        2、fork：拆分任务，压入栈中
        3、执行加  join
     */
    public static void test2() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        ForkJoinTask<Long> task = new Fork(0L, 10_0000_0000L);//创建任务
        ForkJoinTask<Long> result = forkJoinPool.submit(task);// 提交任务
        Long sum = result.get();  //获取结果
        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+" 时间："+(end-start));
    }

    public static void test03(){
        long start = System.currentTimeMillis();
        long sum = LongStream.rangeClosed(0L, 10_0000_0000L).parallel().reduce(0, Long::sum);
        long end = System.currentTimeMillis();
        System.out.println("sum="+sum+" 时间："+(end-start));
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        test2();
//        test1();
        test03();
    }
}
```





# **异步回调**

> **future:**





14



# JMM

jmm是java内存模型，

​	线程解锁之前，必须将共享变量从工作内存刷回主内存

​	线程获取锁之间，必须从主内存中获取最新的共享变量到工作内存。

​	加锁和解锁是同一把锁



**内存交互的8种操作**

read   load   user  assign   store wirte  lock  unlock

 

# volatile

-  保证可见性
- 不保证原子性
- 禁止指令重排

1. 保证可见性

```java
package com.volatil;

import java.util.concurrent.TimeUnit;

//保证可见性
public class See {
    private volatile static int num;
    public static void main(String[] args) throws InterruptedException {

        new Thread(()->{
            //线程一直等待，当main线程将num改为1，此线程立即从主存中获取到新的值，退出while再向下执行
            while (num==0){
            }
         
        }).start();

        TimeUnit.SECONDS.sleep(3);

        num=1;
//        TimeUnit.SECONDS.sleep(1);
        System.out.println(num);
    }
}

```

2、不保证原子性

​		原子性：**操作不能被打扰，操作不可以被分割，同时成功或者失败**

```java
package com.volatil;

//不保证原子性

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class Demo {
    private volatile static int num;

    public static void add(){
        //num++在底层是可以被分割为3步的，它不是原子性操作
        num++;
    }

    public static void main(String[] args) {
        //按道理应该是2万，但是由于volatile不保证原子性，一些线程执行过程中会被
        //其他线程打断。
        for (int i=0;i<20;i++){
            new Thread(()->{
                for (int j=0;j<1000;j++){
                    add();
                }
            }).start();
        }
        while (Thread.activeCount()>2){  //java中默认开启了两个线程mian  gc
            Thread.yield();//线程让步
        }
        System.out.println(Thread.currentThread().getName()+":"+num);
    }

}

```

可以使用原子类解决原子性问题

```java
package com.volatil;

//不保证原子性

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.concurrent.atomic.AtomicInteger;

public class Demo {
    //原子类的Integer
    private volatile static AtomicInteger num=new AtomicInteger();

    public  static void add(){
//        num++;   //num++是一个原子性操作，它在底层被分为了三步  获取   加一  写回
        num.getAndIncrement();//原子性操作，直接在内存中++  CAS  native  自旋锁
    }

    public static void main(String[] args) {
        //按道理应该是2万，但是由于volatile不保证原子性，一些线程执行过程中会被
        //其他线程打断。
        for (int i=0;i<20;i++){
            new Thread(()->{
                for (int j=0;j<1000;j++){
                    add();
                }
            }).start();
        }
        while (Thread.activeCount()>2){  //java中默认开启了两个线程mian  gc
            Thread.yield();//线程让步
        }
        System.out.println(Thread.currentThread().getName()+":"+num);
    }

}

```

3、指令重排

**源代码-->编译器优化的重排--> 指令并行也可能会重排--> 内存系统也会重排---> 执行**

**处理器在进行指令重排的时候，考虑：数据之间的依赖**

volatile如何避免指令重排？

内存屏障：

1. 保证特定操作的执行顺序
2. 保证某写变量的内存可见性

**Volatile 是可以保持可见性。不能保证原子性，由于内存屏障，可以保证避免指令重排的现象产生**

​	

# 单例模式

1. 饿汉式

```java
//饿汉式
public class Hunger {
    private Hunger(){}

    private final static Hunger hunger=new Hunger();

    public static Hunger getInstance(){
        return hunger;
    }
}

```

2.懒汉式

```java
package com.single;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class LazyMan {
    private static boolean flag=false;
    private LazyMan(){
        synchronized (LazyMan.class){
            if (flag==false){
                //第一次创建对象
                flag=true;
            }else {
                throw new RuntimeException("不要试图破话单例");
            }
        }
    }

    private volatile static LazyMan lazyMan;

    public static LazyMan getInstance() {
        if (lazyMan == null) {
            synchronized (LazyMan.class) {
                if (lazyMan == null) {
                    lazyMan = new LazyMan();   //这里可能发生指令重排，所以可以使用volatile
                }
            }
        }
        return lazyMan;
    }


    public static void main(String[] args) throws Exception {
        LazyMan man = LazyMan.getInstance();

        Class<LazyMan> clazz=LazyMan.class;
        Field flag = clazz.getDeclaredField("flag");
        flag.setAccessible(true);
        flag.set(man,false);
        Constructor<LazyMan> constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);
        LazyMan lazyMan = constructor.newInstance();

        System.out.println(man==lazyMan);
    }
}

```

单例模式不安全，它可以被反射破坏。

可以使用枚举类，反射不能破坏枚举

```java
package com.single;

//这是一个枚举类
public enum  EnumLazy {
    LAZY_MAN;

    public  static EnumLazy getInstance(){
        return LAZY_MAN;
    }
}

```





# CAS

比较并交换

```java
package com.cas;

import java.util.concurrent.atomic.AtomicInteger;

public class Demo01 {
    public static void main(String[] args) {
        //设置了一个原子类的Integer值
        AtomicInteger atomicInteger = new AtomicInteger(2020);
		//设置期望2020，如果内存中atomicInteger的值满足期望，则修改为2021（比较并交换）
        System.out.println(atomicInteger.compareAndSet(2020, 2021));
        System.out.println(atomicInteger.get());
        
        System.out.println(atomicInteger.compareAndSet(2020, 2021));
        System.out.println(atomicInteger.get());
    }
}

```

CAS：比较当前工作内存中的值和主内存中的值，如果这个值是被期望的，那么则执行操作，如果不是就一直循环（自旋锁）

​	cas缺点：

1. 循环会耗时
2. 一次性只能保证一个共享变量的原子性
3. ABA问题



# ABA

什么是ABA？

A线程和B线程都操作同一个变量，他们都获取了变量，但B比A更快，在获取变量后修改该变量，然后再将该变量改回来，此时A中原来获取的变量已经发生了改变，虽然值还是一样的

```java
package com.cas;

import java.util.concurrent.atomic.AtomicInteger;

//ABA问题

public class Demo2 {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(2020);
        
        //期望  更新
        //========================捣乱的线程B=======
        //先改为2022，再改回来
        System.out.println(atomicInteger.compareAndSet(2020, 2022));
        System.out.println(atomicInteger.get());
        System.out.println(atomicInteger.compareAndSet(2022, 2020));
        System.out.println(atomicInteger.get()); 

        //===============期望执行的线程A==========
        System.out.println(atomicInteger.compareAndSet(2020, 3333));
        System.out.println(atomicInteger.get());

        //B线程执行成功了，A线程也执行成功了
        //但是实际上A是应该是不能成功的，因为A所获取的共享变量已经被B修改过，虽然最后的值
        //还是一样，但修改过就是和以前不以样了
    }
}

```

使用原子引用解决ABA问题

# 原子引用

原子引用的思想：乐观锁。 比较并交换时，比较对应的版本。

**AtomicStampedReference**