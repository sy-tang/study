虚拟机网络连接设置：

1、桥接模式：

虚拟机的网段和我们本机的属于同一个网段，假设一个教室有很多人都接了教室的网，这时候他们的本机都在同一个网段，假设他们都装了虚拟机，由于虚拟机的网段和本机的属于同一网段，192.168.0.1~255，除掉1和255，只有253多个，当学生过多时，容易造成ip冲突

![1585572693407](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585572693407.png)

2、NAT模式：网络地址转换方式，本机使用另一个网段与虚拟机的网段交互，linux可以访问外网（虚拟机可以通过本机的网段访问其他同网段的机器），不会造成IP冲突

![1585572829127](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585572829127.png)



 ![img](file:///D:\QQ\153513165\Image\C2C\K(T73415SWBQU4ZYALLA[)J.jpg) 

 ![img](file:///D:\QQ\153513165\Image\C2C\LTDPA83}JGT%9KK[_36FEF3.png) 





![1585714194868](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585714194868.png)



DNS地址

![1585714646690](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585714646690.png)

