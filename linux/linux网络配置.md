使用VMware软件

1、查看自己VMware的地址

​	win+r  cmd打开命令窗口

​	输出ipconfig，找到VMnet8，如下，

​	![1585732670473](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585732670473.png)

2、查看自己VMware的网关

![1585732927810](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585732927810.png)

![1585732980042](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585732980042.png)

直到了自己的ip和网关，就可以开始在linux中配置自己的IP和网关了



首先要知道：

1、虚拟机ip前三个位置需要和VMnet8的前三位一样

比如我的VMnet是192.168.126.1

那么linux中可以设置为192.168.126.24/192.168.126.8等

2、网关和VMware中的保持一致即可

操作：

1、命令行中输入下面命令（vi后面有一个空格）

​	vi /etc/sysconfig/network-scripts/ifcfg-ens33（centOS

7中）

/etc/sysconfig/network-scripts/ifcfg-eth0 (centOS6中)

​	1、设置为静态ip

​	2、开机自动启动网络

​	3、ip（IPADDR）、网关(GATEWAY)、子网掩码(NETMASK)按上面说的配置即可



![1585733776676](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585733776676.png)

4、设置DNS

vi /etc/resolv.conf    //编辑 resolv.conf文件

nameserver 114.114.114.114   //添加DNS地址

 

可以添加多个DNS地址，格式为：（可以添加自己本机网络的IPV4的地址）
nameserver xxx1.xxx1.xxx1.xxx1
nameserver xxx2.xxx2.xxx2.xxx2


常用的DNS地址:

  阿里  223.5.5.5  或者  223.6.6.6

  谷歌  8.8.8.8

  国内移动、电信和联通通用的DNS  114.114.114.114







chkconfig：

setup:查看系统服务

/etc/init.d/服务名称



\1. lsof -i:端口号

2.netstat -tunlp |grep 端口号



```shell
useradd  -d/-g  【目录/组】  用户名

userdel  用户名

usermod  

groupadd

groupdel

chown

chgrp

chmod
lsblk -f
df -h
du -h /目录

#进程ps
服务：守护进程。后台进程。service或者systemctl
chkconfig  --list   #对服务的启动级别设置
netstat -anp
netstat  -tunlp 
rpm -qa       #查询所有安装的rpm包
rpm -qi 包名  #查看包的信息
```

