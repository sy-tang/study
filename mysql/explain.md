# 索引



**索引是帮助mysql快速获取数据的数据结构。通过索引，mysql能够快速达到一个位置去查询数据，而不用扫描全表去查询数据。**

~~在mysql中索引的存储结构有两种：**BTREE**，**Hash**~~~~~~

innodb中索引存储结构是B+树。

**为什么使用树结构而不是用hash结构。hash索引时间复杂度为O（1），平衡二叉树索引时间复杂度为O（logn）,表面上看起来，是hash索引更具有优势，但是，在查询时，很多都是范围查询，而hash是无序的，树是有序的，hash无法定位范围数据。所以使用的就是树结构的索引。**

**对比**

**平衡二叉树：**它是一种查询效率很高的树，每个节点最多只有两个子节点，这样当节点很多时，会造成树的高度很大。每一个节点只存储一个数据，这样在以页为单位访问磁盘时，会造成多次磁盘IO。

**B树：**和平衡二叉树一样，节点是有序的，它每个节点可以存储多个值。当节点大小为叶的大小时，每次读取都能读取一个节点的数据，速度非常快。因为每个节点都可以存储多个值，所以树的高度也不会很大。

**B+树：**和B树的区别是，非叶子节点不存储数据，所有的数据都是存储在叶子节点中，叶子节点之间有链表关联。

IO次数比B树更少。

## **使用索引的优缺点**

​			优点：排序速度、查询速度快

​			缺点：更新，删除速度慢。占用空间。

**聚簇索引：**聚簇：数据按照一定顺序，一个一个紧密的排列在一起存储。mysql中，一般使用id作为聚簇索引。

​	优点：聚簇索引将索引和数据行保存在同一个B-Tree中，通过聚簇索引可以直接获取数据，相比于非聚簇索引还要进行二次查询，聚簇索引效率更高。

​	聚簇索引对于范围查询的效率很高。因为数据是按大小进行排序的

i~~nnodb中没有非聚簇索引~~。~~innodb中索引的数据存储是聚簇索引。~~

innodb中有且只有一个聚簇索引。

> InnoDB**聚集索引**的叶子节点存储行记录，因此， InnoDB必须要有，且只有一个聚集索引：
>
> （1）如果表定义了PK，则PK就是聚集索引；
>
> （2）如果表没有定义PK，则第一个not NULL unique列是聚集索引；
>
> （3）否则，InnoDB会创建一个隐藏的row-id作为聚集索引；

**非聚簇索引、二级索引（secondary index）：** 非聚簇索引，又叫二级索引（辅助索引）。二级索引的叶子节点中保存的不是指向行的物理指针，而是行的主键值。当通过二级索引查找行，存储引擎需要在二级索引中找到相应的叶子节点，获得行的主键值，然后使用主键去聚簇索引中查找数据行，这需要两次B-Tree查找。 

# explain命令：

查看执行计划，模拟执行sql语句，从而知道sql语句的执行过程。

**优化器：在不改变执行结果的情况下，对查询的sql语句进行调整。**

explain查出来的字段。

**id**:id个数表示这个sql中查询的趟数，id越大，优先级越高，越先被访问。id相同时，从上到下依次执行。

**select_type:**(查询的类型)

​	simple：简单的select查询

​	primary：查询中包含任何复杂的子部份，最外层标记为primary

​	derived：衍生查询。From中子查询的结果会被放入到一个临时表中。

​	subquery：使用了子查询。

​	dependt_subquery:依赖子查询，子查询是一个范围查询，结果有多条

​	uncacheable_subquery:不可缓存的子查询，比如子查询中使用了系统变量。

​	union：使用了union连接了select

**table**：查询了哪个表

partitions：代表分区表中的命中情况，非分区表，该项为null

**type：**表的连接类型    访问类型 

​	system：表中只有一行

​	const：通过索引一次就命中了

​	qe_ref：使用了唯一索引

​	ref：使用了索引，但不是唯一索引

​	range：使用了范围查询

​	index：~~使用了索引，但是没有通过索引过滤。一般表示使用了覆盖索引~~。只遍历索引树。full index scan

​	all：遍历全表  full table scan

**possible_keys：**可能使用的索引

**key：**真正使用到的索引

**key_len:**索引命中的长度

**rows：**整个sql物理扫描的行，越小越好

**ref：** 列与索引的比较 

**extra**：

​	using filesort：使用了order by排序，但是没有使用到索引，造成了文件排序

​	using temporary：使用了group by，但是没有使用到索引，使用临时表来存储结果集

​	using join buffer：使用了连表查询，没有使用到索引，使用缓冲区来存储结果集

​	impossible where；where条件永远获取不到值。

​	using  where

​	using index    ：使用了覆盖索引。

**索引失效的情况：**

- 不符合最佳前缀法则，第一个索引就无法命中，导致后面的索引也失效了
- 索引列使用了函数
- 索引列使用了范围查询，会导致右边的索引失效。
- 使用了不等于 <>
- 使用了is not null。
- like中使用了百分号开头，索引失效
- 值与字段类型不匹配。

**连表查询时，表之间会有角色的区别。前者为驱动表，后者为被驱动表。驱动表设置索引是不会生效的，所以应该将小表或者不能设置索引的表作为驱动表，将可以使用索引的表作为被驱动表。需要注意的是，在mysql中，对于inner join，会自动选择驱动表和被驱动表。我们可以使用直连指定驱动和被驱动（straight_join）.**



在使用order by时，直接设置索引是不会生效的。

需要使用过滤条件-----------------------------**无过滤，不索引**

索引顺序和排序顺序要对应---------------------**顺序错，必排序**（using filesort）

升序和降序要方向要相同-------------------------------**方向反，必排序**（using filesort）





使用group by和使用order by类型，只不过它不用过滤条件索引也会生效。



**覆盖索引：**

​	**覆盖索引就是包含所有查询的字段，mysql通过索引就可以获取查询的数据，因而不需要读取数据行。**

​	优点：

- 索引的大小远小于数据 大小，通过索引获取查询的数据，大大减少对数据的访问量

- 索引按顺序存储，对于IO密集型的范围查询会比随机从磁盘中读取每一行数据的IO要少。

-  避免对主键索引的二次查询。 

**回表查询：**

 https://www.cnblogs.com/myseries/p/11265849.html 

**如果出现using index condition就是二级索引回表**	

 先定位主键值，再定位行记录， 



using  filesort (文件排序)的两种排序：

​	单路排序(速度快)：

​	双路排序：

​		mysql4.1之前是使用双路排序，







```mysql
#创建数据库，如果不存在
CREATE DATABASE [IF NOT EXISTS] shcool
DROP DATABASE IF EXISTS	shcool;
user school   #使用数据库
、、
```

modify可以修改字段类型和约束，change不能

change可以修改字段名，modify不能。

**DDL**

**DQL**

**DML**

**DCL**

singleobject（Map类型）:{beanName:bean}

objectFactorys:{beanName:objectFactory}

objects:{beanName:Object}

  ![img](https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1589876298278&di=a6b46ccbbfefb4e7075b006599729f39&imgtype=0&src=http%3A%2F%2Fwww.west.cn%2Finfo%2Fupload%2F20180618%2Fcfmfobj0fri.jpg)  

```mysql
#concat函数  拼接
SELECT CONCAT('个人信息',`s_name`) FROM t_stu;

#distinct去重，看一共有几个年龄段
SELECT DISTINCT s_age  FROM t_stu;

#查看每个年龄的人数。
SELECT  s_age,COUNT(*) AS 人数  FROM t_stu GROUP BY s_age;

#查询在年龄18到21之间的学生
SELECT  * FROM t_stu WHERE s_age BETWEEN 18 AND 21;

#查询在年龄不在18到21之间的学生
SELECT  * FROM t_stu WHERE NOT s_age BETWEEN 18 AND 21;

#模糊查询%   任意个
SELECT * FROM t_stu WHERE `s_name` LIKE 'xiao%';

#模糊查询_   一个
SELECT * FROM t_stu WHERE `s_name` LIKE '小_';

#模糊查询_   一个
SELECT * FROM t_stu WHERE `s_name` LIKE '_张';

#查部门id不为空的
SELECT * FROM t_stu WHERE `deptid` IS NOT NULL;

#查询为空的
SELECT * FROM t_stu WHERE `deptid` IS  NULL;

#in
SELECT * FROM t_stu WHERE id IN (1,2,3);

#子查询、联合查询
SELECT * FROM t_stu ss,(SELECT * FROM t_dept) dd  WHERE ss.`deptid`=dd.id;
SELECT * FROM t_stu ss INNER JOIN t_dept dd ON ss.`deptid`=dd.`id`;

#===============================================================================================================

#7种查询
#inner join   查交集
SELECT * FROM t_stu ss INNER JOIN t_dept dd ON ss.`deptid`=dd.`id`;

#left join    以左边为准。
SELECT * FROM t_stu ss LEFT JOIN t_dept dd ON ss.`deptid`=dd.`id`;

#left join    以左边为准。只查与左边有关的，把交集部分去除(找出不属于任何部门的学生)
SELECT * FROM t_stu ss LEFT JOIN t_dept dd ON ss.`deptid`=dd.`id` WHERE dd.id IS NULL;

#right join   以右边为准
SELECT * FROM t_stu ss RIGHT JOIN t_dept dd ON ss.`deptid`=dd.`id`;

#right join   以右边为准  把交集部分去掉（查询没有部长的部门）
SELECT * FROM t_stu ss RIGHT JOIN t_dept dd ON ss.`deptid`=dd.`id` WHERE ss.`id` IS NULL;

#练习 查出每一个学生所在的部门
SELECT ss.`s_name`,dd.`dept_name` FROM t_stu ss LEFT JOIN t_dept  dd ON ss.`deptid`=dd.`id`; 
#查出每个部门的部长
SELECT dd.`dept_name`,ss.`s_name` FROM t_stu ss RIGHT JOIN t_dept dd ON dd.`head_id`=ss.`id`;
#查询每个部门年龄大于20的人
SELECT dd.`dept_name`,ss.`s_name`,ss.`s_age` FROM t_stu ss INNER JOIN t_dept dd ON ss.`deptid`=dd.`id` AND ss.`s_age`>=20;
#查出每个部门的部长以及他所在的学院
SELECT dd.`dept_name` 部门,ss.`s_name` 姓名,yy.`name` 学院 FROM t_stu ss RIGHT JOIN t_dept dd ON dd.`head_id`=ss.`id` LEFT JOIN t_yuan yy ON ss.`y_id`=yy.`id`;

#自连接查询
SELECT a.`typename` 父级,b.`typename` 子级 FROM
`type` a, `type` b
WHERE a.`id`=b.`pid`;


#===========================================================================================

#降序 desc   升序 asc
SELECT * FROM t_stu ORDER BY s_age DESC;

SELECT * FROM t_stu ORDER BY s_age ASC;

#分页
SELECT * FROM t_stu LIMIT  5;

#======================================常用函数========================================
#绝对值
SELECT ABS(-8);

#向上取整
SELECT CEILING(9.3);
SELECT FLOOR(9.3);
#返回随机数
SELECT RAND();
SELECT CHAR_LENGTH('helloworld');#字符串长度
SELECT CONCAT('he','llo'); #合并字符串
SELECT NOW();


SELECT YEAR(NOW());


#聚合函数
#count\sum\avg\max\min\
SELECT COUNT(deptid) FROM t_stu ;  #忽略null。为null的不会计算
SELECT COUNT(1) FROM t_stu ;
SELECT COUNT(*) FROM t_stu ;


SELECT SUM(deptid) FROM t_stu; //求和

#求每个部门的平均年龄，最大年龄，最小年龄   group by  having:分组条件。  每个部门，每个学院等等就可以使用排序
SELECT FLOOR(AVG(ss.s_age)),MAX(ss.s_age),MIN(ss.s_age),dd.dept_name FROM t_stu ss RIGHT JOIN t_dept dd ON ss.deptid=dd.id  GROUP BY dd.id HAVING AVG(ss.s_age)>20;
#where条件后的表达式不允许聚合函数。


###############################事务#######################################
SET autocommit =0;  #关闭自动提交
SET autocommit =1;  #开启自动提交（默认）

#开启事务
START TRANSACTION  \BEGIN;

#具体操作
INSERT xxx;
INSERT xxx;

#保存点(存档)
SAVEPOINT;

#关闭回滚事务
COMMIT \ ROLLBACK;

ROLLBACK TO SAVEPOINT;  #回滚到某个保存点












#sql版本 
SELECT VERSION();




##############sql编程
DELIMITER $$     ##重定义输出符
 CREATE FUNCTION  mock_data()  ##创建函数
 RETURNS INT      ##返回一个int值
 BEGIN           ##函数开始
 DECLARE num INT DEFAULT 1000000;    ##定义变量
 DECLARE i  INT DEFAULT 0;
	WHILE i<num DO                ##循环
	#插入数据
	INSERT	 INTO `app_user`(`name`,`email`,`phone`,`gender`,`password`,`age`)
	VALUES
	(CONCAT('用户',i),'153513165@qq',FLOOR(CONCAT('18',RAND()*(999999999-100000000)+100000000)),FLOOR(RAND()*2),UUID(),FLOOR(RAND()*100));
	
	  SET i=i+1;                   ##i自增
	END WHILE;
	RETURN i;
 
 END;             ##函数结束
```

索引分类

**帮助mysql高效获取数据的数据结构**

主键索引

唯一索引

普通索引

全文索引



mysiam和innodb的区别

- mysiam中不支持事务，没有隔离级别、支持表锁，不支持行锁
- innodb中支持事务，有隔离级别，支持表锁和行锁
- mysiam中的索引使用前缀压缩技术让索引变的更小
- innodb中使用数据原格式来进行存储
- mysiam中索引通过数据的物理位置引用被索引的行
- innodb中通过主键id引用被索引的行

**statement对象：用于向数据库发送sql语句的。**

prepareStatement：预编译：有效防止sql注入。

|  对比  |           myISAM           |       INNODB       |
| :----: | :------------------------: | :----------------: |
|  外键  |           不支持           |        支持        |
|  事务  |           不支持           |        支持        |
| 行表锁 |          支持表锁          |       都支持       |
|  缓存  | 只缓存索引，不缓存真实数据 | 缓存索引和真实数据 |
|        |                            |                    |







**学习网站**

![1589977087345](D:\TyporaDir\mysql\explain.assets\1589977087345.png)

水平的双向指针。（理论上的）实际上是单向的指针。

## **B+树：**

**叶子节点包含了所有元素。所有元素都进行了排序。叶子节点之间有指针指向。**

![1589977459095](D:\TyporaDir\mysql\explain.assets\1589977459095.png)

**操作系统中：**

页：逻辑单位   一页的大小：4kb

每次IO取一页的数据。

**innodb中**：

页的大小为：16kb



**数据链**----->**页目录(槽)**(使用二分查找来找到要查询的数据所在的槽。空间换时间)

![](D:\TyporaDir\mysql\explain.assets\1589979280711.png)

最小记录所在的分组只能有一条;最大记录所在的分组只能是1～8条之间;剩下的分组4

![1589979864160](D:\TyporaDir\mysql\explain.assets\1589979864160.png)

有可能出现由页组成的长链表。如何解决？

使用一个区域来存储主键值和页。如下：（也就是b+树结构了）

![1589980213675](D:\TyporaDir\mysql\explain.assets\1589980213675.png)

![1589980348749](D:\TyporaDir\mysql\explain.assets\1589980348749.png)

所有的数据都存在叶子节点。索引就是数据，数据就是索引。

**每个索引都有一个索引树。**







![1589891831334](D:\TyporaDir\mysql\explain.assets\1589891831334.png)

指针压缩：jdk.6后默认开启。

 **XX:+/-UseCompressedOops 关闭指针压缩**

**空对象：**一个类没有普通属性（没有实例数据），这样的类就是空对象。开启指针压缩时占16， 

**mark word =8字节**

**类型指针：开启指针压缩时4，不开启时8**

16=8+**4（类型指针）**+0（数组长度）+0（实例数据）+4（填充对齐，为了整个值是8的倍数）。所有的对象都是8字节对齐。为了性能更高。

未开启时占16：

16=8+**8**+0+0+0（对齐）

**普通对象**

开启指针压缩时占24=8+4+0+4+4+4

不开启时占24=8+8+0+4+4+0

指针压缩开启后8->4

怎么存储的？做了什么？

一个oop能支持的最大堆空间是多少？

2的35次方。35=32+3。

如何扩容：16字节对齐。





## 4次挥手

客户端和服务器一开始都处于已建立连接的状态。

1.2.当客户端没有要发送给服务端的数据了，那么客户端就会给服务端发送一个连接释放报文段，表示想要关闭从客户端到服务端的连接。客户端发送连接释放报文段后就进入到fin-wait-1的状态。服务端接收到客户端的报文段后，会给客户端发送确认，自己进入到close-wait状态。客户端接收到确认后，就会进入到fin-wait-2的状态。

此时，客户端到服务端的连接就已经关闭了，客户端不能给服务端发送消息。但是服务端到客户端的连接没有关闭。服务端可以发送消息给客户端，客户端还能接收服务端的消息。也就是处于一个**半连接**的状态。



3.4.当服务端没有要发送给客户端的数据时。服务端会给客户端发送连接释放报文段，进入last-ACK状态。客户端接收到这个报文段后，必须确认这个报文段。客户端给服务端发送确认后，进入到time-wait状态。服务端接收到确认后，就进入到close状态了。

需要注意的是，服务端接收到确认就直接进入到close状态，而**客户端有一段time-wait状态**，这个状态大概**持续2MSL(4分钟)**。客户端需要**等待time-wait状态结束后，才能到close状态**。**所以服务端要比客户端先close**。



### 为什么要有time-wait这个状态，或者为什么要等待4分钟客户端才会进入close阶段呢？

解：**因为最后一次确认有可能会丢失，当确认丢失时，服务端会再次发送连接释放报文段，如果没有这段等待时间，那么此时客户端是close状态的，这个状态不能接收连接释放报文段，也不能发送确认，这会导致服务端因收不到确认而无法正常close**。

还有可以防止失效的请求报文段出现在本次连接中。当客户端发送最后一次确认报文段后，经过4分钟，可以使本连接持续时间内所产生的所有报文段从网络中消失。



## 三次握手

为什么要三次握手，而不能是两次握手？

为了防止已失效的连接请求报文段又传送到了服务端。客户端发送给服务端的连接请求报文段有可能在某个网络节点中滞留，如果滞留的时间太长，导致在连接释放后才到达服务端，如果只有两次握手，那么服务端接收到失效的连接后，会给客户端发送确认报文段，发送确认后，连接就建立了，而客户端并没有发送连接请求，就不会理睬服务端，一直不会给服务端发送数据，这就导致了服务端资源的浪费。