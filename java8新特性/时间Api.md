# 一、概述

GMT：格林威治标准时间。UTC：时间标准时间

**一些关键的类：**

Instant：时间戳

localDate:不包含具体时间的日期：如2018-01-12

LocalTime：不包含日期的时间，如12:26:23

LocalDateTime:包含时间和日期

ZonedDate：包含完整日期时间。

Timezones:时区



**新的日期时间特性：**

不变性：新的日期时间API中，所有的**类是不可变的，线程安全**

关注点分离：日期时间和及其时间分离

清晰：

实用操作：有一写现成的写好的工具方法。

TemPoralAdjuster：转换器。更方便的操作时间





# 二、常用类的介绍

## 2.1 Instant和Clock

```java
  Instant now = Instant.now();
        System.out.println(now);  //输出GMT类型的日期时间
        System.out.println(now.getEpochSecond());   //获取从原点到现在的秒数
        System.out.println(now.getNano());   //获取纳秒数

        //获取毫秒数
        System.out.println(now.toEpochMilli() );
        System.out.println(System.currentTimeMillis());
```

plus，加

minus：减

isAfter(time):时间是不是在time这个时间之后

isBefore(time):时间是不是在time时间之前 。



**parse方法**:parse,将GMT时间字符串转成Instant对象

```java
 Instant parse = Instant.parse("2020-06-04T23:48:54.178Z");  //parse,将GMT时间字符串转成Instant对象
        long epochSecond = parse.getEpochSecond();   //1591314534秒
        System.out.println(epochSecond);
```



**until方法**

```java
Instant parse = Instant.parse("2020-06-04T23:48:54.178Z");  //parse,将GMT时间字符串转成Instant对象
        long epochSecond = parse.getEpochSecond();   //1591314534秒
        System.out.println(epochSecond);
        Instant parse1 = Instant.parse("2020-02-06T23:48:54.178Z");
		
		//119天  util：可以计算两个时间之间的间隔
        System.out.println(parse1.until(parse, ChronoUnit.DAYS));
```



**Duration.between（starttime,endtime）;**

**计算两个时间的时间差**

```java
Duration between = Duration.between(parse1, parse);
        System.out.println(between.toDays());  //119
```





**Clock:**提供**某个特定时区**的时间日期

```java
   Clock clock = Clock.systemUTC();  //utc时区
        Clock clock1 = Clock.systemDefaultZone();//操作系统当前所在的时区

        System.out.println(clock);  //SystemClock[Z]
        System.out.println(clock1);//SystemClock[Asia/Shanghai]上海时间

        System.out.println();
        System.out.println(LocalDateTime.now(clock)); //2020-06-05T00:13:07.266
        System.out.println(LocalDateTime.now(clock1));//2020-06-05T08:13:07.266  上海时间
```



## 2.2LocalDate本地日期

**只有年月日，没有时间**

```java
      LocalDate now = LocalDate.now();
        System.out.println(now);   //2020-06-05

        LocalDate time = LocalDate.of(2020, 06, 04); //输入指定的年月日，转成LocalDate对象
        System.out.println(time); //2020-06-04

        System.out.println(now.plusDays(3));  //在now这个时间上加几天  2020-06-08
        System.out.println(now.plusMonths(3));  //在now这个时间上加几个月  2020-09-05

        System.out.println("------------");
        System.out.println(now.plus(Period.of(1, 2, 3)));//2021-08-08，加1年，加2个月，加3天


        //判断是不是闰年
        System.out.println(now.isLeapYear());  //true
        
   //plus,minus用于基本运算，with***基本都是用来修改的，但修改的不是原有的时间，而是返回一个新的时间。
```

## 2.3localTime和LocalDateTime

