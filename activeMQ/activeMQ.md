# 一、概述

作用：解耦、异步、削峰





# 二、基本操作

## 1.队列Queue

队列：一对一

1、activemq的基本开发流程

消息的生产者

```java
public static final String  ACTIVEMQ_URL="tcp://192.168.126.6:61616";

    public static void main(String[] args) throws JMSException {
        //1.创建连接工厂
        ActiveMQConnectionFactory activeMQConnectionFactory=new ActiveMQConnectionFactory(ACTIVEMQ_URL);
        //2.建立连接connection。启动
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
        //3.创建session
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        //4.创建queue
        Queue queue01 = session.createQueue("queue01");
        //5.创建消息的生产者,将生产的消息放到队列中
        MessageProducer producer = session.createProducer(queue01);

        //6.创建消息，并发送
        for (int i = 1; i <=10 ; i++) {
            Message message = session.createTextMessage("msg----" + i);
            producer.send(message);
        }
        //关闭连接
        producer.close();
        session.close();
        connection.close();
        System.out.println("*********");
```

消息的消费者：

```java
public static final String ACTIVE_URL = "tcp://192.168.126.6:61616";

    public static void main(String[] args) throws JMSException, IOException {
        //1.创建连接工厂
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVE_URL);
        //2.创建连接并启动
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue queue01 = session.createQueue("queue01");
        final MessageConsumer consumer = session.createConsumer(queue01);

        //方式一：同步阻塞
//        while (true) {
//            TextMessage receive = (TextMessage) consumer.receive();  //receive(),一直等待队列中有数据。同步阻塞
//            if (receive != null) {
//                System.out.println("消费者消费的数据是：" + receive.getText());
//            } else {
//                break;
//            }
//        }

        //方式二：使用监听器监听队列
        consumer.setMessageListener(new MessageListener() {
            public void onMessage(Message message) {
                if (null != message && message instanceof TextMessage){
                    TextMessage textMessage= (TextMessage) message;
                    try {
                        System.out.println("MessageListener-----:"+textMessage.getText());
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        System.in.read(); //保证控制台不灭
        consumer.close();
        session.close();
        connection.close();
```

## 2.主题Topic

主题：一对多

基本操作和队列一样。

注意：使用主题后，要先启动订阅者，再来发送消息。否则发送的消失就是废消息。

## 总结：

queue是一对一，topic是一对多。~~消费者获取到了数据后要签收~~

queue会将数据存储起来，topic无状态。

topic在没有订阅者时，发送的消息会丢弃。

# 三、JMS

java  message service

![ javaee是什么](D:\TyporaDir\activeMQ\activeMQ.assets\1591692542552.png)

java消息服务：是指两个应用程序之间进行异步通信的API。它为标准消息协议和消息服务提供了一组接口。包括创建、发送、读取等。用于支持java应用程序开发。在javaEE中，当两个应用程序使用JMS进行通信时，他们之间并不是直接相连的，而是通过一个共同的消息收发服务组件关联起来以达到解耦/异步/削峰的效果。

## 1.JMS组成结构和特点

**组成：**

- JMS provider：具体落地 的实现；如activeMQ，kafka等
- JMS producer：消息的生产者，创建和发送消息
- JMS consumer： 消息的消费者，接收和处理消息
- **JMS message：**
  - **消息头**
  - **消息属性**
  - **消息体**





**常用的消息头：**

**JMSDestination**:消息发送的目的地

**JMSDeliveryMode**：持久或非持久

​	持久：一条持久消息在JMS出现故障时，并不会丢失，它会在服务器恢复之后再次传递

​	非持久：最多会被传送一次，JMS服务器出现故障，该消息将永远丢失。

**JMSExpiration**：消息过期时间。**默认是永不过期**。如果timeToLive的值为0，则JMSExpiration被设置为0，表示该消息永不过期。消息过期后会被清除

**JMSPriority：**优先级，0-9；0-4：普通消息，5-9：加及消息。默认是4级。

**JMSMessageID：唯一识别每个消息的标识，它由MQ产生。**





**消息体：**

- 封装具体的消息数据
- 5种消息体格式
  - **TextMessage**：普通字符串消息，包含一个string
  - **MapMessage**:一个map类型的消息，**key为String**类是，**值为java的基本类型**
  - BytesMessage：二进制数组消息，包含一个byte[]
  - StreamMessage:java数据流消息，用标准流操作来顺序的填充和读取
  - ObjectMessage：对象消息，包含一个**可序列化的java对象**
- **发送和接收的消息体类型必须一致**





**消息属性：**

- 如果需要除消息头字段以外的值，那么可以使用消息属性
- 作用：识别、去重、重点标注等
- 它是以属性名和属性值对的形式制定的，可以将属性看成是消息头的扩展。



```java
 //6.创建消息，并发送
        for (int i = 1; i <=10 ; i++) {
            Message message = session.createTextMessage("msg----" + i);
            producer.send(message);
            MapMessage mapMessage = session.createMapMessage();  //Map类型的消息体
            mapMessage.setJMSExpiration(3600L);  //消息头，设置过期时间
            mapMessage.setString("k1","mapMessage---v1");
            mapMessage.setStringProperty("hh","vip");  //消息属性
            producer.send(mapMessage);
        }
```



## 2.JMS消息的可靠性

如何保证消息的可靠性：

- 持久
- 事务
- 签收



持久化：消息的**生产者**可以设置消息是否持久化。若持久化：则服务器宕机后，消息不会丢失。非持久化：服务器宕机后，消息丢失。

### 2.1在Queue中的持久

**在队列中，默认是开启了持久化的。**

NON_PERSISTENT:非持久化

```java
MessageProducer producer = session.createProducer(queue);  //通过session创建生产者
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);   //生产者设置为不持久化，该生产者发送的消息都是不持久化的
```



PERSISTENT:持久化

```java
 MessageProducer producer = session.createProducer(queue);  //创建生产者
        producer.setDeliveryMode(DeliveryMode.PERSISTENT);  //持久化，当MQ宕机后，数据不会丢失
```

### 2.2在topic中的持久

在topic中，非持久化是没有意义的，因为订阅者先订阅，此时不管发布者发送的消息是否持久化，订阅者都能收到。

**在topic中使用持久化：**

**订阅者：订阅者要先启动，相当于先订阅某个主题**。订阅之后，什么时候上线，就什么时候去获取发布者发布的消息。

```java
public class Consumer {
    public static final String ACTIVEMQ_URL="tcp://192.168.126.6:61616";
    public static final String TOPIC_NAME="topic01";
    public static void main(String[] args) throws JMSException, IOException {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);
        Connection connection = activeMQConnectionFactory.createConnection();

        connection.setClientID("zs");  //设置订阅者ID

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Topic topic = session.createTopic(TOPIC_NAME);
        TopicSubscriber subscriber = session.createDurableSubscriber(topic, "remark");  //订阅者订阅主题
        connection.start();  //建立连接
        Message message = subscriber.receive();
        while(null != message){
            TextMessage message1= (TextMessage) message;
            System.out.println("消费者消费"+message1.getText());
            message=subscriber.receive(3000L);  //超时等待，超过三秒还不能获取到消息就下线。
        }
        subscriber.close();
        session.close();
        connection.close();
    }
}
```



**发布者：**建立连接的位置发生了改变

```java
public class Pro {
    public static final String ACTIVEMQ_URL="tcp://192.168.126.6:61616";
    public static final String TOPIC_NAME="topic01";
    public static void main(String[] args) throws JMSException {

        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);
        Connection connection = activeMQConnectionFactory.createConnection();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Topic topic = session.createTopic(TOPIC_NAME);
        MessageProducer producer = session.createProducer(topic);
        producer.setDeliveryMode(DeliveryMode.PERSISTENT);   //设置发布者的持久化
        connection.start();  //建立连接
        //发送消息
        for (int i = 1; i < 4; i++) {
            TextMessage textMessage = session.createTextMessage("pro msg --" + i);
            producer.send(textMessage);
        }
        producer.close();
        session.close();
        connection.close();

    }
}

```

测试：

1.持久化的订阅者先启动订阅了主题后下线，发布者发布消息。订阅者再次启动后可以获取下线阶段发布者发布的消息。

2.持久化的订阅者先启动订阅了主题后下线，发布者发布**持久化**消息。MQ宕机后再次启动MQ，订阅者再次上线后，依旧可以获取到发布者发布的消息。这就实现了消息的持久化。不会因为MQ宕机而丢失消息。

### 2.3总结：持久的小总结

**在Queue中**，默认是开启了持久化的，MQ宕机不会丢失消息。当设置为非持久化后，MQ宕机会丢失消息。

**在Topic中**，非持久化是没有意义的。我们可以将发布者发布的消息设置为持久化消息，订阅者设置为持久化订阅者，这样，在MQ宕机后，订阅者依旧可以获取MQ中未消费的消息。





### 2.4事务

**事务偏向生产者**

```java
/**
 * 测试事务，生产者
 */
public class Pro {

    public static  final  String    ACTIVEMQ_URL="tcp://192.168.126.6:61616";
    public static  final  String   QUEUE_NAME="queue01";

    public static void main(String[] args) throws JMSException {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
        //第一个参数表示事务是否开启。第二个参数表示签收。
        Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
        Queue queue = session.createQueue(QUEUE_NAME);
        MessageProducer producer = session.createProducer(queue);  //创建生产者
        for (int i = 1; i <=6 ; i++) {
            TextMessage textMessage = session.createTextMessage("pro msg----" + i);
            producer.send(textMessage);
        }
        producer.close();
        session.commit(); //当开启了事务时，需要在session关闭前提交事务，否则不会将消息放到MQ中
        session.close();
        connection.close();
    }
}
```

开启了事务，就要在session.close之前提交事务，当发生异常时，可以使用try，catch来进行事务回滚。如下：

```java
try {
            session.commit(); //当开启了事务时，需要在session关闭前提交事务，否则不会将消息放到MQ中
        } catch (JMSException e) {
            session.rollback();  //当发生异常时，进行事务回滚
            e.printStackTrace();
        }finally {
            if(session != null){
                session.close();  //关闭连接
            }
        }
```



对于消费者，也可以设置事务。**消费者开启了事务也必须提交事务，否则会造成重复读取的问题。**

```java
/**
 * 测试事务，消费者
 */
public class Consumer {
    public static final String ACTIVEMQ_URL="tcp://192.168.126.6:61616";
    public static final String QUEUE_NAME="queue01";
    public static void main(String[] args) throws JMSException, IOException {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
        Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
        Queue queue = session.createQueue(QUEUE_NAME);
        MessageConsumer consumer = session.createConsumer(queue);
        while (true){
            TextMessage message = (TextMessage) consumer.receive(3000L);
            if (null != message){
                System.out.println("consumer 消费："+message);
            }else {
                break;
            }
        }
        consumer.close();
        session.commit();  //事务需要提交，否则会造成重复消费的情况
        session.close();
        connection.close();

    }
}
```

### 2.5事务小总结

activeMQ中事务主要是用在生产者，开启了事务就必须要提交事务，否则生产的消息不会放到消息队列中。当发生异常时，可以对事务进行回滚。在消费者中，也可以开启事务，但是需要注意，开启了事务就必须要提交事务，否则从消息队列中读取消息时，不会将以消费的消息出队列，这样会造成消息的重复消费。



### 2.6签收

**签收主要偏向消费者。**

在**消费者**中可以设置消息的签收方式，有一下四种；

- AUTO_ACKNOWLEDGE  自动签收
- CLIENT_ACKNOWLEDGE  手动签收
- DUPS_OK_ACKNOWLEDGE  允许部分重复的签收
- SESSION_TRANSACTED  

**2.6.1非事务的签收**

```java
/**
在消费者中，设置签收方式
*/
ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
Session session=connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);//设置手动签收
        Queue queue = session.createQueue(QUEUE_NAME);
        MessageConsumer consumer = session.createConsumer(queue);
        while (true){
            TextMessage message = (TextMessage) consumer.receive(1000L);
            if (null !=message){
               //message.acknowledge();  //注销手动签收后会造成重复消费，已消费的消息不会出队列
                message.acknowledge();  //手动签收
                System.out.println(message.getText());
            }else {
                break;
            }
        }
```

**当消费者设置为手动签收后，需要在消费消息后调用acknowledge()方法进行手动签收。如果不签收的话，已消费的消息不会出队列，那么就会被重复消费。**



**2.6.2开启事务时的签收：**

- **情况一：**当**消费者开启事务**，并在session关闭之前也**进行了commit()时**，**默认会进行签收**，所以开启了手动签收但是**没有调用acknowledge()方法，也不会造成重复消费**。

```java
public static void main(String[] args) throws JMSException {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
        Session session = connection.createSession(true, Session.CLIENT_ACKNOWLEDGE);  //开启了事务，手动签收
        Queue queue = session.createQueue(QUEUE_NAME);
        MessageConsumer consumer = session.createConsumer(queue);
        while (true){
            TextMessage message = (TextMessage) consumer.receive(1000L);
            if (null !=message){
            //message.acknowledge();  //此时注销手动签收也不会造成重复消费
                System.out.println(message.getText());
            }else {
                break;
            }
        }
        consumer.close();
        session.commit();//事务提交了
        session.close();
        connection.close();
    }
```



- **情况二：**当**消费者开启事务**，在session关闭之前**不进行commit()时**，**不会进行默认签收**，**开启了手动签收并调用acknowledge()方法**，**也会造成重复消费。**

```java
 public static void main(String[] args) throws JMSException {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(ACTIVEMQ_URL);
        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
        Session session = connection.createSession(true, Session.CLIENT_ACKNOWLEDGE);  //开启了事务，手动签收
        Queue queue = session.createQueue(QUEUE_NAME);
        MessageConsumer consumer = session.createConsumer(queue);
        while (true){
            TextMessage message = (TextMessage) consumer.receive(1000L);
            if (null !=message){
            message.acknowledge();  //此时手动签收也会造成重复消费，因为事务没有提交
                System.out.println(message.getText());
            }else {
                break;
            }
        }
        consumer.close();
//        session.commit();//注销事务提交
        session.close();
        connection.close();
    }
```



由此可见：**事务的优先级比较高**。当正常进行事务操作时（开启事务，并commit了），会对已消费的消息进行签收，此时不管有没有调用acknowledge方法，都不会造成重复消费。但是消费者中没有进行commit时，就算调用了acknowledge方法，也会造成重复消费。

### 2.7签收小总结

**在事务性会话中，当一个事务被成功提交则消息会被自动签收。如果事务回滚，则消息会被再次传送。**

**在非事务会话中，消息何时被确认取决于创建会话时的应答模式。**

## 3.总结

### 3.1点对点，队列

![1591771041309](D:\TyporaDir\activeMQ\activeMQ.assets\1591771041309.png)

### 3.2一对多，发布订阅

![1591771153687](D:\TyporaDir\activeMQ\activeMQ.assets\1591771153687.png)



# 四、Broker

> **相当于是一个ActiveMQ服务器实例**
>
> 用代码的形式启动mq，将mq嵌入到java代码中，以便随时用随时启动，节省了资源，也保证了可靠性。

mq和redis类似，可以通过不同的配置文件，启动不同的mq服务（实例）。如下：

```shell
##使用02这个配置文件启动mq
./activemq start xbean:file:/myactiveMQ/apache-activemq-5.15.13/conf/activemq02.xml 
```



# 五、spring整合activeMQ

1. 导入依赖

```xml
<dependency>
            <groupId>org.apache.activemq</groupId>
            <artifactId>activemq-all</artifactId>
            <version>5.14.4</version>
        </dependency>
        <dependency>
            <groupId>org.apache.xbean</groupId>
            <artifactId>xbean-spring</artifactId>
            <version>3.16</version>
        </dependency>

<!--        mq与spring整合-->
        <!-- https://mvnrepository.com/artifact/org.springframework/spring-jms -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jms</artifactId>
            <version>4.3.23.RELEASE</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.apache.activemq/activemq-pool -->
        <dependency>
            <groupId>org.apache.activemq</groupId>
            <artifactId>activemq-pool</artifactId>
            <version>5.15.9</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>4.3.23.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>4.3.23.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
            <version>4.3.23.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.4.1</version>
        </dependency>
        <dependency>
            <groupId>cglib</groupId>
            <artifactId>cglib</artifactId>
            <version>2.2.2</version>
        </dependency>
```

2.配置spring配置文件

```xml
<!--包扫描-->
    <context:component-scan base-package="com.spring"/>

<!--配置生产者-->
    <bean id="jmsFactory" class="org.apache.activemq.pool.PooledConnectionFactory" destroy-method="stop">
        <property name="connectionFactory">
            <bean class="org.apache.activemq.ActiveMQConnectionFactory">
                <property name="brokerURL" value="tcp://192.168.126.6:61616"/>
            </bean>
        </property>
        <property name="maxConnections" value="100"/>
    </bean>

<!--    配置队列目的地，queue的-->
    <bean id="destinationQueue" class="org.apache.activemq.command.ActiveMQQueue">
        <constructor-arg index="0" value="spring-active-queue"></constructor-arg>
    </bean>

    <!--    配置队列目的地，topic的-->
    <bean id="destinationTopic" class="org.apache.activemq.command.ActiveMQTopic">
        <constructor-arg index="0" value="spring-active-topic"></constructor-arg>
    </bean>

<!--spring提供的jms工具类-->
    <bean id="jmsTemplate" class="org.springframework.jms.core.JmsTemplate">
<!--        连接工厂-->
        <property name="connectionFactory" ref="jmsFactory"/>
<!--        目的地-->
        <property name="defaultDestination" ref="destinationTopic"/>
        <property name="messageConverter">
            <bean class="org.springframework.jms.support.converter.SimpleMessageConverter"/>
        </property>
    </bean>

<!--配置监听-->
    <bean id="jmsContainer" class="org.springframework.jms.listener.DefaultMessageListenerContainer">
        <property name="connectionFactory" ref="jmsFactory"/>
        <property name="destination" ref="destinationTopic"/>
        <property name="messageListener" ref="myListener"/>
    </bean>
```

其余代码MySpringBoot\

\src\main\java\com\spring中



注意，使用监听器时，需要在spring配置文件中配置监听器，并且需要自定义监听器。使用监听器可以不开启消费者就可以获取到生产者生产的消息。因为监听器监听了这个队列。一旦有值就会去消费。



**其他：如何在整合后使用持久化、事务、签收等？**



# 六、springboot整合activemq

## 6.1前言

碰到的异常：错误的创建bean名字叫做***，未满足依赖项

原因：**broker-url，activemq的ip地址前未加tcp://**

**配置文件：**

```java
server:
  port: 7777

spring:
  activemq:
    broker-url: tcp://47.92.83.52:61616
    user: admin
    password: admin
  jms:
    pub-sub-domain: false


myqueue: queue
```

**核心代码：**生产者发布消息

```java
@Service
public class SendService {

    @Autowired
    JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    Queue queue;

    public void sendMessage(){
        jmsMessagingTemplate.convertAndSend(queue,"msg:"+ UUID.randomUUID().toString().substring(0,8));
        System.out.println("send success");
    }

}
```



## 6.2使用持久

### 6.2.1Queue中默认使用持久

**在生产者中，默认就是开启了持久的。就算MQ宕机了也不会丢失数据。**

我们可以设置是否持久：如下：

>```java
>JmsTemplate jmsTemplate = jmsMessagingTemplate.getJmsTemplate();  //转成JmsTemplate
>jmsTemplate.setDeliveryMode(PERSISTENT);  //开启持久  测试不丢失数据
>jmsTemplate.setDeliveryMode(NOT_PERSISTENT);  //设置为不持久  测试也不丢失数据。额..？？？？
>```

问题：jmsTemplate.setDeliveryMode(NOT_PERSISTENT);  //设置为不持久  测试也不丢失数据。额..？？？？

**光设置NOT_PERSISTENT是没有用的，还是会按默认的设置来发布消息。所以光设置这个属性默认还是按持久化来发送消息**

如果非要发送非持久消息。需要设置：如下

> ```java
> jmsTemplate.setDeliveryMode(NON_PERSISTENT);
> jmsTemplate.setExplicitQosEnabled(true); //还需要设置为true，非持久才生效
> //设置为true后，非持久生效。MQ宕机后重启，所有的数据都丢失，队列也没有了
> ```



### 6.2.1在topic中对订阅者使用持久(坑超多)

**注意：pub/sub中使用持久，在配置文件中配置了mq的ip地址、用户名、密码是没有用的。需要我们自己创建ConnectFactory，在连接工厂中配置地址，密码等、并将连接工厂注入到容器中，这样才能够正确连接到mq。如果不将连接工厂注入到容器中，会报error，不断的尝试去连接。代码如下：**

```java
@Configuration
@EnableJms
public class ActiveMQConfig {

    @Value("${spring.activemq.broker-url}")
    public String url;
    @Value("${spring.activemq.user}")
    public String user;
    @Value("${spring.activemq.password}")
    public String password;

    /**
     * 将连接工厂注入到容器中
     * @return
     */
    @Bean
    public ConnectionFactory connectionFactory(){
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(url);
        activeMQConnectionFactory.setUserName(user);
        activeMQConnectionFactory.setPassword(password);
        return activeMQConnectionFactory;
    }

    /**
     * 自定义topic的监听 
     * @param connectionFactory
     * @return
     */
    @Bean
    public JmsListenerContainerFactory jmsListenerContainerTopic (ConnectionFactory connectionFactory){
        DefaultJmsListenerContainerFactory bean = new DefaultJmsListenerContainerFactory();
        //不在这里指定为true，无法连接到topic，就算配置文件中写了也不行。
        bean.setPubSubDomain(true);  
        
        bean.setClientId("10001");
        bean.setSubscriptionDurable(true); //开启topic的持久
        
        bean.setSessionTransacted(false); //关闭事务
        bean.setSessionAcknowledgeMode(INDIVIDUAL_ACKNOWLEDGE);  //手动签收
        bean.setConnectionFactory(connectionFactory);
        return bean;
    }

}
```





## 6.3使用签收

**1.给消费端自定义监听，不断的接收消息**

```java
  //自定义queue的监听
    @Bean
    public JmsListenerContainerFactory jmsListenerContainerQueue(@Qualifier("jmsConnectionFactory") ConnectionFactory connectionFactory){
        DefaultJmsListenerContainerFactory bean = new DefaultJmsListenerContainerFactory();
        bean.setPubSubDomain(false); //设置为queue
        bean.setSessionTransacted(false); //关闭事务，才能手动签收
        bean.setSessionAcknowledgeMode(INDIVIDUAL_ACKNOWLEDGE);  //签收单条消息
        bean.setConnectionFactory(connectionFactory);
        return bean;
    }

    //自定义topic的监听
    @Bean
    public JmsListenerContainerFactory jmsListenerContainerTopic (ConnectionFactory connectionFactory){
        DefaultJmsListenerContainerFactory bean = new DefaultJmsListenerContainerFactory();
        bean.setPubSubDomain(true);  //设置为Topic
        bean.setSubscriptionDurable(true); //开启topic的持久
        bean.setSessionTransacted(false); //关闭事务
        bean.setSessionAcknowledgeMode(INDIVIDUAL_ACKNOWLEDGE);  //手动签收
        bean.setConnectionFactory(connectionFactory);
        return bean;
    }
```

**2.接收消息**

```java
 @JmsListener(destination = "${myqueue}",containerFactory="jmsListenerContainerQueue")
    public void receiveQueue(TextMessage message){
        try {
          message.acknowledge();  //签收，不签收的话下次启动后会重复消费
            System.out.println("---queue:"+message.getText());
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
```



## 6.4使用事务



## 6.5遇到的坑

1.在自定义监听中使用手动签收时，**需要关闭事务**，并且使用的**参数是INDIVIDUAL_ACKNOWLEDGE**，而不是Session.CLIENT_ACKNOWLEDGE

2.在自定义监听中对pub/sub使用持久化时，**配置文件**中配置的地址、用户名、密码都**没用**，需要我们自定义ConnectFactory，在ConnectFactory中配置地址、用户名、密码，**并将ConnectFactory注入到容器中**。在开启持久化时，**需要设置ClientID。ID要唯一**。

3.**一定一定记住，先开启pub**，**再开启持久化的sub**，否则持久化的sub永远不接受消息，不管重启多少次。说明根本就没有将sub注册上

4.在测试sub的持久化时，要清空以前测试的。否则可能会影响

# 七、activemq的传输协议

## 7.1概述

```xml
 <transportConnectors>
            <!-- DOS protection, limit concurrent connections to 1000 and frame size to 100MB -->
            <transportConnector name="openwire" uri="tcp://0.0.0.0:61616?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
            <transportConnector name="amqp" uri="amqp://0.0.0.0:5672?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
            <transportConnector name="stomp" uri="stomp://0.0.0.0:61613?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
            <transportConnector name="mqtt" uri="mqtt://0.0.0.0:1883?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
            <transportConnector name="ws" uri="ws://0.0.0.0:61614?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600"/>
        </transportConnectors>
```

**openwire---tcp---61616**:默认使用的传输协议。在高可用时一般不使用tcp

amqp---amqp---5672

stomp---stomp---61613

mqtt---mqtt---1883

ws---ws---61614



**tcp：**

> broker默认的传输协议，监听的端口是61616
>
> 在网络传输数据前，必须需要序列化数据，消息是通过一个叫做wire protocol的来序列化成字节流。默认情况下mq把wire protocol叫做Openwire，他的目的是促进网络上的效率和数据快速交互。
>
> 优点：tcp协议传输效率高，稳定性强
>
> 高效性：字节流方式传递，效率高
>
> 有效性、可用性：应用广泛，支持任何平台。

**NIO：**

![1591844234804](D:\TyporaDir\activeMQ\activeMQ.assets\1591844234804.png)

官网： http://activemq.apache.org/using-activemq-5 

 http://activemq.apache.org/configuring-version-5-transports.html 

![1591842048453](D:\TyporaDir\activeMQ\activeMQ.assets\1591842048453.png)

## 7.2使用NIO网络模型

要使用NIO，需要修改activeMQ的配置文件。

```xml
 <transportConnectors>
    <transportConnector name="nio" uri="nio://0.0.0.0:61618"/>  
  </<transportConnectors>
```

监听端口为61618。如果不指定MQ的网络监听端口，那么这些协议(openwire、stomp、mqtt...)都将使用BIO网络IO模型.

所以为了提高单节点的网络吞吐性能，我们需要明确指定Active的网络模型。**url以nio开头表示这个协议使用nio的网络模型。**

![1591845338160](D:\TyporaDir\activeMQ\activeMQ.assets\1591845338160.png)

添加后重启activeMQ：

![1591845376313](D:\TyporaDir\activeMQ\activeMQ.assets\1591845376313.png)

**测试：**

- 添加了NIO后，不影响原先TCP的运行。

- 将yml配置文件的ip地址修改：broker-url: nio://192.168.126.6:61618。开启linux中61618的端口。
- 开启消费者和生产者，可以进行通信

## 7.3增强的NIO

7.2中只在配置文件中添加的nio配置，只能结合tcp协议使用，不支持stomp、mqtt、ampq、ws这几种协议。

如何配置，让NIO能够结合所有的协议使用？

需要进行如下配置。

> ```xml
> <transportConnector name="auto+nio" uri="auto+nio://0.0.0.0:61608?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600&amp;org.apache.activemq.transport.nio.SelectorManager.corePoolSize=20&amp;org.apache.activemq.transport.nio.SelectorManager.maximumPoolSize=50"/>
> ```

**配置该内容后，既可以使用nio，也可以使用tcp。对于其他协议，也可以使用，不过编码的模板不一样，需要改代码。**



# 八、消息存储和持久化

## 概述

前面提过的mq的消息可靠性中包含了持久，事务、签收，这三种是mq自带的。

在mq中消息的存储和持久化**可以借助外部的组件。**

官网： http://activemq.apache.org/persistence 

在ActiveMQ中，持久化机制有很多，常用如下：

- AMQ Message Stroe
- KahaDB
- JDBC
- LeavelDB
- JDBC Message stroe with ActiveMQ Journal:JDBC的加强。



## **8.1AMQ Message Stroe：**

>  基于文件的**存储形式**，它具有**写入速度快和容器恢复**的特点。消息存在一个个文件中，文件**默认大小为32M**，当一个文件中**消息已经全部被消费**，那么这个文件将**被标识为可删除，**在**下一个清除阶段，这个文件被删除**，AMQ适用于ActiveMQ5.3之前的版本。



## **8.2kahaDB:**

> **基于日志文件**，从ActiveMQ5.4开始默认的持久化插件。
>
> kahaDB数据存储位置在data目录中的kahadb中。
>
>  [KahaDB](http://activemq.apache.org/kahadb)，它提供了比[AMQ消息存储](http://activemq.apache.org/amq-message-store)更高的可伸缩性和可恢复性。 

![1591856758299](D:\TyporaDir\activeMQ\activeMQ.assets\1591856758299.png)

消息存储使用**一个事务日志**和仅仅用**一个索引文件**来**存储它所有的地址**。

kahaDB是一个专门针对消息持久化的解决方案，它对典型的消失使用模式进行了优化，数据被追加到data logs中。当不再需要log文件中的数据时，log文件会被丢弃（归档或者删除）

### **8.2.1kahaDB存储原理：**

在存储文件kahadb中有5个文件：

- db-[num].log ：kahadb存储消息到预定义大小的数据记录文件中。当数据文件已满时，会创建一个新的文件，num的数值也会递增。如每32M一个文件，文件名称按照num的数字进行编号。当不再引用数据文件中的任何消息时，这个文件会被删除或者归档。
- db.data：该文件包含了持久化的BTree索引，索引了**消息数据记录**中的消息，**它是消息的索引文件。**
- db.redo：用于恢复BTree索引。恢复数据。
- db.free：当前的db.data文件里哪些页面时空闲的，文件具体内容是所有空闲页的ID。记录所有空闲页的ID，建索引时就从空闲的页开始，保证索引连续，不会有碎片。
- lock：文件锁，



## **8.3JDBC：**

将消息存储在JDBC数据库中。适合长时间存储，可靠性更高，但是速度慢。



## **8.4LeavlDB**

和kahaDB相似，也是基于文件的本地数据库存储形式。但是它提供比kahaDB更快的持久性。



## **8.5使用JDBC存储消息**

**1.将mysql驱动jar包添加到activemq中的lib文件夹下****

**2.修改activemq.xml配置。**

>```xml
><persistenceAdapter> 
>  <jdbcPersistenceAdapter dataSource="#my-ds" createTableOnStartUp="true"/> 
></persistenceAdapter>
>//dataSource指定将要引用的持久化数据库的bean的名称
>//createTableOnStartUp:表示启动时就创建一套表，用于存储消息。一般第一次启动后就设置为false。
>**JdbcPersistenceAdapter配置。创建mysql的bean。**
>```

**3.需要在activemq.xml中的</broker>标签后，在<import>标签前配置数据库连接。**

![1591863747340](D:\TyporaDir\activeMQ\activeMQ.assets\1591863747340.png)

**4.配置数据库连接**

```xml
<bean id="mysql-ds" class="org.apache.commons.dbcp2.BasicDataSource" destroy-method="close"> 
    <property name="driverClassName" value="com.mysql.jdbc.Driver"/> 
    <property name="url" value="jdbc:mysql://192.168.1.1:3306/activemq?relaxAutoCommit=true"/> 
    <property name="username" value="root"/> 
    <property name="password" value="root"/> 
    <property name="poolPreparedStatements" value="true"/> 
  </bean> 
```

![1591863967114](D:\TyporaDir\activeMQ\activeMQ.assets\1591863967114.png)



**5.创建数据库activemq。启动activemq时就会自动创建三张表。**

- **ACTIVEMQ_MSGS:消息表**

![1591864747657](D:\TyporaDir\activeMQ\activeMQ.assets\1591864747657.png)

- **ACTIVEMQ_ACKS：签收表**

![1591864778697](D:\TyporaDir\activeMQ\activeMQ.assets\1591864778697.png)

- **ACTIVEMQ_LOCK：锁**

**6.重启activemq。激活配置，看是否自动创建表**

![1591866203743](D:\TyporaDir\activeMQ\activeMQ.assets\1591866203743.png)

**7.代码验证**

**7.1点对点验证**

**生产者首先要开启持久。队列默认开启了持久。**

- **开启了持久**后，生产者生产的消息会**保存在数据库中**，消费者**消费消息后**，数据库中的**消息就会清空**。
- 消息保存在ACTIVEMQ_MSGS这个表中

- 不开启持久，生产的消息保存在内存中，**不会保存在数据库中。**

![1591868761361](D:\TyporaDir\activeMQ\activeMQ.assets\1591868761361.png)

**7.2topic验证**

消费者开启持久，会将消费者存在ACTIVEMQ_ACKS表中。消息保存在ACTIVEMQ_MSGS中，与Queue不同的是，topic中消息被消费后不会删除。会一直保存。

![1591869066059](D:\TyporaDir\activeMQ\activeMQ.assets\1591869066059.png)



![1591869162021](D:\TyporaDir\activeMQ\activeMQ.assets\1591869162021.png)



## 8.6总结

前提：开启了持久

对于队列：消息放在msgs表中，消息消费完就删除

对于topic：订阅者放在acks表中，消息放在msgs表中，消息消费完不删除。



**小坑：**

在使用MQ去连JDBC时，一定要将驱动包放在MQ的lib文件夹下。

createTableOnStartUp这个属性不写的时候，默认重启activeMQ会重新建表。所以可以在第一次启动后将这个属性改为false。

**下划线坑爹，**操作系统中机器名带下划线_的话,可能会报BeanFactory not initialized or already close异常。需要修改机器名并重启。



## 8.7JDBC message store  with ActiveMQ Journal

这个克服了单独使用JDBC的不足，JDBC每次有消息过来，就会去写库读库，耗费资源。

ActiveMQ journal，是一种**高速缓存写入技术**，大大提高了性能。

当消费者的消费速度能够跟上生产者的生产速度时，journal文件能够大大减少需要写入到DB中的消息。因为journal还没将消息写入DB，消费者就消费了该消息，减少写入次数。

如果消费者消费的速度慢，journal文件就可以将消息批量写入到DB。



**配置 with activemq  journal：**

![1591870907249](D:\TyporaDir\activeMQ\activeMQ.assets\1591870907249.png)



## 8.8存储持久化总结

![1591871711416](D:\TyporaDir\activeMQ\activeMQ.assets\1591871711416.png)

![1591871913825](D:\TyporaDir\activeMQ\activeMQ.assets\1591871913825.png)



# 九：zookeeper和Replicated LevelDB集群

可复制的LevelDB。保证高可用。

官网： http://activemq.apache.org/masterslave.html 

![1591879818829](D:\TyporaDir\activeMQ\activeMQ.assets\1591879818829.png)

## **9.1集群原理：**



**zookeeper相当于是哨兵，负责监控主机从机、选举主机等。**

客户端只连接Master，客户端访问不到从机。



## 9.2配置集群。



# 十、activemq特性

## 1.引入消息队列后如何保证其高可用性

持久、事务、签收。使用zk和带复制LevelDB.



## 2.异步投递Async Sends

官网： http://activemq.apache.org/async-sends 

**大多数情况下默认都是异步投递的。**

对于慢消费者而言，消费的比生产的速度慢。使用同步投递就会造成阻塞。所以需要使用异步方式投递。

![1591881845174](D:\TyporaDir\activeMQ\activeMQ.assets\1591881845174.png)

开启异步投递的三种方式：

- 在ip地址后添加参数：`jms.useAsyncSend=true`

> ```java
> "tcp://192.168.126.6:61616?jms.useAsyncSend=true"
> ```



- 使用 [ActiveMQConnectionFactory](http://incubator.apache.org/activemq/maven/activemq-core/apidocs/org/apache/activemq/ActiveMQConnectionFactory.html) 对象实例开启

> ```java
> ((ActiveMQConnectionFactory)connectionFactory).setUseAsyncSend(true);
> ```



- 使用 [ActiveMQConnection](http://incubator.apache.org/activemq/maven/activemq-core/apidocs/org/apache/activemq/ActiveMQConnection.html) 类型的连接来开启

> ```java
> ((ActiveMQConnection)connection).setUseAsyncSend(true);
> ```



## 3.由于异步投递可能会丢失消息，那么异投递如何确认投递成功？

![1591882727611](D:\TyporaDir\activeMQ\activeMQ.assets\1591882727611.png)

异步投递需要接收回值并由客户端判断是否发送成功。

![1591883224914](D:\TyporaDir\activeMQ\activeMQ.assets\1591883224914.png)



## 4.延迟投递和定时投递

官网： http://activemq.apache.org/delay-and-schedule-message-delivery 

![1591884013993](D:\TyporaDir\activeMQ\activeMQ.assets\1591884013993.png)

![1591884146310](D:\TyporaDir\activeMQ\activeMQ.assets\1591884146310.png)

- 在activemq的xml文件中添加属性 schedulerSupport=true。
- 在java代码中封装辅助消息类型。ScheduledMessage。

![1591884512751](D:\TyporaDir\activeMQ\activeMQ.assets\1591884512751.png)

![1591884785864](D:\TyporaDir\activeMQ\activeMQ.assets\1591884785864.png)



## 5.分发策略



## 6.ActiveMQ消费重试机制

broker给消费者不断地发重复的消息。

### 6.1具体哪些情况会引起消息重发

- 消费端开启了事务并在session中使用rollback(),有可能造成事务回滚，消息重发
- 消费端开启事务，并在调用commit()之前关闭或者没有调用commit
- 消费端在手动签收的传递模式下，在session中调用了recover()方法。

### 6.2消息重发时间间隔和重发次数

默认每间隔一秒钟发送一次，一共发送6次

### 6.3有毒消息Poison ACK谈谈你的理解

当一个消息被重发超过默认的最大重发次数（6次）时，消费端会给mq发送一个“poison ack” 表示这个消息有毒，告诉broker不要再发了，这个时候broker会把这个消息放到DLQ(死信队列中)

![1591927602125](D:\TyporaDir\activeMQ\activeMQ.assets\1591927602125.png)



## 7.死信队列

Dead letter  Queue。

**消息被重发了超过6次后，就会被MQ移入死信队列**，死信队列主要是用于处理失败的消息。

默认所有的异常消息都放到共享的死信队列中。我们可以给重要的业务设置单独的死信队列。如下图：

![1591928775250](D:\TyporaDir\activeMQ\activeMQ.assets\1591928775250.png)



自动删除过期消息

![1591928850831](D:\TyporaDir\activeMQ\activeMQ.assets\1591928850831.png)



将非持久的消息放入死信队列。

![1591928868177](D:\TyporaDir\activeMQ\activeMQ.assets\1591928868177.png)

## 8.如何保证消息不被重复消费？幂等性问题

![1591929009197](D:\TyporaDir\activeMQ\activeMQ.assets\1591929009197.png)