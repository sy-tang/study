豪猪哥（Hystrix）

**服务降级，熔断**。断路器。能够**接近实时监控**





# **服务降级  ：fallback**

## 1、简单的使用服务降级

可以在消费端和服务端设置服务降级

项目：cloud-provider-hystrix-payment8001

### **1、服务端引入hystrix的依赖**

​	serviceimpl中设置降级的兜底方法和超时时间

```java
package com.porject.springcloud.service;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class HystrixPaymentServiceImpl {

    public String paymentInfo_OK(Integer id)
    {
        return "线程池:  "+Thread.currentThread().getName()+"  paymentInfo_OK,id:  "+id+"\t"+"O(∩_∩)O哈哈~";
    }


    //本方法执行超时或者错误时，使用fallbackMethod指定的兜底方法
    //HystrixProperty中表示只允许这个方法在3秒内执行完成（红线），超过3秒则表示超时了，那就执行兜底方法了
    @HystrixCommand(fallbackMethod = "paymentInfo_TimeOutHandler",commandProperties = {
            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value = "5000")
    })
    public String paymentInfo_TimeOut(Integer id){
        int time=3;
        try { TimeUnit.SECONDS.sleep(time); } catch (InterruptedException e) { e.printStackTrace(); }
        return "线程池:  "+Thread.currentThread().getName()+" id:  "+id+"\t"+"O(∩_∩)O哈哈~"+"  耗时(秒): "+time;
    }

    public  String paymentInfo_TimeOutHandler(Integer id){
        return "线程池:  "+Thread.currentThread().getName()+" id:  "+id+"\t"+"我是兜底的方法";
    }

}

```

**使用了@HystrixCommand注解就要搭配@EnableCircuitBreaker** 熔断器

### **2、在客服端中使用Hystix**

首先需要注意：在openFegin中默认超时时间为1s，调用服务时，如果超过1s就会报超时错误。因为Fegin中包含了Ribbon，所以可以通过Ribbon来设置超时时间

```yml
ribbon:
  #指的是建立连接所用的时间，适用于网络状况正常的情况下,两端连接所用的时间
  ReadTimeout: 5000
  #指的是建立连接后从服务器读取到可用资源所用的时间
  ConnectTimeout: 5000
```

**使用Hystix**

在客户端的controller中配置了兜底的方法和超时时间。不过需要注意，Hystrix默认的超时时间也是1s，所以1s中没有执行完，就直接报超时错误。如下注释的规则中，设置了6s的超时等待，但不生效。不管方法是否正确或者超时，都只等待1s，1s后就去执行兜底方法了。所以需要在yml文件中配置Hystix的超时时间。

```java
@GetMapping("/consumer/payment/timeout/{id}")
    //客户端自己的服务降级，超过1500就不等了。服务端定义的是5000内正常，客户端定义的是1500内正常。
//    @HystrixCommand(fallbackMethod = "paymentTimeOutFallbackMethod",commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "6000")
//    })
    @HystrixCommand
    public String paymentInfo_TimeOut(@PathVariable("id") Integer id){
        System.out.println(id);
        String result=paymentHystrixService.paymentInfo_TimeOut(id);
        System.out.println("************");
        return result;
    }
    public String paymentTimeOutFallbackMethod(@PathVariable("id") Integer id){
        return "客户端80，等了1.5秒或运行出错了，我执行了这个兜底方法，服务降级";
    }
```

yml中的配置,设置了5s的超时等待。

```yml

  #开启hystrix
feign:
  hystrix:
    enabled: true

#配置超时等待的时间
hystrix:
  command:
    default:
      execution:
        timeout:
          isolation:
            thread:
              timeoutInMilliseconds: 5000
```

另外还需要注意的是：**开启了hystrix就需要在启动类中添加启动注解**

```java
@EnableHystrix     //要使用Hystrix就要开启hystrix
```

**使用全局的服务降低方法**

在controller中标注如下注解

```java
@RestController
@Slf4j
@DefaultProperties(defaultFallback = "payment_Gloal_FallbackMethod")  //全局的兜底方法
public class OrderHystirxController {
    
    //在controller中编写全局的兜底方法
    //全局fallback
    public String payment_Gloal_FallbackMethod(){
        return "全局异常处理";
    }
```

在controller中编写服务降级的方法会造成controllr中方法膨胀和混乱

可以编写一个实现service接口的类，重写里面所有的调用服务端的方法。然后再service中的@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT")中添加属性fallback,代码如下。

```java
@Component
@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT",fallback = PaymentFallbackService.class)
public interface PaymentHystrixService {

    @GetMapping("/payment/ok/{id}")
     String paymentInfo_OK(@PathVariable("id") Integer id);


    @GetMapping("/payment/timeout/{id}")
    String paymentInfo_TimeOut(@PathVariable("id") Integer id);


}

```

实现service接口的类

```java
package com.project.springcloud.service;

@Component
public class PaymentFallbackService implements PaymentHystrixService {

    @Override
    public String paymentInfo_OK(Integer id) {
        return "game over  info_ok";
    }

    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "game over  info_timeout";
    }
}

```

这样controller中的兜底方法就可以不用写了，都在上面这个类中定义了所有的兜底方法。当原来的方法执行超时或者错误，就会来这里面执行对应的兜底方法。

在使用服务降级的过程中遇到了一个问题。

当给一个方法标注了它的兜底方法时，这个方法明明时可以执行，但却没有执行，执行了它的兜底方法。

猜测是由于hystrix的默认超时时间只有1s中造成的。当修改为了5s后，在这个5s内，可以从得到服务端响应，则不会被兜底方法执行。另外必须要注意的是注解的搭配使用

```java
@EnableHystrix     //要使用Hystrix就要开启hystrix,在配置文件中开启了hstrix就要使用这个注解
@EnableCircuitBreaker    //和@HystrixCommand 配对使用的。
```

还要记得@Component的使用









# 服务熔断: break

服务降级->进而熔断->恢复调用链路

在项目：cloud-provider-hystrix-payment8001的service层中编写如下代码

```java
//    *********************************服务熔断***************************************

    @HystrixCommand(fallbackMethod = "paymentCircuitBreaker_fallback",commandProperties = {
   @HystrixProperty(name = "circuitBreaker.enabled",value = "true"),// 是否开启断路器
   @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),// 请求次数
   @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "10000"), // 时间窗口期
   @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "60"),// 失败率达到多少后跳闸
    })
    public String paymentCircuitBreaker(@PathVariable("id") Integer id)
    {
        if(id < 0)
        {
            throw new RuntimeException("******id 不能负数");
        }
        String serialNumber = IdUtil.simpleUUID();

        return Thread.currentThread().getName()+"\t"+"调用成功，流水号: " + serialNumber;
    }
    public String paymentCircuitBreaker_fallback(@PathVariable("id") Integer id)
    {
        return "id 不能负数，请稍后再试，/(ㄒoㄒ)/~~   id: " +id;
    }
```

在controller中编写如下代码

```java
 //**************************服务熔断测试******************

    //====服务熔断
    @GetMapping("/payment/circuit/{id}")
    public String paymentCircuitBreaker(@PathVariable("id") Integer id)
    {
        String result = hystrixPaymentService.paymentCircuitBreaker(id);
        log.info("****result: "+result);
        return result;
    }
```

浏览器中测试，

 http://localhost:8001/payment/circuit/1   当为正数时能够执行成功

 http://localhost:8001/payment/circuit/-1 当为负数时（执行失败）执行兜底的方法。





**当为负数时，执行兜底方法(服务降级)  ，当失败率达到60%后，会进行熔断，在一段时间内，所有的请求不管参数是正数还是负数都无法执行成功（都去执行兜底的方法了），间隔一段时间后，慢慢恢复调用的链路，这时为正数的就可以正确执行成功了。这就是服务熔断的过程：服务降级->进而熔断->恢复调用链路**





总结：



# 服务限流 :flowlimit

















# **服务监控**

1、引入依赖

```xml
<dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-hystrix-dashboard</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
```

启动类中添加注解@EnableHystrixDashBoard   

配置端口9001

 http://localhost:9001/hystrix 浏览器中访问，添加监控的路径

‘’

在要监控 的微服务的启动类中添加

```java
/**
     *此配置是为了服务监控而配置，与服务容错本身无关，springcloud升级后的坑
     *ServletRegistrationBean因为springboot的默认路径不是"/hystrix.stream"，
     *只要在自己的项目里配置上下面的servlet就可以了
     */
    @Bean
    public ServletRegistrationBean getServlet() {
        HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(streamServlet);
        registrationBean.setLoadOnStartup(1);
        registrationBean.addUrlMappings("/hystrix.stream");
        registrationBean.setName("HystrixMetricsStreamServlet");
        return registrationBean;
    }
```

在dashboard中添加监控的ip地址，如下图（对8001进行监控）

![1587627439809](D:\TyporaDir\springcloud\day3\Hystrix.assets\1587627439809.png)

测试：

 http://localhost:8001/payment/circuit/1 

![1587627598927](D:\TyporaDir\springcloud\day3\Hystrix.assets\1587627598927.png)

 http://localhost:8001/payment/circuit/-1

![1587627653322](D:\TyporaDir\springcloud\day3\Hystrix.assets\1587627653322.png)