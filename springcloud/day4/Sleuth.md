#  分布式请求链路跟踪

为什么？解决的问题？

在分布式微服务中，有很多链路，一个请求的链路可能非常长，所以需要对链路进行跟踪。

sleuth负责收集，zipkin负责展现。

zipkin下载 https://dl.bintray.com/openzipkin/maven/io/zipkin/java/zipkin-server/ 

java -jar   ** 运行就行了

通过localhost:9411/zipkin访问

![1587712820015](D:\TyporaDir\springcloud\day4\Sleuth.assets\1587712820015.png)

在最初的8001中引入依赖

```xml
   <dependency>
     <groupId>org.springframework.cloud</groupId>
     <artifactId>spring-cloud-starter-zipkin</artifactId>
   </dependency>
```

yml中添加

```yaml
server:
  port: 8001
spring:
  application:
    name: cloud-payment-service  #这个名字是入驻eureka中的名称
  zipkin:
    base-url: http://localhost:9411   #监控的信息打到这个地址
    sleuth:
      sampler:
      #采样率值介于 0 到 1 之间，1 则表示全部采集
      probability: 1
```

controller中

```java
 @GetMapping("/payment/zipkin")
    public String paymentZipkin()
    {
        return "hi ,i'am paymentzipkin server fall back，welcome to atguigu，O(∩_∩)O哈哈~";
    }
```

在80中也加入同样的配置和依赖

在80的controllr中添加

```java

    // ====================> zipkin+sleuth
    @GetMapping("/consumer/payment/zipkin")
    public String paymentZipkin()
    {
        String result = restTemplate.getForObject("http://localhost:8001"+"/payment/zipkin/", String.class);
        return result;
    }
```



访问这一路径，看看是否能够被检测到。

启动eureka  80  8001

访问80

 http://localhost/consumer/payment/zipkin 

在监控网页中可以看到

80调用的8001

![1587714648125](D:\TyporaDir\springcloud\day4\Sleuth.assets\1587714648125.png)