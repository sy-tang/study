# 消息驱动

# 一、为什呢？解决的痛点

​	现在常用的消息中间件种类多。如何整合这些消息中间件？使用Stream

Stream：屏蔽底层消息中间件的差异，降低切换成本，统一消息的编程模型。



binder：绑定器：屏蔽底层消息中间件的差异

中文文档

 https://m.wang1314.com/doc/webapp/topic/20971999.html 

# 二、设计思想

**binder**

## 



## **output对应生产者**

生产者的构建

**1、生产消息**

导包

```xml
 <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
<!--重要的两个依赖-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-stream</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
        </dependency>
        <!--基础配置-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
```

yml

```yaml
server:
  port: 8801

spring:
  application:
    name: cloud-stream-provider
  cloud:
    stream:
      binders: # 在此处配置要绑定的rabbitmq的服务信息；
        defaultRabbit: # 表示定义的名称，用于于binding整合
          type: rabbit # 消息组件类型
          environment: # 设置rabbitmq的相关的环境配置
            spring:
              rabbitmq:
                host: 192.168.126.6
                port: 5672
                username: guest
                password: guest
      bindings: # 服务的整合处理
####这里是output
        output: # 这个名字是一个通道的名称
          destination: studyExchange # 表示要使用的Exchange名称定义
          content-type: application/json # 设置消息类型，本次为json，文本则设置“text/plain”
          binder: defaultRabbit           # 设置要绑定的消息服务的具体设置

eureka:
  client: # 客户端进行Eureka注册的配置
    service-url:
      defaultZone: http://localhost:7001/eureka
  instance:
    lease-renewal-interval-in-seconds: 2 # 设置心跳的时间间隔（默认是30秒）
    lease-expiration-duration-in-seconds: 5 # 如果现在超过了5秒的间隔（默认是90秒）
    instance-id: send-8801.com  # 在信息列表时显示主机名称
    prefer-ip-address: true     # 访问的路径变为IP地址
```



service

```java
//接口
public interface IMessageProvider {
    public String send();
}

//实现类

@EnableBinding(Source.class)    //消息推送的管道
public class IMessageProviderImpl implements IMessageProvider {

    @Resource
    MessageChannel output;
    @Override
    public String send() {
        String serial = UUID.randomUUID().toString();
        output.send(MessageBuilder.withPayload(serial).build());   //推送消息
        System.out.println(serial);
        return null;
    }
}
```

controller

```java
@RestController
public class SendMessageController {

    @Resource
    IMessageProvider iMessageProvider;

    @GetMapping(value = "/sendMessage")
    public String sendMessage()
    {
        return iMessageProvider.send();
    }

}
```

测试：

 http://localhost:8801/sendMessage 

![1587708626118](D:\TyporaDir\springcloud\day4\Stream.assets\1587708626118.png)

@EnabaleBinding

## input对应消费者

**消费者接收消息**

添加依赖

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
    <!--基础配置-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-devtools</artifactId>
        <scope>runtime</scope>
        <optional>true</optional>
    </dependency>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <optional>true</optional>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
</dependencies>
```



yml

```yaml
server:
  port: 8802

spring:
  application:
    name: cloud-stream-consumer
  cloud:
    stream:
      binders: # 在此处配置要绑定的rabbitmq的服务信息；
        defaultRabbit: # 表示定义的名称，用于于binding整合
          type: rabbit # 消息组件类型
          environment: # 设置rabbitmq的相关的环境配置
            spring:
              rabbitmq:
                host: 192.168.126.6
                port: 5672
                username: guest
                password: guest
      bindings: # 服务的整合处理
      
####这里是input
        input: # 这个名字是一个通道的名称(输入)
          destination: studyExchange # 表示要使用的Exchange名称定义
          content-type: application/json # 设置消息类型，本次为对象json，如果是文本则设置“text/plain”
          binder: defaultRabbit # 设置要绑定的消息服务的具体设置



eureka:
  client: # 客户端进行Eureka注册的配置
    service-url:
      defaultZone: http://localhost:7001/eureka
  instance:
    lease-renewal-interval-in-seconds: 2 # 设置心跳的时间间隔（默认是30秒）
    lease-expiration-duration-in-seconds: 5 # 如果现在超过了5秒的间隔（默认是90秒）
    instance-id: receive-8802.com  # 在信息列表时显示主机名称
    prefer-ip-address: true     # 访问的路径变为IP地址

```



controller

```java
@Component
@EnableBinding(Sink.class)   //开启接收消息
public class ReceiveMessageListenerController {

    @Value("${server.port}")
    String serverPort;

    @StreamListener(Sink.INPUT)  //监听  接收
    public void input(Message<String> message ){
        System.out.println("消费者1号,----->接受到的消息: "+message.getPayload()+"端口号："+serverPort);
    }
}

```

测试结果，生产者生产的的消息，消费者能够接收到

![1587710082254](D:\TyporaDir\springcloud\day4\Stream.assets\1587710082254.png)





# 三、消息重复消费的问题

再创建一个消费者8803，看看是否会重复消费

8802和8803获取的消息一样，造成了重复消费的问题。

不同的组是可以重复消费的

**存在两个不同的组，所以可能造成重复消费。默认每个微服务的组都不一样，所以每个微服务都会消费这个消息。**

![1587711030458](D:\TyporaDir\springcloud\day4\Stream.assets\1587711030458.png)

**通过消息分组和持久化解决这一问题**

自定义配置分组，将他们分为同一个组。这样同一个组里的微服务就会发生竞争，只有一个微服务能够获取到消息。

在yml中添加group

![1587711551568](D:\TyporaDir\springcloud\day4\Stream.assets\1587711551568.png)

这样就可以把8802和8803放在同一个组atguiguA中，对于同一个消息，保证只有一个微服务能够拿到。





# 四、持久化

通过加group加默认开启了持久化

首先去掉8802的group，保留8803的分组

将8802和8803关掉，8801不断的发送消息，这时侯，因为他们都关闭了，没有人去消费这些消息。当8802重启的时候，因为它没有分组，所以不会获取消息中间件中的数据，这造成了数据的丢失。

而8803保留了分组，当8803重启时，可以从消息中间件中获取没有人消费的消息并消费。不会丢失数据。





