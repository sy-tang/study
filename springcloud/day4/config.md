# 配置中心

在github中间一个仓库

将公用的配置文件放到仓库中

**服务端**

在confi的微服务中配置如下

```yaml
server:
  port: 3344

spring:
  application:
    name:  cloud-config-center #注册进Eureka服务器的微服务名
  cloud:
    config:
      server:
        git:
          uri: https://github.com/admig/cloud-config.git #GitHub上面的git仓库名字
          ####搜索目录
          search-paths:
            - cloud-config
      ####默认读取分支
      label: master

#服务注册到eureka地址
eureka:
  client:
    service-url:
      defaultZone: http://localhost:7001/eureka

```



在浏览器端访问读取配置文件

 http://localhost:3344/master/config-dev.yml   读取主线上的

 http://localhost:3344/dev/config-dev.yml   读取分支上的

配置读取规则有多种。







客户端：

bootstrap.yml 优先级比application.yml更高。他是系统级别的，application是用户级别的。

bootstrap负责从外部源加载配置文件。

bootstrap中的配置

```yaml
server:
  port: 3355

spring:
  application:
    name: config-client
  cloud:
    #Config客户端配置
    config:
      label: master #分支名称
      name: config #配置文件名称
      profile: dev #读取后缀名称
      uri: http://localhost:3344 #配置中心地址k
#上述3个综合：通过3344，获取master分支上config-dev.yml的配置文件   http://config-3344.com:3344/master/config-dev.yml


#服务注册到eureka地址
eureka:
  client:
    service-url:
      defaultZone: http://localhost:7001/eureka

```

controller

```java
 @Value("${config.info}")   //获取通过3344，获取github中dev中的配置信息
    String configinfo;

    @GetMapping("/configinfo")
    public String configinfo(){
        return configinfo;
    }
```

测试：

![1587693201193](D:\TyporaDir\springcloud\day4\config.assets\1587693201193.png)

能通过3344从github中获取配置信息。

**存在的问题：修改github中的配置信息，3344能够立即获取新的配置信息，3355不能立即获取，重启才能获取修改后的配置信息。**

**如何解决这个动态刷新的问题：**

1.引入actuator监控

2.修改yml，暴露监控端点

```yaml
# 暴露监控端点
management:
  endpoints:
    web:
      exposure:
        include: "*"
```





3.在控制类加上@RefreshScope注解，业务类controller修改

4.**使用post请求刷新3355(手动刷新)**

http://localhost:3355/actuator/refresh

测试：

 http://localhost:3355/configinfo   能够获取修改的值。

![1587693998942](D:\TyporaDir\springcloud\day4\config.assets\1587693998942.png)





**以上还是有一点问题，就是每一个都要去发送请求刷新。**

如何一次刷新，处处生效，并且还可以指定哪些生效，哪些不生效？

config只能手动一个个去发送请求刷新。

解决这一问题：使用消息总线：Bus。