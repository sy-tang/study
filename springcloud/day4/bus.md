# 消息总线：

bus只支持两种消息代理：RabbitMQ和kafka

# 一、动态刷新

两种解决问题的思路：

1. 配置修改后，通知一个客户端，由这个客户端不断的去传染其他的客户端更新配置信息。（**破坏了微服务的单一性！**）
2. 配置修改后，通知服务端（总的配置中心），由配置中心去通知客户端更新配置信息。（**更合适！**）





解决：

在3344中添加依赖

```xml
<!--添加消息总线RabbitMQ支持-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bus-amqp</artifactId>
        </dependency>
```

在3344yml中添加

```yaml

#rabbitmq相关配置,配在sprin下
rabbitmq:
  host: 192.168.126.6
  port: 5672
  username: guest
  password: guest

##rabbitmq相关配置,暴露bus刷新配置的端点
management:
  endpoints: #暴露bus刷新配置的端点
    web:
      exposure:
        include: 'bus-refresh'
```

在3355中添加依赖

```xml
<!--添加消息总线RabbitMQ支持-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bus-amqp</artifactId>
        </dependency>

```

添加配置

```yaml
#rabbitmq相关配置,配在spring下
rabbitmq:
  host: 192.168.126.6
  port: 5672
  username: guest
  password: guest
```

3366和3355一样





测试：只给3344发送post请求，由3344通知其他

 http://localhost:3344/actuator/bus-refresh 





3355，3366订阅的都是springCloudBus

![1587698679898](D:\TyporaDir\springcloud\day4\bus.assets\1587698679898.png)





# 二、动态刷新的定点通知

该通知的通知，不该通知的不通知。

只想通知3355，不通知3366

post刷新公式： http://localhost:3344/actuator/bus-refresh /{destination}

例如： http://localhost:3344/actuator/bus-refresh/config-client:3355     //服务名：端口号

3355已经改变

![1587698978978](D:\TyporaDir\springcloud\day4\bus.assets\1587698978978.png)

3366没有改变

![1587698995726](D:\TyporaDir\springcloud\day4\bus.assets\1587698995726.png)