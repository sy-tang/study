eureka

ribbon

gateway

openfegin

hystrix

config

bus



**restTemplate：** 模板类

> RestTemplate类可用于在应用中调用rest服务，它简化了与http服务的通信方式，统一了RESTful的标准，封装了http链接， 我们只需要**传入url及返回值类型**即可。相较于之前常用的HttpClient，RestTemplate是一种更优雅的调用RESTful服务的方式 