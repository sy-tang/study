

# 一、搭建

 https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html 

服务注册+配置中心。替代了eureka和config。

nacos天生自带负载均衡。（整和了ribbon，整合了ribbon就可以是使用restTemplate）

下载后启动

localhost:8848/nacos

![1587718932287](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587718932287.png)





使用nacos

在服务端添加依赖  详情看项目cloudalibaba-provider-payment9001

```xml
<dependencies>
        <!--SpringCloud ailibaba nacos -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!-- SpringBoot整合Web组件 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <!--日常通用jar包配置-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
```































# 二、nacos服务配置中心

具体看项目3377

导入依赖

```xml
<dependencies>
        <!--nacos-config-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
        <!--nacos-discovery-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!--web + actuator-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <!--一般基础配置-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
```





![1587775908845](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587775908845.png)





**注意在nacos界面建立yaml时，必须以yaml结尾**

![1587777693961](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587777693961.png)



测试

![1587777725060](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587777725060.png)





**nacos可以动态刷新。在nacos中修改配置后，无需发送post请求刷新，就可以直接获取到新的值。比config方便很多。**

![1587777828452](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587777828452.png)

# 三、nacos命名空间、分组

分类管理非常牛逼

![1587778116016](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587778116016.png)

命名空间





Namespace +Group+Data ID

类是maven中的GAV





## 1、根据Data ID获取指定环境的配置

**在application.yml中，可以指定使用的开发环境 比如dev或者test。指定了就可以加载nacos中配置的相应环境的配置文件。**

如

![1587778980772](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587778980772.png)

![1587779003515](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587779003515.png)

测试。加载的是test环境的配置文件。

![1587779031489](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587779031489.png)

结论：通过yml中指定不同的开发环境，可以加载对应的配饰文件。



## 2、分组：Group

通过组名的不同来加载不同组的配置文件

操作如下

在两个不同的组中添加配置文件名相同的配置文件。

![1587779674283](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587779674283.png)

在yml中通过改变组来加载他们。

![1587779772903](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587779772903.png)

测试：加载到了开发组中的配置文件

![1587779844886](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587779844886.png)

**总结：可以加载指定组中的配置文件。**





## 3、命名空间

创建2个自己的命名空间

![1587779983352](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587779983352.png)

可以在指定的空间下编写配置文件。

![1587780556691](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587780556691.png)

在bootstrap中添加命名空间的id，指定要加载哪个空间下的配置文件。

![1587780396180](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587780396180.png)

测试：

![1587780643445](D:\TyporaDir\springcloud\cloudAlibaba\Nacos.assets\1587780643445.png)

## 总结

namespace、group、data id和maven中的gav一样。

可以通过ngd准确的找到配置文件的位置。比如上图所示：找到dev空间下的dev组的名字为nacos-config-client-dev.yaml的配置文件。

nacos默认的空间是public，默认的分组是DEFAULT_GROUP。在我们没有指定命名空间时，默认就是从pubic中去找。





# 四、nacos集群和持久化配置（重要）

 https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html 

多台nacos如何配置？搭建多台nacos。集群。

nacos的持久化。