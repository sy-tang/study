负载均衡+RestTempalte

**修改均衡机制时，需要注意，不能将这个配置类放在@ComponentScan扫描的包或者子包下。也就是说不能放在启动类这个或者子包下，需要我们在启动类上一层的包中进行配置（或自定义）均衡机制**

![1587543752093](D:\TyporaDir\springcloud\day2\ribbon.assets\1587543752093.png)

使用随机的均衡机制(常用的机制有7种，在IRule这个类中)

```java
@Configuration
public class MySelfRule {

    @Bean
    public IRule myRule(){
        return new RandomRule(); //随机的均衡机制
    }
}
```

启动类中加上注解

```java
//name 表示是哪个服务   configuration表示使用的均衡机制。这里是随机，说明会随机的访问name表示的服务
@RibbonClient(name = "CLOUD-PAYMENT-SERVICE",configuration = MySelfRule.class)

```

