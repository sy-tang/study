在docker中使用zookeeper

查看zookeeper中注册的服务。

 https://www.cnblogs.com/shar-wang/p/11631027.html 

![1587535578044](D:\TyporaDir\springcloud\day2\zookeeper.assets\1587535578044.png)

**思考：zk服务节点是临时节点还是持久节点？**

**（细分为：临时  带序号临时  持久  带序号的持久）**

**答案：临时节点。服务间隔一段时间没有发心跳给zk，zk会直接踢掉该服务。不像eureka会有自我保护机制。**

zk的集群，只要在配置文件中添加多个zk的地址即可。

