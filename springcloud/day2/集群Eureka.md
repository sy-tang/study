相互注册，相互守望

7001的service-url注册到7002中

7002的service-url注册到7001中



健康检测 http://localhost:8001/actuator/health 





服务发现：获取eureka中微服务的信息。

在payment8001中的controller中编写服务发现。在启动类中添加注解：@EnableDiscoveryClient

```java
 //服务发现
    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping("/payment/discovery")
    public Object discovery() {
        //获取所有的服务
        List<String> services = discoveryClient.getServices();
        for (String ele:services){
            System.out.println(ele);
        }
        //获取eurkea中的名称为CLOUD-PAYMENT-SERVICE的所有服务的实例
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance:instances){
            //通过服务的实例来获取该服务的信息，id、主机名、端口、访问的uri
            System.out.println(instance.getServiceId()+"\t"+instance.getHost()+"\t"+instance.getPort()+"\t"+instance.getUri());
        }
        return discoveryClient;
    }
```

获取到了所有注册的服务

![1587524948629](D:\TyporaDir\springcloud\day2\Untitled.assets\1587524948629.png)

输出了服务的id  主机名  端口号等信息

![1587525114283](D:\TyporaDir\springcloud\day2\Untitled.assets\1587525114283.png)

eureka中自我保护：某一时刻某一个微服务不可用了，eureka不会立刻清理该微服务。依旧会对该微服务的信息进行保存。（高可用的思想）（相当于可以赊账）

自我保护导致的原因：属于CAP中的AP分支

怎么禁止自我保护：