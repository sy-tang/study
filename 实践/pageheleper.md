常见：404错误 ，资源找不到

空指针异常。

模板解析错误。



mybatis插入一条数据后立即获取该数据的主键

```xml
useGeneratedKeys="true" keyProperty="id
```

maven中依赖的声明周期？



模板方法模式

定义一个操作中的算法骨架，而将一些操作延迟到子类中执行。模板方法可以使得子类不改变算法的骨架根据需要重定义该算法的某些特定步骤。

策略模式

定义一系列算法，把他们封装起来，并使他们可以相互替换。比如说加减乘除分别封装起来，根据自身需要来使用相应的操作。





mysql基本知识

//全是大写

auto_increment   主键自增

Null   和Not Null   ：字段设计时尽量不为空，   “ ”：空直不占存储空间，null会占用空间(/0)   

primary key（id）  定义id为主键

engine=innoDB    存储引擎为innoDB

create database  创建数据库

drop  table   删除数据库

插入语句：insert into   表名  （字段名）  values （值）



查询语句：select   *  from   表名   where  id=1 ;

> binary  设定区分大小写
>
> where  binary   name="hh" 和where  binary  name=“HH”是不一样的，区分大小写。
>
> **group by:**对select查询出来的结果集按照某个字段或者表达式进行分组，获得一组组的集合，然后从每组中取出一个指定字段或者表达式的值。
>
> **having：**用于对where和group by查询出来的分组经行过滤，查出满足条件的分组结果。它是一个过滤声明，是在查询返回结果集以后对查询结果进行的过滤操作。
>
>  **select –>where –> group by–> having–>order by->limit** 



更新语句：update   表名   set   name='zhangshan',age=23  where id=2;

**UPDATE替换某个字段中的某个字符**

当我们需要将字段中的特定字符串**批量**修改为其他字符串时，可已使用以下操作：

> ```
> UPDATE table_name SET field=REPLACE(field, 'old-string', 'new-string') 
> [WHERE Clause]
> 实例：以下实例将更新 runoob_id 为 3 的runoob_title 字段值的 "C++" 替换为 "Python"：
> UPDATE runoob_tbl SET runoob_title = REPLACE(runoob_title, 'C++', 'Python') where 
> runoob_id = 3;
> 
> ```



删除语句：delete from  user where id=3;

drop、delete、 truncate  都有删除表的作用。

![1588123852612](D:\TyporaDir\实践\pageheleper.assets\1588123852612.png)

like模糊匹配的规则

‘%a’：以a结尾的数据

‘a%’ ：以a开头的数据

‘%a%’：含有a的数据

![1588124231470](D:\TyporaDir\实践\pageheleper.assets\1588124231470.png)

![1588124323862](D:\TyporaDir\实践\pageheleper.assets\1588124323862.png)



# MySQL UNION 操作符

### 描述

MySQL UNION 操作符用于连接两个以上的 SELECT 语句的结果组合到一个结果集合中。多个 SELECT 语句会删除重复的数据。

### 语法

MySQL UNION 操作符语法格式：

> ```
> SELECT expression1, expression2, ... expression_n
> FROM tables
> [WHERE conditions]
> UNION [ALL | DISTINCT]
> SELECT expression1, expression2, ... expression_n
> FROM tables
> [WHERE conditions];
> ```

### 参数

- **expression1, expression2, ... expression_n**: 要检索的列。
- **tables:** 要检索的数据表。
- **WHERE conditions:** 可选， 检索条件。
- **DISTINCT:** 可选，删除结果集中重复的数据。默认情况下 UNION 操作符已经删除了重复数据，所以 DISTINCT 修饰符对结果没啥影响。
- **ALL:** 可选，返回所有结果集，包含重复数据。
- ![1588124780888](D:\TyporaDir\实践\pageheleper.assets\1588124780888.png)

# MySQL 排序

 **ORDER BY** 子句来设定你想按哪个字段哪种方式来进行排序，再返回搜索结果。 

 你可以使用 ASC 或 DESC 关键字来设置查询结果是按升序或降序排列。 默认情况下，它是按升序排列。 

对字符串排序时，如果是jbk编码的话可以直接使用order by排序，如果是utf-8，先转成gbk，在排序。

![1588125547669](D:\TyporaDir\实践\pageheleper.assets\1588125547669.png)

# Mysql分组Group by

GROUP BY 语句根据一个或多个列对结果集进行分组。

在分组的列上我们可以使用 COUNT, SUM, AVG,等函数。

> ```mysql
> 语法
> SELECT column_name, function(column_name)
> FROM table_name
> WHERE column_name operator value
> GROUP BY column_name;
> ```

![1588125889051](D:\TyporaDir\实践\pageheleper.assets\1588125889051.png)

![1588126573133](D:\TyporaDir\实践\pageheleper.assets\1588126573133.png)

# MySQL 连接的使用

JOIN 按照功能大致分为如下三类：

- **INNER JOIN（内连接,或等值连接）**：获取两个表中字段匹配关系的记录。
- **LEFT JOIN（左连接）：**获取左表所有记录，即使右表没有对应匹配的记录。
- **RIGHT JOIN（右连接）：** 与 LEFT JOIN 相反，用于获取右表所有记录，即使左表没有对应匹配的记录。

具体操作： https://www.runoob.com/mysql/mysql-join.html 





# MySQL NULL 值处理

 我们已经知道 MySQL 使用 SQL SELECT 命令及 WHERE 子句来读取数据表中的数据,但是当提供的查询条件字段为 NULL 时，该命令可能就无法正常工作。 

为了处理这种情况，MySQL提供了三大运算符:

- **IS NULL:** 当列的值是 NULL,此运算符返回 true。
- **IS NOT NULL:** 当列的值不为 NULL, 运算符返回 true。
- **<=>:** 比较操作符（不同于 = 运算符），当比较的的两个值相等或者都为 NULL 时返回 true。

关于 NULL 的条件比较运算是比较特殊的。你不能使用 = NULL 或 != NULL 在列中查找 NULL 值 。

在 MySQL 中，NULL 值与任何其它值的比较（即使是 NULL）永远返回 NULL，即 NULL = NULL 返回 NULL 。

MySQL 中处理 NULL 使用 IS NULL 和 IS NOT NULL 运算符。

# MySql正则表达式

# MySQL 事务(重要)

 https://www.runoob.com/mysql/mysql-transaction.html 

ACID：原子性，一致性，隔离性、持久性





隔离级别：读未提交  读提交   重复读   串行化

### MYSQL 事务处理主要有两种方法：

1、用 BEGIN, ROLLBACK, COMMIT来实现

- **BEGIN** 开始一个事务
- **ROLLBACK** 事务回滚
- **COMMIT** 事务确认

这个和redis一样

>  mult：开始事务
>
> 压入命令...
>
> [放弃事务discard]
>
> exec：提交事务，执行命令集



> begin;   开启事务
>
> 压入sql语句...
>
> [事务回滚rollback]
>
> commit：事务确认，提交事务。



2、直接用 SET 来改变 MySQL 的自动提交模式:

- **SET AUTOCOMMIT=0** 禁止自动提交
- **SET AUTOCOMMIT=1** 开启自动提交





savepoint   savepoint_name;设置保留点，

Rollback to  savepoint_name：回滚到某个保留点

release  savepoint   savepoint_name；删除保留点。

![1588130705515](D:\TyporaDir\实践\pageheleper.assets\1588130705515.png)



# Alert   修改表名、修改添加字段

![1588130925517](D:\TyporaDir\实践\pageheleper.assets\1588130925517.png)

![1588131083298](D:\TyporaDir\实践\pageheleper.assets\1588131083298.png)

# MySQL 索引

 https://www.runoob.com/mysql/mysql-index.html 

单列索引和组合索引

 单列索引，即一个索引只包含单个列，一个表可以有多个单列索引， 

 组合索引，即一个索引包含多个列。 

创建索引时，你需要确保该索引是应用在 SQL 查询语句的条件(一般作为 WHERE 子句的条件)。

实际上，索引也是一张表，该表保存了主键与索引字段，并指向实体表的记录。

上面都在说使用索引的好处，但过多的使用索引将会造成滥用。因此索引也会有它的缺点：虽然索引大大提高了查询速度，同时却会降低更新表的速度，如对表进行INSERT、UPDATE和DELETE。因为更新表时，MySQL不仅要保存数据，还要保存一下索引文件。

建立索引会占用磁盘空间的索引文件。