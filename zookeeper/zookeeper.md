# 一、zookeeper的应用场景

## 1、概述

zookeeper是一个经典的**分布式数据一致**性解决方案。致力于为分布式应用提供一个**高性能、高可用**、且具有严格**顺序访问控制能力的分布式协调存储服务**。

- 维护配置信息
- 分布式锁服务
- 集群管理
- 生成分布式唯一id

> 高性能：zk将数据存储在内存中，直接服务于客户端的所有非事务请求，非常适用于以读为主的场景。
>
> 高可用：zk一般需要配置集群。
>
> 顺序访问：对于客户端的每个写的请求，zk都会分配一个全局唯一的ID，这个ID反映了所有事务的操作的先后顺序。





## 2、zk的数据模型

zk的数据节点可以视为树状结构。数据节点兼具文件和目录两种特点。既像文件一样维护了节点的数据，ACL等数据结构，又像目录一样可以作为路径标识的一部分。

![1592722374522](D:\TyporaDir\zookeeper\zookeeper.assets\1592722374522.png)



一个节点大体上可以分为三部分：

- 节点的数据
- 节点的子节点
- 节点的状态stat：用来描述当前节点的的创建、属性等。



节点状态stat的属性：

> - cZxid       数据节点创建时的事务ID
> - ctime       数据节点创建时间
> - mZxid      数据节点最后一次更新的事务ID
> - mtime     数据节点最后一次更新的时间
> - pZxid       子节点最后一次被修改的事务ID
> - cversion   子节点更改次数
> - dataVersion     数据节点版本
> - aclVersion        节点ACL的更改次数
> - ephemeralOwmer    表示临时节点
> - dataLength      数据长度
> - numChildren     当前节点的子节点个数。



节点类型：

zk节点分为永久节点和临时节点。

永久节点，不依赖于session会话，就算会话结束，永久节点的数据也不会被删除。只有执行删除操作才能删除数据。

临时节点：依赖于session会话，会话结束，节点就会被删除。临时节点不允许有子节点。



# 二、zk的基本使用

## 1、启动

在linux中**解压zk的压缩包**，复制conf下的配置文件：如下：

```shell
 cp zoo_sample.cfg  zoo.cfg
```

zoo.cfg是zk启动时默认加载的配置文件，如果不复制为zoo.cfg,那么启动时需要手动指定启动的配置文件。



**启动zk**。启动的执行文件在bin下，启动命令如下：

```shell
./zkServer.sh start    ##方式一：复制了配置文件
./zkServer.sh start   zoo_sample.cfg  ##  方式二：使用指定的配置文件去启动
```



**连接客户端**。在bin下有zk自带的客户端。连接命令如下：

```shell
./zkCli.sh 
```



**退出连接**

```shell
quit
```



**关闭zk**

```shell
./zkService.sh stop
```





## 2、常见的有关节点的命令

### 2.1节点类型、创建节点

在zk中，节点分为**有序节点**和**临时节点**两大类   

创建节点的命令：

```shell
create [-s] [-e] /path data   ##-s 表示创建有序节点   -e表示创建临时节点
```

如果什么参数都不带，**默认创建的就是持久化节点**。

所以总的来说，一共有4种节点：

- **持久化节点**
- **持久化有序节点**
- **临时节点**
- **临时有序节点**

```shell
#创建持久化节点   zk宕机重启也不丢失数据
[zk: localhost:2181(CONNECTED) 0] create /node1  a  ##创建节点
Created /node1
[zk: localhost:2181(CONNECTED) 1] get /node1    ##获取节点
a
cZxid = 0x4
ctime = Thu Jun 18 23:38:05 CST 2020
mZxid = 0x4
mtime = Thu Jun 18 23:38:05 CST 2020
pZxid = 0x4
cversion = 0
dataVersion = 0
aclVersion = 0
ephemeralOwner = 0x0
dataLength = 1
numChildren = 0


#创建持久化有序节点。 持久化有序节点常用于分布式时生成唯一ID
[zk: localhost:2181(CONNECTED) 1] create -s /node2 b  ##-s创建有序节点
Created /node20000000001
[zk: localhost:2181(CONNECTED) 2] get /node20000000001  #获取节点
b
cZxid = 0xb
ctime = Thu Jun 18 23:42:04 CST 2020
mZxid = 0xb
mtime = Thu Jun 18 23:42:04 CST 2020
pZxid = 0xb
cversion = 0
dataVersion = 0
aclVersion = 0
ephemeralOwner = 0x0
dataLength = 1
numChildren = 0


#创建临时节点。zk宕机获取客户端断开连接，数据就丢失了
[zk: localhost:2181(CONNECTED) 3] create -e /node3 c  ##创建节点
Created /node3  
[zk: localhost:2181(CONNECTED) 4] get /node3   ##获取数据
c
cZxid = 0xc
ctime = Thu Jun 18 23:43:54 CST 2020
mZxid = 0xc
mtime = Thu Jun 18 23:43:54 CST 2020
pZxid = 0xc
cversion = 0
dataVersion = 0
aclVersion = 0
ephemeralOwner = 0x172c8173e950000
dataLength = 1
numChildren = 0

#创建有序临时节点。常用于分布式锁
create -s -e /node4 d

```





### 2.2更新节点数据

使用set命令修改节点的数据。命令如下：

```shell
[zk: localhost:2181(CONNECTED) 3] set /node1 aa  #修改/node1的数据为 aa
cZxid = 0x4
ctime = Thu Jun 18 23:38:05 CST 2020
mZxid = 0xf
mtime = Thu Jun 18 23:51:01 CST 2020
pZxid = 0x4
cversion = 0
dataVersion = 1     ##每次修改，数据版本都会加1
aclVersion = 0
ephemeralOwner = 0x0
dataLength = 2
numChildren = 0


##类似乐观锁的修改
set /node1 aaa 1     #当dataVersion不为1时，无法修改/node1节点的数据。
```





### 2.3删除节点。

操作和set**类似**

```shell
delete /node1    ##删除节点/node1

delete  /node1  2  ##当版本号为2时才能删除
```

当要删除的节点下面还有子节点时，可以使用rmr递归删除该节点以及该节点的子节点。

```shell
rmr /node1
```





### 2.4查看节点

```shell
get /node1   ##查看节点数据

stat /node1 ##查看节点属性，不包括数据


ls /node1  ##查看该节点的子节点
ls2 /node1  #查看该节点的子节点，并显示节点属性

[zk: localhost:2181(CONNECTED) 3] ls /node1  #查看该节点的子节点
[]  #子节点为空
[zk: localhost:2181(CONNECTED) 4] ls2 /node1
[]  #子节点为空
cZxid = 0x4    #显示节点属性
ctime = Thu Jun 18 23:38:05 CST 2020
mZxid = 0x12
mtime = Thu Jun 18 23:52:59 CST 2020
pZxid = 0x4
cversion = 0
dataVersion = 2
aclVersion = 0
ephemeralOwner = 0x0
dataLength = 3
numChildren = 0


```





### 2.5监听节点

```shell
get /node1 watch   #监听/node1节点，当这个节点发生改变时，会通知客户端。一个监听器只会监听一次

stat /node1 watch  #监听/node1节点，当这个节点发生改变时，会通知客户端

ls /node1 watch   #监听子节点的增加或者修改操作
ls2 /node1 watch  #监听子节点的增加或者修改操作
```





# 三、zk中节点的权限

## 3.1、概述

acl权限控制，使用scheme：id： permission 来标识。

权限模式【scheme】：表示授权的策略

id： 表示授权对象

permission：表示授予的权限



在zk中，需要为每个节点单独设置权限。子节点不继承父节点的权限。



每个节点能**同时支持多种权限控制**方案和多个权限。



## 3.2权限模式

 采用何种方案来授权

- world---------对于登入到zk的用户都有权限
- ip--------------只有某个ip才有权限
- auth----------使用添加认证的用户
- digest-------使用用户名：密码的方式认证



## 3.3权限

- create-------------c---------------可以创建子节点
- delete-------------d--------------可以删除子节点
- read----------------r--------------可以读取节点数据并显示子节点列表
- write---------------w-------------可以设置节点数据
- admin------------a--------------可以管理节点权限



## 3.4、授权命令

- getAcl--------------获取Acl权限
- setAcl--------------设置Acl权限
- addauth----------添加认证用户

例子：

```shell
setAcl /node1 ip:192.168.126.6:crwda  #给node1设置权限


#添加认证用户
addauth digest u1:123456
setAcl /node1 auth:u1:crwda  #设置权限



```





























## zk中集群之间数据的一致性

zk集群之间有三种角色：leader、follower、observer

zk的一致性协议：zab（zookeeper Atomic  BroadCast）

zab的工作原理：

> leader从客户端接收到一个写的请求，为这个请求生成一个事务，并为这个事务生成一个唯一的事务id(Zxid),然后leader将这个事务提议（propose）发送给所有的follower节点。跟随者将接收到的事务请求加入到历史队列中，并发送ack给leader。当leader接收到大多数的ack消息时，leader会发送commit请求给follower。follower收到commit请求后，就会从队列中将事务commit。







## zk中的leader选举

分两种:启动时的leader选举、运行时的leader选举。

### 服务器状态：

looking：寻找leader状态，集群中没有leader。进入选举状态

leading：领导者状态，表示当前服务器是leader

following：跟随者状态，表示当前服务器是follower。

observing：观察者状态，表示当前服务器角色是observer。

### 启动时的选举：

当只有一台服务器启动时，是无法进行选举的。当有第二台服务器启动时，开始进行选举。选举过程：

> 1.每个server都会发出一个投票，都先投向自己。投票中包含了这个服务器的myid和zxid。比如只有两个服务器时，投的分别为（1,0）,(2,0)。
>
> 2.集群中每台服务器接收来自其他服务器的投票
>
> 3.比较投票。先检查zxid，zxid大的作为leader，小的作为follower。如果zxid相同，则比较服务器id，服务器id大的做为leader，小的作为follower。如上（1，0），（2，0），他们的zxid都为0，所以比较myid。显然第二个服务器的id更大，所以服务器2应该作为leader。所以再次投票，所有的服务区都给2投票。
>
> 4.统计投票，每次投票后，服务器都会统计投票信息。判断是否已经有过半的机器接收到相同的投票信息。
>
> 5.统计了投票信息后，就会确定leader。服务器更新自己的状态，将自己的状态改为leader或者follower。

注意：一共会进行2次投票。第一次投向自己。第二次投向应该成为leader的服务器。一旦选出了leader。后面启动的服务器只能做follower了。

## 运行时的选举

在运行过程中，当leader宕机后，余下的服务器会进入looking状态，然后进入选举过程。选举过程和启动时一样，先比较zxid，相同则比较myid。