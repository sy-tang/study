![1585831063857](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585831063857.png)

![1585830802090](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585830802090.png)

![1585833320362](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585833320362.png)











## CRUD内容总结

### 

#### 		1、web.xml的配置过程

​				配置contextConfigLocaltion   ------>配置spring的配置文件的地址

​				配置监听器：contextLoaderListener

​				配置前端控制器：DispatcherServlet,配置contextConfigLocaltion--配置springmvc的地址  拦截/

​				配置filter，字符编码  encoding  CharacterEncodingFilter  拦截/*    forceRequestEncoding  true

​				配置restful风格的url，， HttpPutFormContentFilter 和HiddenHttpMethodFilter

#### 		2、配置springmvc的配置文件，

​				扫描Controller中的包  component-Scanner 

​				配置视图解析器InternalResourceViewResolver  ----WEB-INF/views/     .jsp

​				配置两个：annotation—driven       default-servlet-handler

#### 		3、配置spring的配置文件

​				扫描注解，除了不扫描被Controller标志的组件

​				配置数据源   DrivenManagerDataSource   url dirverName  pwd  username

​				整合Mybatis    SqlSessionFactoryBean  关联数据源，起别名、mapper文件的地址，mybatis配置文件的 地址

​				将mybatis、mapper扫描进容器中   MapperScannerConfiguration   mapper文件地址

​				配置事务管理 （用于管理数据源） DataSourceTransactionMannger   引入数据源

​				配置切点（aop） 关联增强事务

​				配置增强事务，关联事务管理

#### 4、配置mytais.xml

​				开启驼峰命名

​				引入PageHelper

#### 5、配置Mybatis逆工程，自动生成mapper文件，pojo等（Mybatis Generator）

 http://mybatis.org/generator/configreference/xmlconfig.html 

#### 6、引入pageHelper插件  （分页插件） 

https://github.com/pagehelper/Mybatis-PageHelper/blob/master/README_zh.md 

```xml
		<dependency>
            <groupId>com.github.pagehelper</groupId>
            <artifactId>pagehelper</artifactId>
            <version>5.0.0</version>
        </dependency>
```

#### 7、使用spring单元测试

```xml
			<!--      spring单元测试--> 		
		<dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>4.1.6.RELEASE</version>
        </dependency>
        <!--    mybatis逆向工程jar-->
        <dependency>
            <groupId>org.mybatis.generator</groupId>
            <artifactId>mybatis-generator-core</artifactId>
            <version>1.3.6</version>
        </dependency>
		<!--      JSR303--> 	
		<dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>5.4.1.Final</version>
        </dependency>
```

#### 8、MockMvc

#### 9、bootstrap的部分使用

#### 10、ajax

```jsp
//获取项目名称    /crud       前带/  后不带斜杠
<%    pageContext.setAttribute("PATH", request.getContextPath());%>
```

​	在ajax中可以直接发delete和put请求，但是在使用put请求时，要配置HttpPutFormContentFilter，tomcat只包装post请求的数据，将数据包装为Map，后端使用request.Paramter获取数据时，就是从Map中获取。attr可以给元素添加属性，prop适合操作dom原生的属性，比如使用prop获取checked是否选中或者设置值。parents（“tr”).find("th.eq(0)")，直接获取父级tr中第一个th的对象  $("#check_one:checkde")获取所有被选中的id为checked_one的元素。

```javascript
$("#myModal form").serialize()   //将form表单中的数据序列化，直接获取表单中的所有数据

$("#updModal input[name=gerder]").val([msg.extend.emp.gerder])//在val中可以填写要选中的值
$("#updModal select").val([msg.extend.emp.dId])//将要在列表选中
confirm()//提示框
```

![1585983070216](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585983070216.png)

reset：重置

find（“*”）:查找

removeClass:清除class样式

#### 11、相关注解

​		@RequestParam：类似Request.getParamter，获取请求中指定的参数

​		@PathVariable ：获取url中的参数

​		@Valid  ：配和JSR303进行校验，result就是校验结果

​		@Pattern ：pojo中标注在属性上，设置校验规则，表示该属性要按规则进行校验

![](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585983713270.png)

#### 12、设置了一个专门用于返回信息的类Msg类

​			包含code（定义状态码）、msg(定义成功或者失败的信息)、Map<String,Object>类型的 extend属性,可以将执行结果添加到Map中，返回到页面去。



#### 13、配置文件

##### 	1、web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>

<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         id="WebApp_ID" version="3.1">
<!--配置spring-->
  <context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>classpath:applicationContext.xml</param-value>
  </context-param>
  
<!--  配置监听器-->
  <listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
  </listener>

<!--  配置springmvc-->
  <servlet>
    <servlet-name>springmvc</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>classpath:springmvc.xml</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
  </servlet>
  <servlet-mapping>
    <servlet-name>springmvc</servlet-name>
    <url-pattern>/</url-pattern>
  </servlet-mapping>

<!--  配置字符编码，一定要在最前面-->
  <filter>
    <filter-name>encoding</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
    <init-param>
      <param-name>encoding</param-name>
      <param-value>utf-8</param-value>
    </init-param>
    <init-param>
      <param-name>forceRequestEncoding</param-name>
      <param-value>true</param-value>
    </init-param><init-param>
      <param-name>forceResponseEncoding</param-name>
      <param-value>true</param-value>
    </init-param>
  </filter>
  <filter-mapping>
    <filter-name>encoding</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>

<!--  使用curd风格-->
<!--  允许在ajax中直接使用put请求-->
  <filter>
    <filter-name>HttpMethodFilter</filter-name>
    <filter-class>org.springframework.web.filter.HttpPutFormContentFilter</filter-class>
  </filter>
  <filter-mapping>
    <filter-name>HttpMethodFilter</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>
  <!--
        将POST请求转化为DELETE或者是PUT
         要用_method指定真正的请求参数
  -->
  <filter>
    <filter-name>Hidden</filter-name>
    <filter-class>org.springframework.web.filter.HiddenHttpMethodFilter</filter-class>
  </filter>
  <filter-mapping>
    <filter-name>Hidden</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>
</web-app>

```



##### 2、applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop" xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd">

    <!--配置扫描-->
    <context:component-scan base-package="com.project">
        <context:exclude-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
    </context:component-scan>
    <!--    导入properties文件-->
    <context:property-placeholder location="classpath:db.properties"/>

    <!--    配置数据源-->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="${jdbc.driver}"/>
        <property name="url" value="${jdbc.url}"/>
        <property name="username" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>
    </bean>

    <!--    注册一个可批量操作的sqlSession-->
    <bean id="sqlSession" class="org.mybatis.spring.SqlSessionTemplate">
        <constructor-arg name="sqlSessionFactory" ref="factory"/>
        <constructor-arg name="executorType" value="BATCH"/>
    </bean>

    <!--    整和mybatis-->
    <bean id="factory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <!--        引入数据源-->
        <property name="dataSource" ref="dataSource"/>
        <!--        配置别名-->
        <property name="typeAliasesPackage" value="com.project.pojo"/>
        <!--        配置mapper的文件地址-->
        <property name="mapperLocations" value="classpath:mapper/*.xml"/>
        <property name="configLocation" value="classpath:mybatis.xml"/>
    </bean>
    <!--    将mybatis扫描到ioc容器中-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="sqlSessionFactoryBeanName" value="factory"/>
<!--        扫描mapper文件-->
        <property name="basePackage" value="com.project.mapper"/>
    </bean>
<!--    配置事务-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
<!--        引入要管理的数据源-->
        <property name="dataSource" ref="dataSource"/>
    </bean>

<!--    配置切入点-->
    <aop:config>
<!--        配置切点-->
        <aop:pointcut id="mypoint" expression="execution(* com.project.service.*.*(..))"/>
<!--        配置增强事务-->
        <aop:advisor advice-ref="txAdvice" pointcut-ref="mypoint"/>
    </aop:config>
<!--    配置事务增强，关联事务管理-->
    <tx:advice id="txAdvice" transaction-manager="transactionManager">
        <tx:attributes>
            <tx:method name="*"/>
            <tx:method name="sel*" read-only="true"/>
            <tx:method name="get*" read-only="true"/>
        </tx:attributes>
    </tx:advice>
</beans>

```



##### 3、springmvc.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc.xsd">
<!--配置扫描-->
    <context:component-scan base-package="com.project" use-default-filters="false">
        <context:include-filter type="annotation" expression="org.springframework.stereotype.Controller"/>
    </context:component-scan>

<!--   配置视图解析器-->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/views/"/>
        <property name="suffix"  value=".jsp"/>
    </bean>

<!--    两个配置-->

    <mvc:annotation-driven/>
    <mvc:default-servlet-handler/>
<!--    配置静态资源的寻找方式-->
<!--    <mvc:resources mapping="" location=""-->

</beans>
```



##### 4、mybatis

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <settings>
        <setting name="mapUnderscoreToCamelCase" value="true"/>
    </settings>
    <plugins>
        <plugin interceptor="com.github.pagehelper.PageInterceptor">
            <!-- config params as the following -->
<!--            <property name="param1" value="value1"/>-->
            <property name="reasonable" value="true"/>
        </plugin>
    </plugins>
</configuration>
```





##### 5、mbg

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE generatorConfiguration
        PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">

<generatorConfiguration>

    <context id="DB2Tables" targetRuntime="MyBatis3">
<!--        不生成注释-->
        <commentGenerator>
            <property name="suppressAllComments" value="true" />
        </commentGenerator>

        <jdbcConnection
                driverClass="com.mysql.cj.jdbc.Driver"
                        connectionURL="jdbc:mysql://localhost:3306/yiwu?serverTimezone=Asia/Shanghai"
                        userId="root"
                        password="root">
        </jdbcConnection>

        <javaTypeResolver >
            <property name="forceBigDecimals" value="false" />
        </javaTypeResolver>
<!--指定javabean生成的位置-->
        <javaModelGenerator targetPackage="com.project.pojo"
                            targetProject="D:\\MyIdeaProject\\springdemo\\CRUD\\src\\main\\java">
            <property name="enableSubPackages" value="true" />
            <property name="trimStrings" value="true" />
        </javaModelGenerator>
<!--指定sql映射文件生成的位置-->
        <sqlMapGenerator targetPackage="mapper"
                         targetProject="D:\\MyIdeaProject\\springdemo\\CRUD\\src\\main\\resources">
            <property name="enableSubPackages" value="true" />
        </sqlMapGenerator>
<!--指定dao接口生成的位置-->
        <javaClientGenerator type="XMLMAPPER"
                             targetPackage="com.project.mapper"
                             targetProject="D:\\MyIdeaProject\\springdemo\\CRUD\\src\\main\\java">
            <property name="enableSubPackages" value="true" />
        </javaClientGenerator>
<!--指定每个表生成的策略-->
<!--                             表名称                  生成的bean的名称-->
        <table tableName="tbl_emp" domainObjectName="Employee" ></table>
        <table tableName="tnl_dept" domainObjectName="Department"></table>

    </context>
</generatorConfiguration>
```

 耗时1024东，
精心整理了2020Java全面系统的学习路线

Java基础：[av80585971](https://www.bilibili.com/video/av80585971/)

数据库
Mysql：[av68811608](https://www.bilibili.com/video/av68811608/)
Oracle：[av81386804](https://www.bilibili.com/video/av81386804/)
JDBC：[av68736927](https://www.bilibili.com/video/av68736927/)
C3P0：[av73840600](https://www.bilibili.com/video/av73840600/)

前端技术
HTML、CSS、Javascript：[av73840600](https://www.bilibili.com/video/av73840600/)
jQuery-：[av37976701](https://www.bilibili.com/video/av37976701/)
Ajax：[av15657082](https://www.bilibili.com/video/av15657082/) [av15866060](https://www.bilibili.com/video/av15866060/)
Vue：[av76249419](https://www.bilibili.com/video/av76249419/?spm_id_from=333.788.b_636f6d6d656e74.68)
微信小程序：[av73342655](https://www.bilibili.com/video/av73342655/?spm_id_from=333.788.b_636f6d6d656e74.69) [av40455083](https://www.bilibili.com/video/av40455083/)

动态网页：[av73840600](https://www.bilibili.com/video/av73840600/)

编程强化
JVM优化：[av70549061](https://www.bilibili.com/video/av70549061/)
数据结构算法：[av83826038](https://www.bilibili.com/video/av83826038/?spm_id_from=333.788.b_636f6d6d656e74.73) [av78639604](https://www.bilibili.com/video/av78639604/)

软件项目管理
Maven： [av52364221](https://www.bilibili.com/video/av52364221/)
SVN：[av73840600](https://www.bilibili.com/video/av73840600/) [av7959491](https://www.bilibili.com/video/av7959491/)
Git：[av75718460](https://www.bilibili.com/video/av75718460/)
Jenkins：[av83089670](https://www.bilibili.com/video/av83089670/)

热门技术框架
SSM：[av7770852](https://www.bilibili.com/video/av7770852/)
Mybatis-Plus ：[av69233006](https://www.bilibili.com/video/av69233006/)
Spring Data：[av71980390](https://www.bilibili.com/video/av71980390/)
Spring：[av70828462](https://www.bilibili.com/video/av70828462/)

分布式架构
Dubbo、Zookeeper：[av74933720](https://www.bilibili.com/video/av74933720/)
SpringCloud：[av77085442](https://www.bilibili.com/video/av77085442/) [av89898642](https://www.bilibili.com/video/av89898642/)
Eureka：[av74127911](https://www.bilibili.com/video/av74127911/)
Skywalking：[av80349807](https://www.bilibili.com/video/av80349807/)

服务器中间件
RocketMQ：[av66702383](https://www.bilibili.com/video/av66702383/)
Kafka：[av71991051](https://www.bilibili.com/video/av71991051/)
Sharding-JDBC：[av68736927](https://www.bilibili.com/video/av68736927/)
Redis：[av76235738](https://www.bilibili.com/video/av76235738/)
MongoDB：[av80450883](https://www.bilibili.com/video/av80450883/)

服务器技术
Tomcat：[av67233983](https://www.bilibili.com/video/av67233983/)
Linux系统：[av73840600](https://www.bilibili.com/video/av73840600/)
CentOS、Virtualbox：[av23360560](https://www.bilibili.com/video/av23360560/)
Ubuntu、Vmware：[av14184325](https://www.bilibili.com/video/av14184325/)

容器技术
Docker：[av69121091](https://www.bilibili.com/video/av69121091/)

业务解决方案（实战项目）
Elasticsearch：[av66600360](https://www.bilibili.com/video/av66600360/)
Lucene：[av77895717](https://www.bilibili.com/video/av77895717/)
Quartz：[av36062910](https://www.bilibili.com/video/av36062910/)
Elastic-Job：[av74128695](https://www.bilibili.com/video/av74128695/)
SpringSecurity：[av74851468](https://www.bilibili.com/video/av74851468/)
Activiti：[av7670054](https://www.bilibili.com/video/av7670054/) 