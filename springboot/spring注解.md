```java
AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfigOfPropertyValues.class);
//获取ioc容器
@configuration:表示是一个配置类

@ComponentScan（value=“com.atguigu”,includeFilters={ @Filter(type=FilterType.Annotation,classes={Controller.class})}，useDefaultFilters=false)//扫描com.atguigu下的包，按注解的方式过滤掉,这里表示只包含被comtoller注解的类（组件），其他被@Service/@R**注解的组件不扫描，还可以多次使用@Filter来配置很多规则，，比如正则等，还可以自定义规则（CUSTOM）编写自己的规则要编写一个实现接口TypeFilter的类，return true表示将符合规则的组件加容器中，false表示不加到容器中。每一个在扫描范围内的组件都要进行规则判断，符合才被加入到容器中
                                                   
    
    /*通过bean注解注入ioc容器的组件默认是单实例，一经实例就无法改变
    @Scope指定作用范围  prototype、singleton、request、session
    prototype：多实例  ioc容器启动时不调用方法创建对象，需获取对象时才创建，获取几次就创建几次
    singleton：单实例（默认@bean是单实例），ioc容器启动会调用方法创建对象到ioc容器中，以后每次获取就是直接从容器中拿这个创建好的对象
    request：同一次请求创建一次
    session：同一个session创建一次
    
    
    @Scope("prototype")
    @bean//所创建的组件不是单实例了
    */
```

@Filter中过滤的类型，按注解，按类型，custom自定义

![1583647886903](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1583647886903.png)

```java

/*懒加载 @Lazy 
	单实例bean：默认在容器启动时创建对象
	懒加载：容器启动时先不创建对象，第一次获取bean时创建对象，并初始化 
*/
@Lazy
@Bean
public  组件
```

```java
/**
@Conditional：按照条件进行判断，给容器中添加组件，满足条件给容器中注册bean  可以放在方法上也可以放在类上
编写自己的条件需要编写一个实现接口Condition的类，重写里面的方法
context.getBeanFactory()//获取ioc创建、装配Bean的工厂
context.getClassLoader()//获取类加载器
context.getEnviroment();获取环境，可以通过它获取环境变量、系统名称（win还是linux）environment.getProperty（"os.name"）
context.getRegistry()//获取bean定义的注册类，注册bean的定义

*/
@Conditional({WindowsCondition.class})//系统是win时添加bean
@Bean
```

## 1、注册组件的方式

```java
/*给容器中注册组件：
    1、包扫描+组件标注注解（@Controller/@Service/@Repository/@Component）自己写的类
    2、@Bean   第三方导入的包
    3、@Import（快速给容器中导入一个组件）
    	1）、@Import({Color.class,Red.class})//导入两个组件，id默是类名
    	2）、ImportSelector：导入选择器  返回需要导入到容器中的组件的全类名数组； 编写自定义需要返回的组件类时需要实现ImportSelector接口
    	3）、ImportBeanDefinitionRegistrar
    	编写它的实现类，从写里面的方法，使用registrar.registrarBeanDefinition("组件名id"，beanDefinition)给容器中手动注册组件(RainBow)
    	RootBeanDefinition beanDefinition=new RootBeanDrfinition（RainBow.class）;//指定bean的类型
    	4）、使用spring提供的FactoryBean(工厂Bean)
    	实现接口FactoryBean<组件对象的类型>，
    	public color getObject() throw Exception{
    	return new color()
    	}
    	返回一个color对象，并添加到容器中
    	&xxxx,获取工厂bean本身
    */
    

	
    
```

## 2、Bean的声明周期

```java
/*
Bean的声明周期；
  bean创建->初始化->销毁的过程
  容器管理bean的生命周期
  我们可以自定义初始化和销毁方法，容器在bean进行到当前声明周期时来调用我们自定义的初始化和销毁方法
  
  初始化：容器启动时，对象创建完成并赋值好，调用初始化方法
  销毁：
  	单实例时，容器关闭时销毁
  	多实例时，容器只会将对象创建好，不会管理这个bean，不调用销毁方法，手动调用销毁方法
  		
  
  1）、自定义初始化和销毁方法
     以前是在<bean>中指定init-method和destroy-method   
     注解方式：@Bean（initMethod="方法名称",destroyMethod="销毁方法"）
  2）、通过实现InitializingBean接口，定义初始化逻辑 
  		通过实现DisposableBean接口，定义销毁逻辑
  3）、可以使用JSR250
  		@PostConstruct：在bean创建完成并且属性赋值完成时来执行初始化
  		@PreDestroy：在容器销毁bean之前通知清理工作
  		都是标注在方法上
  4)、BeanPostProcessor接口：bean的后置处理器
  	在bean初始化前后进行一些处理工作（配置了后所有的bean在初始化之前都将进行处理工作）
  	postProcessBeforeInitialization：在bean初始化之前进行后置处理工作
  	postProcessAfterInitialization：在bean初始化之后进行后置处理工作
*/
```

BeanPostProcessor在底层非常重要

```java
属性赋值
    在属性上直接使用
    @Value("张山")
    @Value("#{20-2}")
    @Value("${***}")取配置文件中的值【***.properties】先使用@PropertySource(value={"classpath:/person.properties"})读取外部配置文件中的key、value，保存到环境变量中，也可以通过environment获取
```

3、自动装配

```java
自动装配：spring中利用依赖注入（DI），完成对ioc容器中各个组件的依赖关系的赋值
    /*
   1、 @Autowired   自动注入  默认true  【spring的注解】
    	默认优先按照类型去找，若有多个相同类型的组件，则按照名字作为组件的id去容器中查找。可以使用@Qualifier("组件id")来明确指定使用的组件。
    	使用自动装配时，默认该组件必须赋值好（在容器中）。当使用@Autowired(required=false)时，该组件找得到就找，找不到就为null，不用必须在容器中了
    	
    @Primary: 让spring进行自动装配组时，默认使用首选的bean
    	这个注解标注在一个组件上，表示当容器中有多个与该组件类型相同的组件时，优先使用该组件。使用了这个注解（首选装配）后，就不能使用@Qualifier(明确指定使用哪个组件)，否则还是使用指定的组件
    	意思就是没有明确指定时，使用首选装配，有明确指定则使用明确指定了。
    	
    	
    2、spring还支持使用@Resource（JSR250）和@Inject（JSR330）    【这两个注解是java规范的注解】
    @Resource：默认按照组件名称来进行装配，不能结合@Qualifier和@Primary注解，可以使用@Resource(name="***组件名")来指定装配的组件
    @Inject：使用时需要导入依赖，和Autowired一样使用，没有@Autowired（required=false）的功能
    
    3）、@Autowired能标在构造器、参数、方法、属性 
    	1、这个注解标注在方法（一般标在set方法）上时，spring容器创建当前对象，会调用这个方法，完成赋值。方法使用的参数，自定义类型的值会从ioc容器中获取（既参数是一个对象时，这个参数的值会去容器中寻找对应的bean）
    	默认情况下，加在ioc容器中的组件，容器启动时会调用无参构造器来创建对象，再进行初始化赋值等操作
    	2、构造器：标注在一个组件的有参构造器上时，spring启动时会调用有参构造器。构造器需要的组件，也是从容器中获取。如果组件只有一个有参构造器，则可以省略@Autowired，参数位置的组件还是可以从容器中获取
    	3、标注在参数位置：都是从容器中获取参数组件的值
    */
```

自定义组件想要使用spring容器中底层的一些组件，比如：ApplicationContext、BeanFactory,....；自定义组件只需要实现xxxAware接口，在创建对象时，会调用接口规定的方法注入相关的组件  总接口Aware

![](D:\TyporaDir\images\1583570688714.png)

每一个xxxAware：功能使用xxxProcessor



@PropertySource：扫描properties文件

```java
profile:spring为我们提供的可以根据当前环境，动态的激活和切换一系列组件的功能
   //@Profile：指定组件在哪个环境的情况下才能被注册到容器中，不指定的话，任何环境都能注册这个组件。
    /*
    加了profile的bean，只有在这个环境被激活的时候才能注册到容器中。默认情况下时default环境
    写在配置类上时，只有该环境生效，整个配置类里配置的组件才能生效
    没有标注环境标识（profile）的组件，任何环境下都能被加载。
    */
    
    
    /*
    激活profile
    1）、命令行的方式   -Dspring-profile-active=dev/test
    2）、代码的方式，
    使用无参构造获取ioc容器，
    给ioc容器进行环境配置    applicationContext.getEnviorment.setActiveProfiles("test","dev");可以设置激活多个环境	
    注册主配置类
    applicationContext.register(***.class)
    启动刷新容器
    applicationContext.refresh();
    */
```

```java
AOP
    前置通知：@Befor
    后置通知：@After	 
    返回通知：@AfterReturning
    异常通之：@AfterThrowing
    环绕通知：@Aruond
    给切面类中通知方法标注这些通知注解，（中放入切点方法）
    
    @Pointcut 切点方法
    
    需要将他们加入到容器中，还必须告诉哪个是切面类，使用@Aspect标注切面类
    在配置类（@Configuration）中添加@EnableAspectJAutoProxy,开启通知的自动代理
```

![](D:\TyporaDir\images\NM2_S7}~9(U7_`VJ[]6JP)P.jpg)

## 4、注解配置的springmvc

1、导入相关的 jar包（pom依赖）

```xml
 <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>4.3.11.RELEASE</version>
    </dependency>

    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>servlet-api</artifactId>
      <version>3.0-alpha-1</version>
      <scope>provided</scope>
    </dependency>
```

2、每个web启动时，会扫描每个jar包下的META-INT/services/SpringServletContainerInitializer

SpringServletContainerInitializer中的onStartup方法会将类型为WebApplicationInitializer组件导入，自己本身不导入，只导入他的子孙类的组件。

springmvc底层中实现webAppInitializerClasses的接口有以下三种

![1585794687937](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585794687937.png)

```java
1）、AbstractContextLoaderInitializer
2）、AbstractDispatcherServletInitializer
3）、AbstractAnnotationConfigDispatcherServletInitializer//通过实现这个接口，可以实现注解配置的springmvc
```

3、实现AbstractAnnotationConfigDispatcherServletInitializer接口，重写其中三个方法。

编写两个类，使用ComponetScan将自己编写的组件扫描到容器中（就像以前xml中扫描service，controller，dao等包）



```java


public class MyAppInitialzer extends AbstractAnnotationConfigDispatcherServletInitializer {
    //配置spring的配置
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{RootConfig.class};
    }

    //配置springmvc的配置
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{AppConfig.class};
    }

    //配置拦截的路径
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
```

4、@EnableWebMvc，启动webmvc中的自定义配置，继承WebMvcConfigurerAdapter可以进行mvc的其他配置，比如（以前xml中配置的路径跳转，拦截器等）。

```java
@ComponentScan(value = "com.project",includeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION,classes = {Controller.class})
},useDefaultFilters = false)
@EnableWebMvc
//扫描controller层的组件
public class AppConfig extends WebMvcConfigurerAdapter {
    @Autowired
    MyInterceptor myInterceptor;
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.jsp("/WEB-INF/page/",".jsp");
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(myInterceptor).addPathPatterns("/**");
    }
}

```

## 5、**异步请求**

​	注解配置的springmvc中有两种方法

方法一：使用Callable

```java
//使用Callable，异步请求
    @ResponseBody
    @RequestMapping("/abc")
    public Callable<String> async01(){
        System.out.println("主线程开始"+Thread.currentThread());
        //创建callable
        Callable<String> callable = new Callable<String>(){
            //复写call方法
            @Override
            public String call() throws Exception {
                System.out.println("副线程开始"+Thread.currentThread());
                Thread.sleep(20000);
                System.out.println("副线程结束"+Thread.currentThread());
                //返回这个请求执行的结果
                return "sdfsd";
            }
        };
        System.out.println("主线程结束"+Thread.currentThread());
		//返回callable
        return callable;
    }
/*
主线程一开始就结束，请求所需要的操作有副线程执行并返回。
*/
```

方法二：使用DeferredResult（推迟结果）

```java
/*
    使用DeferredResult实现异步请求
     */
    @ResponseBody
    @RequestMapping("createOrder")
    public DeferredResult<Object> createOrder(){
        //主线程一开始就结束
        //先创建一个deferredResult，初始化响应时间，超过响应时间的信息
        DeferredResult<Object> deferredResult = new DeferredResult<>((long) 3000, "create fail");
        //保存deferredResult对象，等待其他线程调用它。此时respond还在保持开的状态，等待有值给它返回
        DeferredResultQueue.save(deferredResult);
        return deferredResult;
    }

    @ResponseBody
    @RequestMapping("create")
    public String create(){
        //执行生成订单编号
        String order = UUID.randomUUID().toString();
        //将保存的deferredResult对象取出来
        DeferredResult<Object> deferredResult= DeferredResultQueue.get();
        //将结果通过responed将结果返回出去
        deferredResult.setResult(order);
        return "success------"+order;
    }
```









# spring整合mybatis

spring帮助我们管理对象。

bean是一个对象，被spring**管理**的对象称为bean，属性赋值、依赖注入。



## **@Component和@Bean的区别**

@Component一般用于自己定义的组件，可以直接标注在类上

@Bean用途更多，既可以用于自己定义的组件，多用于第三方的组件。它要配和@configuration注解使用，不能直接标注要类上。

使用注解往容器中注入bean的方式：

> 1. Component
> 2. Bean
> 3. Repository



## Bean的生命周期

创建--->属性赋值（依赖注入）--->初始化----->使用------>销毁

一般来说bean的生命周期和容器的生命周期相同，因为容器启动时，就会去加载所有的bean。



































## transactional失效的情况

- catch住了异常

- 受检异常不回滚
- 非public的方法出现异常，就算标注了该注解也不会回滚
- mysql数据库是否支持事务，innodb支持事务、myisam不支持事务



全局异常处理

@ResponseStatus

@ExceptionHandler

@ControllerAdvice



## spring事务的传播

Propagation枚举了多种事务传播模式，部分列举如下：

- 1、REQUIRED（required）(默认模式)：业务方法需要在一个容器里运行。如果方法运行时，已经处在一个事务中，那么加入到这个事务，否则自己新建一个新的事务。
- 2、NOT_SUPPORTED：声明方法不需要事务。如果方法没有关联到一个事务，容器不会为他开启事务，如果方法在一个事务中被调用，该事务会被挂起，调用结束后，原先的事务会恢复执行。
- 3、REQUIRESNEW（requires new）：不管是否存在事务，该方法总汇为自己发起一个新的事务。如果方法已经运行在一个事务中，则原有事务挂起，新的事务被创建。
- 4、 MANDATORY（mandatory）：该方法只能在一个已经存在的事务中执行，业务方法不能发起自己的事务。如果在没有事务的环境下被调用，容器抛出例外。
- 5、SUPPORTS（supports）：该方法在某个事务范围内被调用，则方法成为该事务的一部分。如果方法在该事务范围外被调用，该方法就在没有事务的环境下执行。
- 6、NEVER（never）：该方法绝对不能在事务范围内执行。如果在就抛例外。只有该方法没有关联到任何事务，才正常执行。
- 7、NESTED（newted）：如果一个活动的事务存在，则运行在一个嵌套的事务中。如果没有活动事务，则按REQUIRED属性执行。它使用了一个单独的事务，这个事务拥有多个可以回滚的保存点。内部事务的回滚不会对外部事务造成影响。它只对DataSourceTransactionManager事务管理器起效。

## jwt

> json  web  token
>
> 一般用于用户认证，用于前后端分离的项目，微信小程序，app开发

无状态：登录成功后，将用户数据加密，将加密后的加密串给用户，加密串中包含了用户数据。此后，客户端与服务器通信都要带上这个jwt，用户可以使用这个加密串去调用其他服务。



定时器