# 一、springboot与缓存

cache：缓存接口，定义缓存，

chacheManager：缓存管理器，管理各种缓存组件

@cacheable：主要针对方法配置，能够根据方法的请求参数对其结果进行缓存

@cacheEvict：清空缓存

@Cacheput：保证方法被调用，又希望结果被缓存

<u>@EnableCaching：开启基于注解的缓存</u>

keyCenerator：缓存数据时key生成策略，可以自己定义key的生成策略

serialize：缓存数据时value序列化策略



# 二、cache原理（@Cacheable）

1、自动配置类CacheAutoConfiguration

@Improt（....）

2、导入了如下缓存组件，

3、其中SimpleCacheConfiguration这个组件默认生效

4、进入SimpleCacheConfiguration，看这个组件给容器中注册了什么

![1585394088480](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585394088480.png)![1585394276024](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585394276024.png)



5、给容器中注册了cacheManager（缓存管理器）：叫做ComcurrentMapCacheManager

![1585394528671](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585394528671.png)



6、ComcurrentMapCacheManager这个缓存管理器可以用于获取和创建ConcurrentMapCache类型的组件

![1585394885213](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585394885213.png)7、ConcurrentMapCache中很多方法用于查缓存的数据，数据保存在ConcurrentMap类型的数组store中（ConcurrentMap<Object, Object> store）

![1585395080790](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585395080790.png)





# 三、缓存运行的流程：以@Cacheable为例

1、方法运行之前先尝试getCache去获取（查询）emp这个缓存（按照cacheNames指定的名字去获取）

​	第一次时，cache为null，所以会先调用createConcurrentMapCache这个方法去创建一个缓存

![1585395413749](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585395413749.png)

2、创建完成后，会去Cache中查找缓存，我这查找的是key是参数id=1.

key默认时使用keyGenerator生成的，默认使用SimpleKeyGenerator生成

![1585395787555](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585395787555.png)

3、方法第一次运行时，肯定是没有这个key的

![1585396059293](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585396059293.png)

4、结果为空，说明缓存中没有保存这样的key，那么执行我们自己的方法，然后将执行的结果put（保存）到store中，下次再次通过这个参数id=1访问这个方法时，就可以从缓存中获取前面保存的数据了，不用再次执行方法，得到数据



**总结：@Cacheable标注的方法执行之前，根据我们配置的缓存名称取查询有没有这个缓存，第一次运行时是没有的，所以去创建这个缓存（emp），再去这个缓存中查询数据，key默认是我们方法参数的值（如id=1），显然也是没有这个key的，所以就执行我们的方法，然后将执行的结果当成value，以key-value的形式保存到store这个数组中。下次再以同样的参数值来执行这个方法时，就能够再缓存中找到结果，那么就不必再去通过执行方法去获取结果了。**

**@Cacheable:运行时机：先寻找该缓存，没有则创建，再执行方法，再将结果put到缓存中**





![1585398447768](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585398447768.png)

### 3.1 自定义keyGenerator，生成key的策略

![1585398772108](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585398772108.png)

### 3.2 condition：满足指定条件才加入缓存

![1585398877077](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585398877077.png)



### 3.3 unless：本来应该加入缓存，除非怎么怎么样，就不加入缓存

![1585399041442](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585399041442.png)





## 四、@Cacheput：保证方法被调用，又希望结果被缓存

（常用：修改了数据库的数据，将结果保存到缓存中）

 **运行时机：先调用方法，再将结果put到缓存中**

![1585399745914](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585399745914.png)

利用@Cacheput（做修改updEmp），可以做到既修改了数据库中数据，也可以将修改的数据保存到缓存中。

结合@Cacheable（做查询selEmp），可以做到不通过方法的执行，获取已经修改的数据。前提是：两者使用的缓存名称一样（都是emp，key都是id），

### 4.1 @CacheEvict：缓存清除，删除一个数据后，将他再缓存中的数据也删掉

当删除（delEmp）数据库中的一条树据后，缓存中也要删掉这个数据

同样也是缓存需要一样，key也要一样

**常用属性：**

**1.allEntries=true;删掉缓存中所有数据，默认是false‘**

**2.brforeInvocation：缓存的清除是否再方法之前执行；**

**默认是false：再方法执行之后清除缓存：好处是当该方法执行出现异常时，不会清除缓存。**

**设置为true时：在方法执行之前清除缓存。不管方法是否出现异常，缓存都会清除**

### 4.2  @caching：指定多个缓存规则

![1585447209871](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585447209871.png)

就是使用多个key存储了相同的数据，可以通过不同的key从缓存中查到相同的数据

### 4.3@cacheConfig    //抽取缓存的公共配置，标注在类上

在每个方法上，都要指明缓存名称，这样比较麻烦！

![1585447919052](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585447919052.png)

# 五、使用redis

之前默认使用ConcurrentMapCache缓存，现在使用redis，首先导入redis的是starter

### 5.1 操作redis

![1585451010131](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585451010131.png)

使用redisTemplate去存储kv都是object类型时，需要这个类是可以序列化的。存储时默认使用jdk的序列化，这样存储到redis缓存中时结果如下

![1585453899422](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585453899422.png)

可以自己去设置默认的序列化

![1585454042515](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585454042515.png)

![1585454150664](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585454150664.png)





自定义缓存管理器：83集。

![1585530355513](C:\Users\汤\AppData\Roaming\Typora\typora-user-images\1585530355513.png)