hello



**@ExceptionHandler** 

![1590481202509](D:\TyporaDir\Untitled 1.assets\1590481202509.png)

使用该注解标注的异常有优先级的问题。优先使用与异常比配度最高的那个方法。

 若该注解加在某个controller中，则只能处理这个controller里面的异常。

所以可以**定义处理全局异常**的

使用**@controllerAdvice**来处理全局异常





可以自定义异常，继承RuntimeException，使用**@ResponseStaus**来定制状态码，reason来响应错误信息。可以标注在类或者方法上，表示在方法上时，表示这个方法是处理异常的，**只要进入了这个方法中，就会抛出我们定制的异常**。

![1590484522022](D:\TyporaDir\Untitled 1.assets\1590484522022.png)

```java
DefaultHandlerExceptionResolver//这个类中帮我们处理了很多异常，比如请求方式不匹配等异常。
SimpleMappingExceptionResolver//处理视图方面的异常，出现那个异常，跳转到那个页面。这种跳转需要在springmvc配置文件中配置。如下图
```

![1590485848322](D:\TyporaDir\Untitled 1.assets\1590485848322.png)

![1590500401400](D:\TyporaDir\MVC异常处理.assets\1590500401400.png)