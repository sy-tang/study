# java基础

# 1、面向对象的特点：

​	封装、继承、多态、抽象

# 2、访问修饰符

public

private

protected

defuault

# 3、java数据类型

基本数据类型：byte short  int long float double  char boolean

枚举类型：enum

引用类型：String



# 4、==和eques的区别？

==比较的是堆内的地址，在Object中equeals也是使用==，但是equals可以自己重写，根据需要来指定比较的策略。

# 5、&和&&的区别：

&：按位与、逻辑与：两边的都会执行一下。

&&：短路与：前一个为false的话，后面的就直接短路了（后面的不执行）



# 6、栈、堆、静态区存放的值

栈：变量、对象的引用     （操作速度最快）

堆：通过new、构造器创建出来的对象

静态区：字面量、常量

![1588211151333](D:\TyporaDir\复习\Untitled.assets\1588211151333.png)

# 7、switch（expr）中能支持的数据类型

1.5之前：byte、short、char、int

1.5开始：增加了枚举类型

1.7：增加了String类型

目前不支持long，float，double 类型。

# 8、break和continue的区别：

break跳出整个循环。continue跳出当前循环，执行下一次循环。

# 9、重写和重载的区别

重载：方法名相同、参数不同

重写：子类对父类的方法重新定义

# 10、

使用eques比较时，两个值相同则hash值一定相同，hash值相同，两个值不一定相同

# 11、String是final类，不能被继承

# 12、String、StringBuilder、StringBuffer的区别

String、不可变长字符串。final

StringBuilder、StringBuffer：可变长的字符串

StringBuilder：（单线程）不安全，效率高

StringBuffer：安全，使用了synchronize锁，效率低。

![1588213019850](D:\TyporaDir\复习\Untitled.assets\1588213019850.png)

# 13、抽象类和接口类的区别：

1、抽象类使用abstract修饰；2、抽象类不能实例化，即不能使用new关键字来实例化对象；3、含有抽象方法（使用abstract关键字修饰的方法）的类是抽象类，必须使用abstract关键字修饰；4、抽象类可以含有抽象方法，也可以不包含抽象方法，抽象类中可以有具体的方法；5、如果一个子类实现了父类（抽象类）的所有抽象方法，那么该子类可以不必是抽象类，否则就是抽象类；6、抽象类中的抽象方法只有方法体，没有具体实现；

 1、接口使用interface修饰；2、接口不能被实例化；3、一个类只能继承一个类，但是可以实现多个接口；4、接口中方法均为抽象方法；5、接口中不能包含实例域或静态方法（静态方法必须实现，接口中方法是抽象方法，不能实现） 



# 14、静态内部类和内部类的区别

静态内部类：可以不依赖外部类实例而实例化

内部类：需要外部类实例化后才能实例化。





![1588215123621](D:\TyporaDir\复习\Untitled.assets\1588215123621.png)

# 15、静态变量和实例变量的区别：

静态变量被static修饰、是类变量，属于类，

![1588215260192](D:\TyporaDir\复习\Untitled.assets\1588215260192.png)

# 16、GC

# 17：接口可以继承接口

![1588217320206](D:\TyporaDir\复习\Untitled.assets\1588217320206.png)

# 18、fianl关键字

修饰类：表示该类不能被继承

修饰方法：表示该方法不能被重写

修饰变量：表示该变量只能一次赋值，后面不能被修改

# 20、创建对象时构造器调用顺序（*）

初始化静态成员---->父类构造器------>非静态成员-------->自己的构造器。

# 21、Integer的范围

-128~127之间。在这之间的值进行比较不会去new一个新的对象，而是使用常量池中的对象进行比较。超过这个个返回，会new一个Integer对象。

# 22、基本类型转字符串

Integer.ParseInt()/valueOf()

+连接符  / valueOf

# 23、error和exception的区别

error是系统级的错误和程序无法处理的异常

exception是需要捕获或者需要程序进行处理的异常

# 24、java中异常处理

try用来执行一段程序，如果系统抛出一个异常，使用catch来捕获。如果有必须执行的代码，需要放到finally中，

throw用于明确地抛出一个异常，throws用于方法上，声明这个方法可能抛出的异常。

# 25、常见异常

![1588225319471](D:\TyporaDir\复习\Untitled.assets\1588225319471.png)

# 26、List、set、Map是否继承自Collection接口？

List、set是，Map不是。Map是一个接口。

# 27、ArrayList 、vector、LinkedList

ArrayList和vector都是以数组存储的，查询快，插入慢，Arraylist线程不安全、vector线程安全。解决？

使用collections中的方法解决多线程下集合不安全的问题。CopyOnWriteArrayList<E>、CopyOnWriteArraySet<E>、synchronizedList、synchronizedSet

LinkedList：双向链表

# 28、list、set、Map

![1588231354519](D:\TyporaDir\复习\Untitled.assets\1588231354519.png)

# **30、TreeMap和TreeSet在排序时如何比较元素**

# 31、sleep和wait的区别：

![1588233402327](D:\TyporaDir\复习\Untitled.assets\1588233402327.png)

# 32、

![1588233805406](D:\TyporaDir\复习\Untitled.assets\1588233805406.png)