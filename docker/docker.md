# d前言

# docker常用命令

为了简便，这里以id代替容器名或者容器id。

```shell
docker search  mysql            #查找镜像
docker pull mysql:[tag]         #拉取镜像
docker run -d -p 8080:8080 --name mytomcat tomcat    #常用的启动镜像的方式，具体看官网
docker run --rm tomcat:9.0      #启动容器，用完自动删除
docker ps                       #查看运行的容器
docker ps  -a                   #查看所有容器，显示所有信息
docker ps  -aq                  #查看所有容器，只显示容器id
docker stop id                  #关闭指定的容器
docker rm id                    #删除指定的容器,只能删除停止的容器
docker rm -f id                 #强制删除容器
docker rm -f $(docker ps -aq)   #强制删除所有的容器
docker rmi id                   #删除镜像
docker update --restart=always id #容器自启动
docker logs -tf id              #查看容器的日志信息。
docker logs -tf --tail 10 id    #查看容器的日志信息，只取最后10条
docker top id                   #查看容器进程信息
docker inspect id               #查看容器信息
docker exec -it id  /bin/bash(or /bin/sh) #进入已启动的容器
docker run -it -d -P --name mytomcat01 tomcat /bin/bash   #已交互模式运行启动
docker attach id      #连接已运行的容器，只有以交互模式启动的才能看到终端。在终端使用exit会关闭容器
docker ps -a |grep mytomcat     #结合docker和linux命令，查找指定的容器
docker cp -a id:容器中路径  要保存的路径    #将容器中的文件复制出来。
docker pause id:                #暂停容器的运行
docker unpause id：             #取消暂停
curl localhost:8080             #linux中命令，测试端口
docker stats                    #查看内存信息
docker commit -m="" -a="" id 目标镜像名  #提交镜像。
docker build -t 镜像名 .         #构建镜像（前提：dockerFile文件在当前目录下）
docker run -it -v 宿主机目录：容器目录  /bin/bash   #挂载到指定目录
docker run -it -v:容器目录  /bin/bash     #匿名挂载 
docker volume ls                #查 看挂载情况
docker history 镜像id            #查看镜像构建时使用的命令
docker network ls               #查看所有的网卡
docker network rm 网卡id         #删除指定的网卡
docker network inspect 网卡id    #查看网卡信息
docker network create -d bridge --subnet 子网  --gateway 网关 自定义网卡名(mynet)  #自定义网卡
docker run -d -P --name tomcat-net --net mynet tomcat   #容器使用自定义的网卡
docker exec -it id ping id      #容器之间ping
docker network connect mynet mytomcat #将mytomcat连接到自定一段网卡中。
docker run -d -P --name tomcat01 --link  tomcat02  #将tomcat01与02互通，
```



![3](D:\TyporaDir\docker\Untitled.assets\1589547156769.png)

from:基础镜像

run：**构建镜像时需要运行的命令**

add：添加镜像等内容

volume：挂载目录

expose：运行端口号

env：运行时的环境变量

cmd：**容器启动时执行的命令**

![1589547577495](D:\TyporaDir\docker\Untitled.assets\1589547577495.png)

![1589549285251](D:\TyporaDir\docker\Untitled.assets\1589549285251.png)

![1589587412214](D:\TyporaDir\docker\Untitled.assets\1589587412214.png)







# 四、docker网络

```shell
docker rm -f $(docker ps -aq)    #删除所有的容器。
docker rmi -f $(docker images -ap)  #删除所有的镜像。

```

## 1、理解dockers0

![1589693769408](D:\TyporaDir\docker\Untitled.assets\1589693769408.png)

如上就是docker0的地址。

> docker如何处理容器的网络？容器能够访问容器外面吗？外面能够ping同容器内部吗？

查看容器内部网络地址，在容器中使用Ip addr或者docker exec -it  启动的容器 ip addr

![1589694253299](D:\TyporaDir\docker\Untitled.assets\1589694253299.png)

```shell
#使用命令测试一下外面ping容器
ping  172.17.0.4   #结果能够ping通
#容器之间也可以相互ping通。只不过容器中可以使用ping命令，有的容器连最基本的命令都没有。
```

**原理**

**只要我们安装了docker，就会有一个网卡docker0，我们启动容器时，docker会给容器分配ip地址。使用的是桥接模式，使用的技术是veth-pair技术**

如上，查看了docker容器中的ip地址后，再次在外面使用ip addr可以看到如下，多了一个网卡。多的就是启动的tomcat的网卡。（成对成对的）

![1589694702040](D:\TyporaDir\docker\Untitled.assets\1589694702040.png)

```shell
#veth-pair就是一对虚拟的设备的接口，成对出现（如上）。一段连接着协议，一段彼此相连。
```

![1589696511444](D:\TyporaDir\docker\Untitled.assets\1589696511444.png)

docker中所有的网络接口都是虚拟的，只要容器删除，对应的网桥就没有了。

docker容器启动时，所有的接口都会重新分配。原先的地址就会失效。

## 2、--link

**如何可以不使用地址来实现容器与容器之间的访问呢？**

**可不可以使用容器的名字来实现容器的互ping呢**？

> 实际上直接ping是ping不通的。如何解决，使用--link来连通两个容器。

```shell
#使用--link，在mytomcat03启动时去配置mytomcat02，这样03就可以ping通02，但是02不能ping通03

[root@localhost ~]# docker run -d -P --name mytomcat03 --link mytomcat02 tomcat  #建立连接
cf2692a3f779a8094768f4a50ad3d95afddb6f0d62e01a681bee198e56d2b551
[root@localhost ~]# docker exec -it mytomcat03 ping mytomcat02   #测试连接，ping通了
PING mytomcat02 (172.17.0.2) 56(84) bytes of data.
64 bytes from mytomcat02 (172.17.0.2): icmp_seq=1 ttl=64 time=0.102 ms
64 bytes from mytomcat02 (172.17.0.2): icmp_seq=2 ttl=64 time=0.076 ms
^X64 bytes from mytomcat02 (172.17.0.2): icmp_seq=3 ttl=64 time=0.085 ms
64 bytes from mytomcat02 (172.17.0.2): icmp_seq=4 ttl=64 time=0.103 ms
64 bytes from mytomcat02 (172.17.0.2): icmp_seq=5 ttl=64 time=0.074 ms
```

```java
//  --link 单向连接。就像在docker中部署jar包时一样，将自己的jar包封装成一个镜像，启动时与mysql、redis等进行连通。这样自己的容器就可以去访问mysql、redis，但是mysql和redis不能访问我们的容器。
```



**查看桥接网络的具体信息**

inspect：查看信息。

```shell
[root@localhost ~]# docker network inspect d9a1dea90d3d   ##查看信息
[
    {
        "Name": "bridge",
        "Id": "d9a1dea90d3d75c618521c3b7300089cf770a0fa97afe8f364fe65be7f38613d",
        "Created": "2020-05-17T21:06:25.747714729+08:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",   #默认，docker0
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",   
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Containers": {
            "7684ea690ea4be49002279fbf351934e3f4ea01a3ac35665bffcbe95f3d61e44": {
                "Name": "mytomcat02",  ##02的地址
                "EndpointID": "606b8e6345526a84c8bbad74acd8f36f24b8c232d735ad416dc51edecafec518",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            },
            "80f6a3d3a66a4e2d4d859684ba507e2517ad625811ac3b7a4b4ac14af5b6883a": {
                "Name": "mytomcat",   ##01的地址
                "EndpointID": "d725664bde47d7ddaac9f40861d27858a03564fce74dcb9f6907c6de38665287",
                "MacAddress": "02:42:ac:11:00:04",
                "IPv4Address": "172.17.0.4/16",
                "IPv6Address": ""
            },
            "b578dc9bbe67c167a1d341de862ef96865bcfd7fb94e452c238744880480fa1b": {
                "Name": "mysql01",
                "EndpointID": "1a0a06bbcfeb8685cf33d46ec54b11390c102de1a68f6e27ee5937890bc826a9",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            },
            "cf2692a3f779a8094768f4a50ad3d95afddb6f0d62e01a681bee198e56d2b551": {
                "Name": "mytomcat03",   ##03的地址
                "EndpointID": "28071953c46129700ab7a2a982454c59bf89e224ff226a22bd1404cbb70f0d4c",
                "MacAddress": "02:42:ac:11:00:05",
                "IPv4Address": "172.17.0.5/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
```

**它是如何通过--link与mytomcat02连通的呢？**

解析：

```shell
##进入容器，查看容器03的 /etc/hosts下的配置信息
[root@localhost ~]# docker exec -it mytomcat03  cat /etc/hosts  
127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.17.0.2      mytomcat02 7684ea690ea4    ##mytomcat02的地址进行了映射。。
172.17.0.5      cf2692a3f779
```

**--link真实开发中已经不适合了，比较笨重**

## 3、自定义网络（*）

**容器互联**

```shell
docker network ls   #查看所有的网卡
dockers network rm 网卡    #移出指定的网卡。
```

**网络模式**

![1589698936626](D:\TyporaDir\docker\Untitled.assets\1589698936626.png)

- 桥接  bridge（dockers默认，自定网络）
- none 不配置网络
- host  和宿主机共享网络

```shell
###--net bridge 是默认参数，我们启动容器的时候默认就有的。
###自定义网络就是在这个上做手脚。
docker run -d -P --name mytomcat04 --net bridge tomcat
```



**自定义网络**

```shell


[root@localhost ~]# docker network create --driver bridge --subnet 192.168.0.0/16  --gateway 192.168.0.1 mynet
9f873f195de16881a2464f55ee8aabc96739f3871a96781cca51135f11d6726f
```

![1589699760443](D:\TyporaDir\docker\Untitled.assets\1589699760443.png)

![1589699901847](D:\TyporaDir\docker\Untitled.assets\1589699901847.png)

```shell
###使用自定义的网络来启动容器
[root@localhost ~]# docker run -d -P --name tomcat-net-01 --net mynet tomcat 
[root@localhost ~]# docker run -d -P --name tomcat-net-02 --net mynet tomcat

##测试连接，结果能够ping通
[root@localhost ~]# docker exec -it tomcat-net-01 ping tomcat-net-02

```

## 4、网络连通

自定义网络已经实现了容器之间的互通。利用相同的网段来实现的。

![](D:\TyporaDir\docker\Untitled.assets\1589701511003.png)

**那不同的网段中的容器之间如何互通呢？这是一个问题。**

**解决**

![1589701598496](D:\TyporaDir\docker\Untitled.assets\1589701598496.png)

**实践**

```shell
 #################将docker0中的mytomcat连接到mynet网段##############
 
[root@localhost ~]# docker network connect mynet mytomcat 
[root@localhost ~]# docker network inspect mynet
[
    {
        "Name": "mynet",
        "Id": "9f873f195de16881a2464f55ee8aabc96739f3871a96781cca51135f11d6726f",
        "Created": "2020-05-17T23:13:38.674447799+08:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "192.168.0.0/16",
                    "Gateway": "192.168.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Containers": {
            "80f6a3d3a66a4e2d4d859684ba507e2517ad625811ac3b7a4b4ac14af5b6883a": {
                "Name": "mytomcat",    #######直接将mytomcat加到了mynet网段中。#####
                "EndpointID": "3b7f22dd4e1ed47d27e787329291cf36af7c35001c6f092df21366d4c25c7e6a",
                "MacAddress": "02:42:c0:a8:00:04",
                "IPv4Address": "192.168.0.4/16",
                "IPv6Address": ""
            },
            "8324ecad9c458b4decd245e209d86964c5c64ebbb40e79186057ccddcdcfc044": {
                "Name": "tomcat-net-01",
                "EndpointID": "fa5944d6f3f706e401f55d1b1dc1b0074569822ba86f32cb2fb281f57db9e782",
                "MacAddress": "02:42:c0:a8:00:02",
                "IPv4Address": "192.168.0.2/16",
                "IPv6Address": ""
            },
            "ada60b9059657f79e33eb578fb1a055a7d262c3d6fe8a93101c640a228ffae32": {
                "Name": "tomcat-net-02",
                "EndpointID": "80c405ec11018bcf190e41cb04ad8bb132d2c65ed35ee7c5af586afaf2be91a9",
                "MacAddress": "02:42:c0:a8:00:03",
                "IPv4Address": "192.168.0.3/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
```

这样mytomcat就是**一个容器两个ip地址**。

**总结：就是使用一个容器多个ip地址来进行不同网段中的容器互通**